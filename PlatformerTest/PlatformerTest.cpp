#include "gtest/gtest.h"
#include "Actor.cpp"
#include "PhysicsCompPlayer.cpp"
#include "PhysicsCompStaticRect.h"
#include "PHysicsCompStaticCircle.h"
#include "CEventWall.cpp"
#include "Volume.cpp"

TEST(PhysicsCase, SimpleGravity) {
	Actor *player = new Actor(5.0f, NULL, new PhysicsCompPlayer());
	player->setInitialPos(0.0f, 0.0f);
	player->mVelX = 0.0f;
	player->mVelY = 0.0f;
	player->physics()->mGravity = 0.5f;

	std::vector<Actor> walls;
	EXPECT_EQ(player->mVelY, 0.0f);
	EXPECT_EQ(player->mPosX, 0.0f);
	EXPECT_EQ(player->mPosY, 0.0f);

	player->update(walls);
	EXPECT_EQ(player->mVelY, 0.5f);
	EXPECT_EQ(player->mPosX, 0.0f);
	EXPECT_EQ(player->mPosY, 0.5f);
}

TEST(CollisionCase, NoCollision) {
	VolumeRect rect1(50.0f, 50.0f);
	rect1.setInitialPosition(0.0f, 0.0f);

	VolumeRect rect2(50.0f, 50.0f);
	rect2.setInitialPosition(50.0f, 0.0f);

	EXPECT_FALSE(rect1.isColliding(&rect2));
	EXPECT_FALSE(rect2.isColliding(&rect1));
}

TEST(CollisionRectCase, CollisionRectInside) {
	VolumeRect rect1(50.0f, 50.0f);
	rect1.setInitialPosition(1.0f, 0.0f);

	VolumeRect rect2(50.0f, 50.0f);
	rect2.setInitialPosition(50.0f, 0.0f);

	CollisionData *cData = rect1.isColliding(&rect2);
	EXPECT_TRUE(cData);

	cData = rect2.isColliding(&rect1);
	EXPECT_TRUE(cData);
}

TEST(CollisionRectCase, CollisionRectLeftRight) {
	VolumeRect rectMoving(2.0f, 2.0f);
	rectMoving.setInitialPosition(0.0f, 0.0f);

	VolumeRect rectStatic(2.0f, 2.0f);
	rectStatic.setInitialPosition(2.0f, 0.0f);

	CollisionData *cData = rectMoving.isColliding(&rectStatic);
	EXPECT_FALSE(cData);
	cData = rectStatic.isColliding(&rectMoving);
	EXPECT_FALSE(cData);

	rectMoving.setPosition(1.0f, 0.0f);

	cData = rectMoving.isColliding(&rectStatic);
	EXPECT_TRUE(cData);
	EXPECT_EQ(cData->xPenetration, -1.0f);
	EXPECT_EQ(cData->yPenetration, 0.0f);

	cData = rectStatic.isColliding(&rectMoving);
	EXPECT_TRUE(cData);
	EXPECT_EQ(cData->xPenetration, 1.0f);
	EXPECT_EQ(cData->yPenetration, 0.0f);
}

TEST(CollisionCircleCase, CollisionRectLeftRight) {
	VolumeCircle circleMoving(2.0f);
	circleMoving.setInitialPosition(0.0f, 0.0f);

	VolumeRect rectStatic(4.0f, 4.0f);
	rectStatic.setInitialPosition(2.0f, -2.0f);

	CollisionData *cData = circleMoving.isColliding(&rectStatic);
	EXPECT_FALSE(cData);
	cData = rectStatic.isColliding(&circleMoving);
	EXPECT_FALSE(cData);

	circleMoving.setPosition(1.0f, 0.0f);

	cData = circleMoving.isColliding(&rectStatic);
	EXPECT_TRUE(cData);
	EXPECT_EQ(cData->xPenetration, -1.0f);
	EXPECT_TRUE(std::abs(cData->yPenetration) < 0.001f);

	cData = rectStatic.isColliding(&circleMoving);
	EXPECT_TRUE(cData);
	EXPECT_EQ(cData->xPenetration, 1.0f);
	EXPECT_TRUE(std::abs(cData->yPenetration) < 0.001f);
}

TEST(CollisionCase, CollideLeftSide) {
	// Actor is 50 x 50
	Actor *player = new Actor(5.0f, NULL, new PhysicsCompPlayer());
	player->setInitialPos(0.0f, 0.0f);
	player->mVelX = 5.0f;
	player->mVelY = 0.0f;
	player->physics()->mGravity = 0.0f;

	Actor *wall = new Actor(0.0f, NULL, new PhysicsCompStaticRect(64.0f, 64.0f, new CEventWall()));
	wall->setInitialPos(67.0f, 0.0f);

	std::vector<Actor> walls;
	walls.reserve(1);
	walls.push_back(*wall);

	player->update(walls);
	EXPECT_EQ(player->mPosX, 5.0f);
	EXPECT_EQ(player->mPosY, 0.0f);

	player->update(walls);
	EXPECT_EQ(player->mPosX, 10.0f);
	EXPECT_EQ(player->mPosY, 0.0f);

	player->update(walls);
	EXPECT_EQ(player->mPosX, 10.0f);
	EXPECT_EQ(player->mPosY, 0.0f);
}

TEST(CollisionCase, CollideRightSide) {
	// Actor is 50 x 50
	Actor *player = new Actor(5.0f, NULL, new PhysicsCompPlayer());
	player->setInitialPos(63.0f, 0.0f);
	player->mVelX = -5.0f;
	player->mVelY = 0.0f;
	player->physics()->mGravity = 0.0f;

	Actor *wall = new Actor(0.0f, NULL, new PhysicsCompStaticRect(64.0f, 64.0f, new CEventWall()));
	wall->setInitialPos(0.0f, 0.0f);

	std::vector<Actor> walls;
	walls.reserve(1);
	walls.push_back(*wall);

	player->update(walls);
	EXPECT_EQ(player->mPosX, 58.0f);
	EXPECT_EQ(player->mPosY, 0.0f);

	player->update(walls);
	EXPECT_EQ(player->mPosX, 57.0f);
	EXPECT_EQ(player->mPosY, 0.0f);
}

