#ifndef _ESCREENACTORPROPERTIES_H
#define _ESCREENACTORPROPERTIES_H

#include "EditorScreen.h"
#include "UIButtonText.h"
#include "UITextBox.h"

class EScreenActorProperties : public Screen {
public:
	EScreenActorProperties(EditorScreen *parent);

	bool onEntry(App &app) override;
	void onExit() override;

	InputResult handleInput() override;
	InputResult handleInput(App &app, SDL_Event &e) override;

	void update(App &app) override;
	void render(App &app, int currentTicks) override;

	bool isStackedScreen() override;

private:
	void saveProperties(App &app);

	EditorScreen *mParent;
	SDL_Rect *mPropertyWindow;
	std::string mTitle;
	UITextBox *mCooldownTextbox, *mLifespanTextbox, *mDelayTextbox;
	UIButtonText *mSaveButton, *mCancelButton;
	
	Texture *mOverlayTexture;
	Texture *mTitleTexture;
};

#endif /* _ESCREENACTORPROPERTIES_H */