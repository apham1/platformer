#ifndef _CEVENTHIT_H
#define _CEVENTHIT_H

#include "CEvent.h"
#include "Actor.h"

class CEventHit : public CEvent {
public:
	/** Triggers upon colliding with the "win" object and allows the player to continue to the next level.
		@param actor The main actor
		@param object The "win" object it's colliding with
		@param cData Collision data
	*/
	void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override {
		if (!actor.mIsInvincible) {
			if (cData->xPenetration >= 0.0f) {
				actor.mVelX = -15.0f;
			} else if (cData->xPenetration < 0.0f) {
				actor.mVelX = 15.0f;
			}

			actor.mVelY = -5.0f;
			actor.mIsInvincible = true;
			actor.mIsKnockedBack = true;
			actor.mIsDucking = false;
		}
	}

	void triggerEventExit(Actor &actor, Actor &object) override {}
};

#endif /* _CEVENTHIT_H */