#ifndef _SCREEN_H
#define _SCREEN_H

#include <SDL.h>

class App;
struct InputResult;

class Screen {
public:
	Screen() {}

	virtual bool onEntry(App &app) = 0;
	virtual void onExit() = 0;
	
	virtual InputResult handleInput() = 0;
	virtual InputResult handleInput(App &app, SDL_Event &e) = 0;

	virtual void update(App &app) = 0;
	virtual void render(App &app, int currentTicks) = 0;

	virtual bool isStackedScreen() = 0;
};

struct InputResult {
	bool inputConsumed;
	Screen *newScreen;

	InputResult() {
		inputConsumed = false;
		newScreen = NULL;
	}
};

#endif /* _SCREEN_H */