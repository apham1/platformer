#include <math.h>
#include "Volume.h"
#include "VolumeCircle.h"
#include "VolumeRect.h"

/** Sets the current position.
	@param x The x position
	@param y The y positioon
*/
void Volume::setPosition(float x, float y) {
	current = !current;
	xPos[current] = x;
	yPos[current] = y;
}

/** Sets the initial current position and initializes old position data.
	@param x The x position
	@param y The y positioon
*/
void Volume::setInitialPosition(float x, float y) {
	xPos[current] = x;
	yPos[current] = y;
	setPosition(x, y);
}

/** @return current x position
*/
float Volume::x() {
	return xPos[current];
}

/** @return current y position
*/
float Volume::y() {
	return yPos[current];
}

/** Rolls back the current position to its previous state.
*/
void Volume::rollbackPosition() {
	current = !current;
}

/** @return old x position
*/
float Volume::xOld() {
	return xPos[!current];
}

/** @return old y position
*/
float Volume::yOld() {
	return yPos[!current];
}

/** Checks if a circle and a rectangle are colliding and finds the x and y penetration depth.
	@param circle A circular object
	@param rect A rectangular object
	@return The collision data containing the x and y penetration depth
*/
CollisionData *Volume::isColliding(VolumeCircle* circle, VolumeRect *rect) {
	VolumeRect convertedRect = rect->convertFromNegativeDimensions();

	float leftOther = convertedRect.x();
	float rightOther = leftOther + convertedRect.w;
	float topOther = convertedRect.y();
	float bottomOther = topOther + convertedRect.h;

	float cX, cY;
	// Finds the closest point of the rectangle
	if (circle->x() < convertedRect.x()) {
		cX = convertedRect.x();
	} else if (circle->x() > convertedRect.x() + convertedRect.w) {
		cX = convertedRect.x() + convertedRect.w;
	} else {
		cX = circle->x();
	}

	if (circle->y() < convertedRect.y()) {
		cY = convertedRect.y();
	} else if (circle->y() > convertedRect.y() + convertedRect.h) {
		cY = convertedRect.y() + convertedRect.h;
	} else {
		cY = circle->y();
	}

	CollisionData *cData = NULL;
	float distanceCenterToClosest = sqrt(GameMath::distanceSquared(circle->x(), circle->y(), cX, cY));
	if (distanceCenterToClosest < circle->r) {
		float oldLeftOther = convertedRect.xOld();
		float oldRightOther = oldLeftOther + convertedRect.w;
		float oldTopOther = convertedRect.yOld();
		float oldBottomOther = oldTopOther + convertedRect.h;

		float penetration = circle->r - distanceCenterToClosest;
		float angle = atan2(circle->y() - cY, circle->x() - cX);

		float xPen = penetration * cos(angle);
		float yPen = penetration * sin(angle);

		cData = new CollisionData(xPen, yPen);
	}

	return cData;
}

/** Checks if a circle and rectangle are colliding from the perspective of the rectangle.
	@param rect A rectangular object
	@param circle A circular object
	@return The collision data containing the x and y penetration depth
*/
CollisionData *Volume::isColliding(VolumeRect *rect, VolumeCircle* circle) {
	CollisionData *cData = isColliding(circle, rect);

	if (cData) {
		cData->xPenetration = -cData->xPenetration;
		cData->yPenetration = -cData->yPenetration;
	}

	return cData;
}