#include "EditorScreen.h"
#include "EScreenActorProperties.h"
#include "App.h"

EScreenActorProperties::EScreenActorProperties(EditorScreen *parent) {
	mParent = parent;
}

bool EScreenActorProperties::onEntry(App &app) {
	mOverlayTexture = new Texture();
	mOverlayTexture->loadFromFile(app.mRenderer, "overlay");
	mOverlayTexture->setAlpha(100);

	mPropertyWindow = new SDL_Rect();
	mPropertyWindow->x = static_cast<int>(mParent->mMouseX);
	mPropertyWindow->y = static_cast<int>(mParent->mMouseY);
	mPropertyWindow->w = 250;
	mPropertyWindow->h = 300;
	
	if (mPropertyWindow->x + mPropertyWindow->w > app.mScreenWidth) {
		mPropertyWindow->x = app.mScreenWidth - mPropertyWindow->w;
	}

	if (mPropertyWindow->y + mPropertyWindow->h > app.mScreenHeight) {
		mPropertyWindow->y = app.mScreenHeight - mPropertyWindow->h;
	}

	mTitle = "Actor Properties";
	mTitleTexture = new Texture();
	mTitleTexture->loadText(app.mRenderer, mParent->mFont, mTitle);

	mCooldownTextbox = new UITextBox(mPropertyWindow->x + 10, mPropertyWindow->y + 35, 100, 20, std::to_string(mParent->mSelectedItems.front().actor->gun()->mCooldown));
	mCooldownTextbox->setLabel(app.mRenderer, mParent->mFont, "Cooldown: ");

	mLifespanTextbox = new UITextBox(mPropertyWindow->x + 10, mPropertyWindow->y + 65, 100, 20, std::to_string(mParent->mSelectedItems.front().actor->gun()->mProjectileLifespan));
	mLifespanTextbox->setLabel(app.mRenderer, mParent->mFont, "Bullet Lifespan: ");

	mDelayTextbox = new UITextBox(mPropertyWindow->x + 10, mPropertyWindow->y + 95, 100, 20, std::to_string(mParent->mSelectedItems.front().actor->gun()->mDelay));
	mDelayTextbox->setLabel(app.mRenderer, mParent->mFont, "Delay: ");

	mSaveButton = new UIButtonText(mPropertyWindow->x + 10, mPropertyWindow->y + 125, 100, 50);
	mSaveButton->setLabel(app.mRenderer, mParent->mFont, "Save");

	mCancelButton = new UIButtonText(mPropertyWindow->x + 120, mPropertyWindow->y + 125, 100, 50);
	mCancelButton->setLabel(app.mRenderer, mParent->mFont, "Cancel");

	return true;
}

void EScreenActorProperties::onExit() {
	if (mOverlayTexture) {
		delete mOverlayTexture;
		mOverlayTexture = NULL;
	}

	if (mTitleTexture) {
		delete mTitleTexture;
		mTitleTexture = NULL;
	}

	if (mSaveButton) {
		delete mSaveButton;
		mSaveButton = NULL;
	}

	if (mCancelButton) {
		delete mCancelButton;
		mCancelButton = NULL;
	}
}

InputResult EScreenActorProperties::handleInput() {
	InputResult res;
	return res;
}

InputResult EScreenActorProperties::handleInput(App &app, SDL_Event &e) {
	InputResult res;
	if (mCooldownTextbox->handleInput(e)) {
		res.inputConsumed = true;
	} else if (mLifespanTextbox->handleInput(e)) {
		res.inputConsumed = true;
	} else if (mDelayTextbox->handleInput(e)) {
		res.inputConsumed = true;
	} else if (e.type == SDL_KEYDOWN) {
		res.inputConsumed = true;
		switch (e.key.keysym.sym) {
			case SDLK_ESCAPE:
				res.inputConsumed = true;
				app.popScreen();
				break;
			case SDLK_RETURN:
				res.inputConsumed = true;
				saveProperties(app);
		}
	} else if (e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP) {
		res.inputConsumed = true;
		if (e.type == SDL_MOUSEBUTTONDOWN) {
			int x, y;
			SDL_GetMouseState(&x, &y);
			mCooldownTextbox->updateTextBox(static_cast<float>(x), static_cast<float>(y), e.button.button == SDL_BUTTON_LEFT && e.type == SDL_MOUSEBUTTONDOWN);
			mLifespanTextbox->updateTextBox(static_cast<float>(x), static_cast<float>(y), e.button.button == SDL_BUTTON_LEFT && e.type == SDL_MOUSEBUTTONDOWN);
			mDelayTextbox->updateTextBox(static_cast<float>(x), static_cast<float>(y), e.button.button == SDL_BUTTON_LEFT && e.type == SDL_MOUSEBUTTONDOWN);
		}

		if (mSaveButton) {
			if (mSaveButton->isClicked()) {
				if (e.type == SDL_MOUSEBUTTONUP) {
					saveProperties(app);
				}
			} else {
				mSaveButton->updateButton(e.button.x, e.button.y, e.button.button = SDL_BUTTON_LEFT && e.type == SDL_MOUSEBUTTONDOWN);
			}
		}

		if (mCancelButton) {
			if (mCancelButton->isClicked()) {
				if (e.type == SDL_MOUSEBUTTONUP) {
					app.popScreen();
				}
			} else {
				mCancelButton->updateButton(e.button.x, e.button.y, e.button.button = SDL_BUTTON_LEFT && e.type == SDL_MOUSEBUTTONDOWN);
			}
		}
	} else if (e.type == SDL_MOUSEMOTION) {
		res.inputConsumed = true;
		if (mSaveButton) {
			mSaveButton->updateButton(e.motion.x, e.motion.y, e.motion.state & SDL_BUTTON_LMASK);
		}
		if (mCancelButton) {
			mCancelButton->updateButton(e.motion.x, e.motion.y, e.motion.state & SDL_BUTTON_LMASK);
		}
	}
	return res;
}

void EScreenActorProperties::update(App &app) {
	mCooldownTextbox->update(app.mRenderer, mParent->mFont);
	mLifespanTextbox->update(app.mRenderer, mParent->mFont);
	mDelayTextbox->update(app.mRenderer, mParent->mFont);
}

void EScreenActorProperties::render(App &app, int currentTicks) {
	SDL_RenderClear(app.mRenderer);

	mParent->render(app, currentTicks);

	mOverlayTexture->render(app.mRenderer, 0, 0, app.mScreenWidth, app.mScreenHeight);

	SDL_SetRenderDrawColor(app.mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderFillRect(app.mRenderer, mPropertyWindow);

	SDL_Rect titleBar = { mPropertyWindow->x, mPropertyWindow->y, mPropertyWindow->w, 25 };
	SDL_SetRenderDrawColor(app.mRenderer, 0xCC, 0xDF, 0xFF, 0xFF);
	SDL_RenderFillRect(app.mRenderer, &titleBar);

	mTitleTexture->render(app.mRenderer, mPropertyWindow->x + 5, mPropertyWindow->y + 3);

	SDL_SetRenderDrawColor(app.mRenderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderDrawRect(app.mRenderer, mPropertyWindow);

	mCooldownTextbox->render(app.mRenderer, currentTicks);
	mLifespanTextbox->render(app.mRenderer, currentTicks);
	mDelayTextbox->render(app.mRenderer, currentTicks);
	mSaveButton->render(app.mRenderer, 0, currentTicks);
	mCancelButton->render(app.mRenderer, 0, currentTicks);

	SDL_RenderPresent(app.mRenderer);
}

bool EScreenActorProperties::isStackedScreen() {
	return true;
}

void EScreenActorProperties::saveProperties(App &app) {
	mParent->mSelectedItems.front().actor->gun()->mCooldown = mCooldownTextbox->getValue();
	mParent->mSelectedItems.front().actor->gun()->mProjectileLifespan = mLifespanTextbox->getValue();
	mParent->mSelectedItems.front().actor->gun()->mDelay = mDelayTextbox->getValue();

	app.popScreen();
}