#include "TitleScreen.h"
#include "GameScreen.h"
#include "EditorScreen.h"
#include "App.h"

/** Activates on entry into the screen, initializes values and loads media used in the screen.
	@param app The main application
*/
bool TitleScreen::onEntry(App &app) {
	if (!loadMedia(app)) {
		return false;
	}

	mMouseX = 0;
	mMouseY = 0;
	mPlayButtonX = app.mScreenWidth / 2 - mPlayTexture.getWidth() / 2;
	mPlayButtonY = app.mScreenHeight / 2 - mPlayTexture.getHeight() / 2;

	return true;
}

/** Activates upon exiting the screen, freeing up memory used in it.
*/
void TitleScreen::onExit() {
	mTitleTexture.free();
	mPlayTexture.free();
}

/** Loads all textures.
	@return true if all media successfully loaded
*/
bool TitleScreen::loadMedia(App &app) {
	if (!mTitleTexture.loadFromFile(app.mRenderer, "titlescreen")) {
		return false;
	}

	if (!mPlayTexture.loadFromFile(app.mRenderer, "playbutton")) {
		return false;
	}

	return true;
}

/** Updates the screen.
*/
void TitleScreen::update(App &app) {
	SDL_GetMouseState(&mMouseX, &mMouseY);
}

/** Renders actors and textures onto the screen.
*/
void TitleScreen::render(App &app, int currentTicks) {
	mTitleTexture.render(app.mRenderer, app.mScreenWidth / 2, app.mScreenHeight / 2);
	mPlayTexture.render(app.mRenderer, app.mScreenWidth / 2, app.mScreenHeight / 2);
}

/** Handles non-event input.
*/
InputResult TitleScreen::handleInput() {
	InputResult res;
	return res;
}

/** Handles event input.
	@param e The event being handled.
*/
InputResult TitleScreen::handleInput(App &app, SDL_Event &e) {
	InputResult res;
	if (e.type == SDL_MOUSEBUTTONDOWN) {
		if (e.button.button == SDL_BUTTON_LEFT) {
			if (mMouseX >= mPlayButtonX && mMouseX <= mPlayButtonX + mPlayTexture.getWidth() &&
			    mMouseY >= mPlayButtonY && mMouseY <= mPlayButtonY + mPlayTexture.getHeight()) {
				res.inputConsumed = true;
				res.newScreen = new GameScreen();
			}
		} else if (e.button.button == SDL_BUTTON_RIGHT) {
			res.inputConsumed = true;
			res.newScreen = new EditorScreen();
		}
	}

	return res;
}

bool TitleScreen::isStackedScreen() {
	return false;
}