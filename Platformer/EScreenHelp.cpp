#include "EScreenHelp.h"
#include <fstream>
#include <sstream>
#include <rapidjson/document.h>
#include "App.h"

EScreenHelp::EScreenHelp() {

}

bool EScreenHelp::onEntry(App &app) {
	mTitleFont = TTF_OpenFont("Fonts/oswald/Oswald-DemiBold.ttf", 42);
	if (!mTitleFont) {
		return false;
	}

	mCategoryFont = TTF_OpenFont("Fonts/oswald/Oswald-Bold.ttf", 24);
	if (!mCategoryFont) {
		return false;
	}

	mFunctionFont = TTF_OpenFont("Fonts/oswald/Oswald-Heavy.ttf", 16);
	if (!mFunctionFont) {
		return false;
	}

	mKeyFont = TTF_OpenFont("Fonts/oswald/Oswald-Regular.ttf", 16);
	if (!mKeyFont) {
		return false;
	}

	// read entire help file into fileContents
	std::ifstream helpFile;
	helpFile.open("Text/editorkeys.json", std::ios::in);
	if (helpFile.fail()) {
		helpFile.close();
		return false;
	}

	std::stringstream sstr;
	sstr << helpFile.rdbuf();
	std::string fileContents = sstr.str();

	// load animation data
	rapidjson::Document d;
	d.Parse(fileContents.c_str());

	int totalHeight = mTopMargin;
	mPage.resize(d.Size());
	for (rapidjson::SizeType i = 0; i < d.Size(); ++i) {
		mPage[i].text.resize(d[i].Size());
		int lineHeight = 0;
		for (rapidjson::SizeType j = 0; j < d[i].Size(); ++j) {
			TTF_Font *font;
			SDL_Color color;
			int type = d[i][j]["type"].GetInt();
			switch (type) {
				case HEADER1:
					font = mTitleFont;
					color = mTitleColor;
					break;
				case HEADER2:
					font = mCategoryFont;
					color = mCategoryColor;
					break;
				case HEADER3:
					font = mFunctionFont;
					color = mFunctionColor;
					break;
				default:
					font = mKeyFont;
					color = mKeyColor;
					break;
			}
			mPage[i].text[j].loadText(app.mRenderer, font, d[i][j]["text"].GetString(), color, (app.mScreenWidth - mLeftMargin - mColumnSpacer) / 2);
			int textHeight = mPage[i].text[j].getHeight();
			if (lineHeight < textHeight) {
				lineHeight = textHeight;
			}
		}
		totalHeight += lineHeight;
	}

	helpFile.close();

	mScrollBar.init(app.mScreenWidth, app.mScreenHeight, totalHeight + mTopMargin, app.mScreenHeight, 0, mFunctionColor, mKeyColor);

	return true;
}

void EScreenHelp::onExit() {
	TTF_CloseFont(mTitleFont);
	mTitleFont = NULL;

	//mTitle.free();
}

InputResult EScreenHelp::handleInput() {
	InputResult res;
	return res;
}

InputResult EScreenHelp::handleInput(App &app, SDL_Event &e) {
	InputResult res;
	if (mScrollBar.handleInput(e)) {
		res.inputConsumed = true;
	} else if (e.type == SDL_KEYDOWN) {
		switch (e.key.keysym.sym) {
			case SDLK_ESCAPE:
			case SDLK_TAB:
			case SDLK_F1:
				res.inputConsumed = true;
				app.popScreen();
				break;
		}
	}
	return res;
}

void EScreenHelp::update(App &app) {

}

void EScreenHelp::render(App &app, int currentTicks) {
	SDL_SetRenderDrawColor(app.mRenderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(app.mRenderer);

	int prevLineHeight = 0;
	int currentLineY = 0;
	for (auto line = mPage.begin(); line != mPage.end(); ++line) {
		int currentLineX = 0;
		for (auto lineText = line->text.begin(); lineText != line->text.end(); ++lineText) {
			lineText->render(app.mRenderer, mLeftMargin + currentLineX, mTopMargin + currentLineY - mScrollBar.getScrollAmount());
			int lineHeight = lineText->getHeight();
			if (lineHeight > prevLineHeight) {
				prevLineHeight = lineHeight;
			}
			currentLineX = lineText->getWidth();
		}
		currentLineY += prevLineHeight;
		prevLineHeight = 0;
	}

	mScrollBar.render(app.mRenderer, currentTicks);
}

bool EScreenHelp::isStackedScreen() {
	return true;
}