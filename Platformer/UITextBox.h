#ifndef _UITEXTBOX_H
#define _UITEXTBOX_H

#include <SDL.h>
#include <string>
#include <cctype>
#include "Texture.h"

class UITextBox {
public:
	UITextBox(int x, int y, int width, int height, std::string inputText = "") {
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
		mIsSelected = false;
		mRenderText = true;
		mInputText = inputText;
		mInputTextTexture = new Texture();
		mLabel = "";
		mLabelTexture = new Texture();
	}

	void render(SDL_Renderer *renderer, int ticks) {
		SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_Rect textBoxRect = { x, y, width, height };
		SDL_RenderFillRect(renderer, &textBoxRect);

		if (mIsSelected) {
			SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0x00, 0xFF);
		} else {
			SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
		}
		SDL_RenderDrawRect(renderer, &textBoxRect);

		mLabelTexture->render(renderer, x - mLabelTexture->getWidth(), y);

		mInputTextTexture->render(renderer, x + 2, y);
	}

	void update(SDL_Renderer *renderer, TTF_Font *font) {
		if (mRenderText) {
			if (mInputText != "") {
				mInputTextTexture->loadText(renderer, font, mInputText);
			} else {
				mInputTextTexture->loadText(renderer, font, " ");
			}
		}
	}

	void updateTextBox(float mouseX, float mouseY, bool mousePressed) {
		if (mousePressed) {
			if (mouseX >= x && mouseX <= x + width && mouseY >= y && mouseY <= y + height) {
				mIsSelected = true;
			} else {
				mIsSelected = false;
			}
		}
	}

	void setLabel(SDL_Renderer *renderer, TTF_Font *font, std::string label) {
		mLabel = label;
		if (mLabel != "") {
			mLabelTexture->loadText(renderer, font, mLabel);
			x += mLabelTexture->getWidth();
		} else {
			mLabelTexture->loadText(renderer, font, " ");
		}
	}

	int getValue() {
		if (mInputText.length() != 0) {
			return std::stoi(mInputText);
		} else {
			return 0;
		}
	}

	bool handleInput(SDL_Event &e) {
		if (mIsSelected) {
			if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
					case SDLK_BACKSPACE:
						if (mInputText.length() > 0) {
							mInputText.pop_back();
							mRenderText = true;
						}
						return true;
				}
			} else if (e.type == SDL_TEXTINPUT && mInputText.length() < 9) {
				for (int i = 0; i < sizeof(e.text.text) / sizeof(char); i++) {
					if (e.text.text[i] == NULL) {
						break;
					}

					char input = e.text.text[i];
					if (std::isdigit(input) != 0) {
						mInputText += e.text.text;
						mRenderText = true;
						return true;
					}
				}
			}
		}

		return false;
	}

	void free() {
		if (mInputTextTexture) {
			delete mInputTextTexture;
			mInputTextTexture = NULL;
		}

		if (mLabelTexture) {
			delete mLabelTexture;
			mLabelTexture = NULL;
		}
	}
	
protected:
	int x, y, width, height;
	std::string mInputText, mLabel;
	bool mIsSelected, mRenderText;

	Texture *mInputTextTexture, *mLabelTexture;
};

#endif /* _UITEEXTBOX_H */