#ifndef _INPUTCOMP_H
#define _INPUTCOMP_H

#include <SDL.h>

class Actor;

class InputComp {
public:
	virtual ~InputComp() {}
	virtual void update(Actor &actor) = 0;
	virtual bool update(Actor &actor, SDL_Event &e) = 0;
};

#endif /* _INPUTCOMP_H */
