#ifndef _PHYSICSCOMPPLAYERCIRCLE_H
#define _PHYSICSCOMPPLAYERCIRCLE_H

#include "PhysicsComp.h"
#include "VolumeCircle.h"

class PhysicsCompPlayerCircle : public PhysicsComp {
public:
	PhysicsCompPlayerCircle(CEvent *e = NULL) : PhysicsComp(e) {
		mVolume = new VolumeCircle(25.0f);
		mGravity = 0.5f;
	}
	void update(Actor &actor) override {}
	void update(Actor &actor, std::vector<Actor> &objects) override {}
	void update(Actor &actor, std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) override;
	void setPos(Actor &actor, float x, float y) override;
	void setInitialPos(Actor &actor, float x, float y) override;
};

#endif /* _PHYSICSCOMPPLAYERCIRCLE_H */