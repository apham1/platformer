#include "Locator.h"

Audio *Locator::mService;

/** @return the audio using the locator.
*/
Audio* Locator::getAudio() {
	return Locator::mService;
}

/** Provides the service to the locator.
	@param service The service
*/
void Locator::provide(Audio *service) {
	mService = service;
}