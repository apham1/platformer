#ifndef _ACTORFACTORY_H
#define _ACTORFACTORY_H

#include "Animation.h"
#include "InputCompPlayer.h"
#include "GraphicsCompStatic.h"
#include "GraphicsCompPlayer.h"
#include "GraphicsCompAnimated.h"
#include "PhysicsCompPlayer.h"
#include "PhysicsCompPlayerCircle.h"
#include "PhysicsCompStaticRect.h"
#include "PhysicsCompStaticCircle.h"
#include "PhysicsCompProjectileCircle.h"
#include "CEventWall.h"
#include "CEventWallTop.h"
#include "CEventWallMiddle.h"
#include "CEventWallBottom.h"
#include "CEventField.h"
#include "CEventDeath.h"
#include "CEventWin.h"
#include "CEventPlatform.h"
#include "CEventHit.h"
#include "CEventProjectile.h"

class ActorFactory {
public:
	static Actor *createPlayer(float x, float y);
	static Actor *createSmallWall(float x, float y);
	static Actor *createHorizontalWall(float x, float y);
	static Actor *createVerticalWall(float x, float y);
	static Actor *createMediumSling(float x, float y);
	static Actor *createLargeSling(float x, float y);
	static Actor *createExit(float x, float y);
	static Actor *createDeathFloor(float x, float y);
	static Actor *createDeathBlock(float x, float y);
	static Actor *createPlatformBox(float x, float y);
	static Actor *createGunHorizontal(float x, float y);
	static Actor *createGunUp(float x, float y);
	static Actor *createGunDown(float x, float y);
	static Actor *createNinjaStar(float x, float y);
	static Actor *createPlatformSmall(float x, float y);
	static Actor *createPlatformBig(float x, float y);
	static Actor *createPlatformThin(float x, float y);
	static Actor *createPillar(float x, float y);
	static Actor *createCannon(float x, float y);
	static Actor *createLightningBall(float x, float y);
	static Actor *createFloor(float x, float y);
	static Actor *createFloorConnector(float x, float y);
	static Actor *createFloorLeftEnd(float x, float y);
	static Actor *createFloorRightEnd(float x, float y);
	static Actor *createWallPiece(float x, float y);
	static Actor *createWallConnector(float x, float y);
	static Actor *createWallTop(float x, float y);
	static Actor *createWallBottom(float x, float y);
	static Actor *createShockwave(float x, float y);
	static Actor *createShockwaveGenerator(float x, float y);
	static Actor *createWidePlatform(float x, float y);
	static Actor *createFloorSmall(float x, float y);
	static Actor *createCrate(float x, float y);
	
	static void free();
	static bool loadMedia(SDL_Renderer *renderer);
	static Actor *createActor(int ID, float x = 0.0f, float y = 0.0f, Direction direction = RIGHT);
	static void setData(Actor *actor, int ID, Direction direction);

private:
	static Texture mPlayerTexture, mWallTexture, mBoxTexture, mWallHorizontalTexture,
				   mWallVerticalTexture, mDeathBlockTexture, mExitTexture,
				   mDeathFloorTexture, mSlingMediumTexture, mSlingLargeTexture,
				   mPlatformTexture, mPlatformSmallTexture, mPlatformBigTexture, mPlatformThinTexture,
				   mPillarTexture, mNinjaStarTexture, mCrossbowTexture, mCrossbowUpTexture,
				   mCrossbowDownTexture, mCannonTexture, mLightningBallTexture, mWidePlatformTexture,
				   mFloorTexture, mFloorConnTexture, mFloorRightEndTexture, mFloorLeftEndTexture,
				   mWallPieceTexture, mWallConnTexture, mWallTopTexture, mWallBottomTexture,
				   mThinPlatformTexture, mPlayerDuckingTexture, mShockwaveTexture, mGeneratorTexture,
				   mFloorSmallTexture, mCrateTexture;
};

#endif /* _ACTORFACTORY_H */