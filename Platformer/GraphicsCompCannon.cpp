#include "GraphicsCompCannon.h"
#include "Actor.h"

GraphicsCompCannon::GraphicsCompCannon(Texture *texture, const char *animationSeq) : GraphicsCompAnimated(texture, animationSeq) {

}

/** Delegates the render call to the texture class
	@param actor The actor used to render
	@param renderer The rendering state
	@param camera The game camera
	@param tick The current game tick
*/
void GraphicsCompCannon::render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) {
	if (actor.gun()) {
		if (actor.gun()->mCooldown - actor.gun()->mCount == 13) {
			mAnimation->setSequence("shoot");
		}
	}

	GraphicsCompAnimated::render(actor, renderer, camera, tick);
}