#ifndef _UIBUTTONTEXT_H
#define _UIBUTTONTEXT_H

#include "UIButton.h"

class UIButtonText : public UIButton {
public:
	UIButtonText(int x, int y, int width, int height) : UIButton(x, y, width, height) {
		mLabel = "";
		mLabelTexture = new Texture();
	}

	~UIButtonText() {
		if (mLabelTexture) {
			delete mLabelTexture;
			mLabelTexture = NULL;
		}
	}

	void render(SDL_Renderer *renderer, int scroll, int ticks) override {
		SDL_Rect buttonBackground = { x, y, width, height };
		if (mIsHovered) {
			SDL_SetRenderDrawColor(renderer, 0xFC, 0xFF, 0x6D, 0xFF);
		} else {
			SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		}
		SDL_RenderFillRect(renderer, &buttonBackground);

		if (mIsClicked) {
			SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x30, 0xFF);
		} else {
			SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
		}
		SDL_RenderDrawRect(renderer, &buttonBackground);

		mLabelTexture->render(renderer, x + width / 2 - mLabelTexture->getWidth() / 2, y + height / 2 - mLabelTexture->getHeight() / 2);
	}

	void setLabel(SDL_Renderer *renderer, TTF_Font *font, std::string label) {
		mLabel = label;
		if (mLabel != "") {
			mLabelTexture->loadText(renderer, font, mLabel);
		} else {
			mLabelTexture->loadText(renderer, font, " ");
		}
	}

private:
	std::string mLabel;
	Texture *mLabelTexture;
};

#endif /* _UIBUTTONACTOR_H */