#ifndef _TEXTURE_H
#define _TEXTURE_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <string>
#include <map>

enum Center {
	DEFAULT_CENTER_X = -1,
	DEFAULT_CENTER_Y = -1
};

struct AnimationSeq {
	int startFrame;
	int totalFrames;
};

class Texture {
	friend class Animation;

public:
	Texture();
	~Texture();

	bool loadFromFile(SDL_Renderer *renderer, const char *fileName, float scaleX = 1.0f, float scaleY = 1.0f, int centerX = DEFAULT_CENTER_X, int centerY = DEFAULT_CENTER_Y);
	bool loadText(SDL_Renderer* renderer, TTF_Font* font, std::string text, SDL_Color color = { 0, 0, 0 }, Uint32 wrapLength = 1920);
	void render(SDL_Renderer *renderer, int x, int y, float scale = 1.0f, SDL_RendererFlip flip = SDL_FLIP_NONE, SDL_Rect *clip = NULL, double angle = 0.0, SDL_Point* rotateCenter = NULL);
	void render(SDL_Renderer *renderer, int x, int y, int width, int height, float scale = 1.0f, SDL_RendererFlip flip = SDL_FLIP_NONE, SDL_Rect *clip = NULL, double angle = 0.0, SDL_Point* rotateCenter = NULL);
	void setAlpha(int alpha);

	int getWidth();
	int getHeight();
	int getFrameWidth();
	int getFrameHeight();
	float getScaleX();
	float getScaleY();

	void free();

private:
	bool loadAnimationData(const char *fileName);

	SDL_Texture *mTexture;
	int mWidth, mHeight;
	int mCenterX, mCenterY;
	float mScaleX, mScaleY;

	int mFrameWidth, mFrameHeight;
	int mInterval;
	std::map<std::string, AnimationSeq> mAnimations;
};

#endif /* _TEXTURE_H */