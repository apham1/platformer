#include <algorithm>
#include "EToolSelect.h"
#include "EScreenActorProperties.h"

EToolSelect::EToolSelect(int maxSelectedItems) {
	mMultiSelectBox = NULL;
	mClipboard.reserve(maxSelectedItems);
}

EToolSelect::~EToolSelect() {
	freeMultiSelectBox();
}

/** Handles non-event input.
    @param editor The editor screen.
*/
bool EToolSelect::handleInput(EditorScreen &editor) {
	return false;
}

/** Handles event input.
	@param editor The editor screen.
	@param e The event being handled.
*/
bool EToolSelect::handleInput(App &app, EditorScreen &editor, SDL_Event &e) {
	SDL_Keymod mouseMoveKey = KMOD_LCTRL;
	if ((e.type == SDL_MOUSEMOTION) && (SDL_GetModState() & mouseMoveKey)) {
		freeMultiSelectBox();
		if ((e.motion.state & SDL_BUTTON_LMASK)) {
			int xDist = e.motion.xrel;
			int yDist = e.motion.yrel;
			if ((SDL_GetModState() & KMOD_LALT) && (SDL_GetModState() & KMOD_LSHIFT)) {
				xDist = 0;
			} else if (SDL_GetModState() & KMOD_LALT) {
				yDist = 0;
			}
			moveSelectedItems(editor, xDist, yDist);
			return true;
		}
	} else if (e.type == SDL_MOUSEBUTTONDOWN && !(SDL_GetModState() & mouseMoveKey)) {
		if (e.button.button == SDL_BUTTON_LEFT) {
			VolumeRect mouse(1.0f, 1.0f);
			mouse.setInitialPosition(editor.mMouseX + editor.mCamera.x, editor.mMouseY + editor.mCamera.y);
			if (!updateSelectedItems(editor, &mouse)) {
				mMultiSelectBox = new VolumeRect(1.0f, 1.0f);
				mMultiSelectBox->setInitialPosition(static_cast<float>(e.button.x) + editor.mCamera.x, static_cast<float>(e.button.y) + editor.mCamera.y);
			}
			return true;
		} else if (e.button.button == SDL_BUTTON_RIGHT) {
			if (editor.mSelectedItems.size() == 1 && editor.mSelectedItems.front().actor->gun()) {
				app.transitionScreen(new EScreenActorProperties(&editor));
			}
			return true;
		}
	} else if (e.type == SDL_MOUSEBUTTONUP) {
		if (e.button.button == SDL_BUTTON_LEFT) {
			if (mMultiSelectBox) {
				updateSelectedItems(editor, mMultiSelectBox, true);
				freeMultiSelectBox();
			}
		}
	} else if (e.type == SDL_MOUSEMOTION) {
		if (mMultiSelectBox) {
			mMultiSelectBox->w += e.motion.xrel;
			mMultiSelectBox->h += e.motion.yrel;
		}
	} else if (e.type == SDL_KEYDOWN) {
		if ((e.key.keysym.mod & KMOD_LCTRL) && (e.key.keysym.mod & KMOD_LSHIFT)) {
			switch (e.key.keysym.sym) {
				case SDLK_a:
					editor.mSelectedItems.clear();
					return true;
			}
		}
		if (e.key.keysym.mod & KMOD_LCTRL) {
			switch (e.key.keysym.sym) {
				case SDLK_c:
					copySelectedItems(editor);
					return true;
				case SDLK_v:
					pasteFromClipboard(editor);
					return true;
				case SDLK_x:
					copySelectedItems(editor);
					eraseSelectedItems(editor);
					return true;
			}
		}
		if (e.key.keysym.mod & KMOD_LSHIFT) {
			switch (e.key.keysym.sym) {
				case SDLK_UP:
					moveSelectedItems(editor, 0, -10);
					return true;
				case SDLK_DOWN:
					moveSelectedItems(editor, 0, 10);
					return true;
				case SDLK_LEFT:
					moveSelectedItems(editor, -10, 0);
					return true;
				case SDLK_RIGHT:
					moveSelectedItems(editor, 10, 0);
					return true;
			}
		}
		switch (e.key.keysym.sym) {
			case SDLK_UP:
				moveSelectedItems(editor, 0, -1);
				return true;
			case SDLK_DOWN:
				moveSelectedItems(editor, 0, 1);
				return true;
			case SDLK_LEFT:
				moveSelectedItems(editor, -1, 0);
				return true;
			case SDLK_RIGHT:
				moveSelectedItems(editor, 1, 0);
				return true;
			case SDLK_DELETE:
				eraseSelectedItems(editor);
				return true;
			case SDLK_f:
				flipSelectedItems(editor);
				return true;
			case SDLK_l:
				alignTopSelectedItems(editor);
				return true;
			case SDLK_k:
				alignVerticalSelectedItems(editor);
				return true;

		}
		if (editor.mSelectedItems.size() == 1) {
			switch (e.key.keysym.sym) {
				case SDLK_PAGEUP:
					if (editor.mSelectedItems[0].index < static_cast<int>(editor.mActors.size() - 1)) {
						reorderSelectedItem(editor, editor.mSelectedItems[0].index + 1);
					}
					return true;
				case SDLK_PAGEDOWN:
					if (editor.mSelectedItems[0].index > 0) {
						reorderSelectedItem(editor, editor.mSelectedItems[0].index - 1);
					}
					return true;
				case SDLK_HOME:
					if (editor.mSelectedItems[0].index < static_cast<int>(editor.mActors.size()) - 1) {
						reorderSelectedItem(editor, editor.mActors.size() - 1);
					}
					return true;
				case SDLK_END:
					if (editor.mSelectedItems[0].index > 0) {
						reorderSelectedItem(editor, 0);
					}
					return true;
			}
		}
	}
	
	return false;
}

/** Update the currently selected actor and actor index with the actor positioned under the mouse.
	@param editor The editor screen.
	@return true if the selected actor list was modified. 
*/
bool EToolSelect::updateSelectedItems(EditorScreen &editor, VolumeRect *selectBox, bool isMultiSelect) {
	if (!(SDL_GetModState() & KMOD_LSHIFT)) {
		editor.mSelectedItems.clear();
	}

	bool isListModified = false;
	if (editor.mSelectedItems.size() < editor.mMaxSelectedItems) {
		int index = editor.mActors.size() - 1;
		std::list<Actor>::reverse_iterator it;
		for (it = editor.mActors.rbegin(); it != editor.mActors.rend(); ++it) {
			Volume *actor = (*it).physics()->getVolume();
			if (actor->isColliding(selectBox)) {
				if (!isMultiSelect) {
					if (!editor.eraseSelectedItem(index)) {
						editor.mSelectedItems.push_back(SelectedItem(&(*it), index));
					}
					return true;
				} else {
					if (!editor.selectedItemsContains(index)) {
						editor.mSelectedItems.push_back(SelectedItem(&(*it), index));
						isListModified = true;
						if (editor.mSelectedItems.size() >= editor.mMaxSelectedItems) {
							break;
						}
					}
					index--;
				}
			} else {
				index--;
			}
		}
	} else {
		SDL_Log("Cannot select anymore items! (max: %i)", editor.mMaxSelectedItems);
	}
	return isListModified;
}

/** Move the selected items' positions. 
    @param xDist distance to move along the x-axis. 
	@param yDist distance to move along the y-axis.
*/
void EToolSelect::moveSelectedItems(EditorScreen &editor, int xDist, int yDist) {
	for (std::vector<SelectedItem>::iterator it = editor.mSelectedItems.begin(); it != editor.mSelectedItems.end(); ++it) {
		it->actor->setPos(it->actor->mPosX + xDist, it->actor->mPosY + yDist);
	}
}

/** Erase an element from the actor list.
    @param editor The editor screen. 
	@param index The index of the actor to erase.
*/
void EToolSelect::eraseSelectedItems(EditorScreen &editor) {
	for (std::vector<SelectedItem>::iterator it = editor.mSelectedItems.begin(); it != editor.mSelectedItems.end(); ++it) {
		editor.mActors.remove(*it->actor);
	}
	editor.mSelectedItems.clear();
}

/** Move the selected actor to a new position in the actor list. 
    @param editor The editor screen.
	@param index The index to move the actor to. 
*/
void EToolSelect::reorderSelectedItem(EditorScreen &editor, int index) {
	ActorData actorData = { editor.mSelectedItems[0].actor->actorID, editor.mSelectedItems[0].actor->mPosX, editor.mSelectedItems[0].actor->mPosY, editor.mSelectedItems[0].actor->mDirection };
	if (editor.mSelectedItems[0].actor->gun()) {
		actorData.projectileCooldown = editor.mSelectedItems[0].actor->gun()->mCooldown;
		actorData.projectileLifespan = editor.mSelectedItems[0].actor->gun()->mProjectileLifespan;
		actorData.delay = editor.mSelectedItems[0].actor->gun()->mDelay;
	}
	eraseSelectedItems(editor);
	std::list<Actor>::iterator selectedActorIter = std::next(editor.mActors.begin(), index);
	editor.mActors.insert(std::next(editor.mActors.begin(), index), *ActorFactory::createActor(actorData.ID, actorData.x, actorData.y, actorData.direction));
	Actor *reorderedActor = &(*std::next(editor.mActors.begin(), index));
	editor.mSelectedItems.push_back(SelectedItem(reorderedActor, index));
}

void EToolSelect::copySelectedItems(EditorScreen &editor) {
	mClipboard.clear();
	for (std::vector<SelectedItem>::iterator it = editor.mSelectedItems.begin(); it != editor.mSelectedItems.end(); ++it) {
		ActorData data;
		data.ID = it->actor->actorID;
		data.x = it->actor->mPosX;
		data.y = it->actor->mPosY;
		data.direction = it->actor->mDirection;
		
		if (it->actor->gun()) {
			data.projectileCooldown = it->actor->gun()->mCooldown;
			data.projectileLifespan = it->actor->gun()->mProjectileLifespan;
			data.delay = it->actor->gun()->mDelay;
		}

		mClipboard.push_back(data);
	}
}

void EToolSelect::pasteFromClipboard(EditorScreen &editor) {
	editor.mSelectedItems.clear();
	for (std::vector<ActorData>::iterator it = mClipboard.begin(); it != mClipboard.end(); ++it) {
		Actor *actor = ActorFactory::createActor(it->ID, it->x + 10.0f, it->y, it->direction);
		if (actor->gun()) {
			actor->gun()->setCooldown(it->projectileCooldown);
			actor->gun()->setProjectileLifespan(it->projectileLifespan);
			actor->gun()->setDelay(it->delay);
		}
		editor.mActors.push_back(*actor);
		editor.mSelectedItems.push_back(SelectedItem(&editor.mActors.back(), editor.mActors.size() - 1));
	}
}

void EToolSelect::flipSelectedItems(EditorScreen &editor) {
	for (std::vector<SelectedItem>::iterator it = editor.mSelectedItems.begin(); it != editor.mSelectedItems.end(); ++it) {
		if (it->actor->mDirection == LEFT) {
			it->actor->mDirection = RIGHT;
		} else if (it->actor->mDirection == RIGHT) {
			it->actor->mDirection = LEFT;
		}
	}
}

/** Align the top of all selected items. Items will be aligned with the last selected item. 
    @param editor The editor screen.
*/
void EToolSelect::alignTopSelectedItems(EditorScreen &editor) {
	int size = editor.mSelectedItems.size();
	if (size > 1) {
		int alignWith = editor.mSelectedItems[size - 1].actor->physics()->getVolume()->getRenderRect().y;
			for (std::vector<SelectedItem>::iterator it = editor.mSelectedItems.begin(); it != editor.mSelectedItems.end(); ++it) {
				int offset = alignWith - it->actor->physics()->getVolume()->getRenderRect().y;
				it->actor->setPosY(it->actor->mPosY + offset);
			}
	}
}

/** Align the center of all selected items along the same X coordinate. Items will be aligned with the last selected item.
	@param editor The editor screen.
*/
void EToolSelect::alignVerticalSelectedItems(EditorScreen &editor) {
	int size = editor.mSelectedItems.size();
	if (size > 1) {
		float alignWith = editor.mSelectedItems[size - 1].actor->mPosX;
		for (std::vector<SelectedItem>::iterator it = editor.mSelectedItems.begin(); it != editor.mSelectedItems.end(); ++it) {
			it->actor->setPosX(alignWith);
		}
	}
}

/** Updates the screen.
	@param editor The editor screen.
	@param renderer The SDL renderer used to update text.
*/
void EToolSelect::update(EditorScreen &editor, SDL_Renderer *renderer) {

}

/** Renders actors and textures onto the screen.
	@param editor The editor screen.
	@param renderer The SDL renderer used to render textures onto the screen.
	@param screenWidth The total width of the screen.
	@param screenHeight The total height of the screen.
	@param currentTicks The current tick of the game.
*/
void EToolSelect::render(EditorScreen &editor, SDL_Renderer *renderer, int screenWidth, int screenHeight, int currentTicks) {
	SDL_Color highlightColor = { 0xFF, 0x00, 0x00, 0xFF };
	if (!editor.mSelectedItems.empty()) {
		for (std::vector<SelectedItem>::iterator it = editor.mSelectedItems.begin(); it != editor.mSelectedItems.end(); ++it) {
			SDL_Rect box = it->actor->physics()->getVolume()->getRenderRect();
			drawBoldBox(editor, renderer, &box, 3, &highlightColor);
		}
	}
	if (mMultiSelectBox) {
		drawBoldBox(editor, renderer, &mMultiSelectBox->getRenderRect(), 3, &highlightColor);
	}
}

void EToolSelect::drawBoldBox(EditorScreen &editor, SDL_Renderer *renderer, SDL_Rect *box, int thickness, SDL_Color *color) {
	SDL_SetRenderDrawColor(renderer, color->r, color->g, color->b, color->a);
	box->x -= editor.mCamera.x;
	box->y -= editor.mCamera.y;
	SDL_RenderDrawRect(renderer, box);

	for (int i = 0; i < thickness - 1; ++i) {
		box->x++;
		box->y++;
		box->w -= 2;
		box->h -= 2;
		SDL_RenderDrawRect(renderer, box);
	}
}

void EToolSelect::freeMultiSelectBox() {
	if (mMultiSelectBox) {
		delete mMultiSelectBox;
		mMultiSelectBox = NULL;
	}
}