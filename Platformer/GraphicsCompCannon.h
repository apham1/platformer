#ifndef _GRAPHICSCOMPCANNON_H
#define _GRAPHICSCOMPCANNON_H

#include "GraphicsCompAnimated.h"
#include "Animation.h"

class GraphicsCompCannon : public GraphicsCompAnimated {
public:
	GraphicsCompCannon(Texture *texture, const char *animationSeq);
	void render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) override;
};

#endif /* _GRAPHICSCOMPCANNON_H */
