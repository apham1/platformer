#ifndef _CEVENTWALL_H
#define _CEVENTWALL_H

#include "CEvent.h"

class CEventWall : public CEvent {
public:
	void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override;
	void triggerEventExit(Actor &actor, Actor &object) override {};
};

#endif /* _CEVENTWALL_H */