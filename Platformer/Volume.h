#ifndef _VOLUME_H
#define _VOLUME_H

#include <SDL.h>
#include "GameMath.h"

struct CollisionData {
	float xPenetration;
	float yPenetration;

	CollisionData(float xPen, float yPen) {
		xPenetration = xPen;
		yPenetration = yPen;
	}
};

class VolumeRect;
class VolumeCircle;

class Volume {
public:
	Volume() {
		current = 0;
		mScale = 1.0f;
	}

	virtual CollisionData *isColliding(VolumeRect *rect) = 0;
	virtual CollisionData *isColliding(VolumeCircle *circle) = 0;

	void setPosition(float x, float y);
	void setInitialPosition(float x, float y);

	// currently does nothing
	void setScale(float scale) {
		mScale = scale;
	}
	void rollbackPosition();

	virtual SDL_Rect getRenderRect() = 0;

	float x();
	float y();
	float xOld();
	float yOld();

	virtual float getWidth() = 0;
	virtual float getHeight() = 0;

protected:
	float xPos[2];
	float yPos[2];

	float mScale;

	int current;

	CollisionData *isColliding(VolumeCircle* circle, VolumeRect *rect);
	CollisionData *isColliding(VolumeRect *rect, VolumeCircle* circle);
};

#endif /* _VOLUME_H */