#ifndef _PHYSICSCOMPPROJECTILECIRCLE_H
#define _PHYSICSCOMPPROJECTILECIRCLE_H

#include "PhysicsComp.h"
#include "VolumeCircle.h"

class PhysicsCompProjectileCircle : public PhysicsComp {
public:
	PhysicsCompProjectileCircle(float radius, float speed, CEvent *e = NULL) : PhysicsComp(e) {
		mVolume = new VolumeCircle(radius);
		mSpeed = speed;
	}

	void update(Actor &actor) override;
	void update(Actor &actor, std::vector<Actor> &objects) override {}
	void update(Actor &actor, std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) override {}
	void setPos(Actor &actor, float x, float y) override;
	void setInitialPos(Actor &actor, float x, float y) override;

	float mSpeed;
};
#endif /* _PHYSICSPROJECTILESTATICCIRCLE_H */
