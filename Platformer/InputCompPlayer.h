#ifndef _INPUTCOMPPLAYER_H
#define _INPUTCOMPPLAYER_H

#include <SDL.h>
#include "InputComp.h"

class InputCompPlayer : public InputComp {
public:
	InputCompPlayer();
	void update(Actor &actor) override;
	bool update(Actor &actor, SDL_Event &e) override;

private:
	void stand(Actor &actor);
	void duck(Actor &actor);

	const Uint8 *mKeyStates;
};

#endif /* _INPUTCOMPPLAYER_H */
