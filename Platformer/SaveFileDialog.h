#ifndef _SAVEFILEDIALOG_H
#define _SAVEFILEDIALOG_H

#include <Windows.h>
#include <Commdlg.h>
#include <tchar.h>

class SaveFileDialog
{
public:
	SaveFileDialog(void);

	TCHAR*DefaultExtension;
	TCHAR*FileName;
	TCHAR*Filter;
	int FilterIndex;
	int Flags;
	TCHAR*InitialDir;
	HWND Owner;
	TCHAR*Title;

	bool saveFile();
};

#endif /* _SaveFILEDIALOG_H */