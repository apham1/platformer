#ifndef _APP_H
#define _APP_H

#include <SDL.h>
#include <SDL_mixer.h>
#include <vector>
#include <stack>
#include "Screen.h"

const double MS_PER_UPDATE = 1000.0 / 64.0;

class GameScreen;

class App {
	friend class GameScreen;
	friend class TitleScreen;
	friend class EditorScreen;
	friend class EToolSelect;
	friend class EScreenActorPicker;
	friend class EScreenActorProperties;
	friend class EScreenHelp;

public:
	App();
	~App();

	int init();
	void run();

private:
	void handleInput();
	void update();
	void render(int currentTicks);
	bool transitionScreen(Screen *screen);
	void popScreen();

	SDL_Window *mWindow;
	SDL_Renderer *mRenderer;
	
	int mScreenWidth = 1366;
	int mScreenHeight = 768;
	bool mQuit;

	std::stack<Screen*> mScreen;
};

#endif /* _APP_H */