#include "CEventWall.h"
#include "Actor.h"

/** Triggers in the event of a collision, rolls back the actors position and handles different collision states.
	@param actor The actor colliding with an object
	@param object The object the actor is colliding with
	@param cData The collision data that determines what side of the object the collision is with.
*/
void CEventWall::triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) {
	actor.physics()->getVolume()->rollbackPosition();

	actor.setPos(actor.mPosX - cData->xPenetration, actor.mPosY - cData->yPenetration);

	// Top side collision
	if (cData->yPenetration > 0.0f) {
   		actor.mAirborne = false;
		actor.mVelY = 0.0f;

		actor.mSling.xSpeed = 0.0f;
		actor.mSling.ySpeed = 0.0f;
	// Bottom side collision
	} else if (cData->yPenetration < 0.0f && std::abs(cData->xPenetration) < 1.0f) {
		actor.mVelY = 0.0f;
		actor.mSling.ySpeed = 0.0f;
	}

	if (cData->xPenetration != 0.0f) {
		actor.mVelX = 0.0f;
		actor.mSling.xSpeed = 0.0f;
	}

	actor.mSling.isSlinging = false;
	actor.mSling.hasSlung = false;
	actor.mIsKnockedBack = false;
}
