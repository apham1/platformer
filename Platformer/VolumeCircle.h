#ifndef _VOLUMECIRCLE_H
#define _VOLUMECIRCLE_H

#include <math.h>
#include <SDL.h>
#include "Volume.h"
#include "VolumeRect.h"

class VolumeCircle : public Volume {
public:
	VolumeCircle(float radius) : Volume() {
		r = radius;
	}

	/** Checks if the objects are colliding.
		@param rect
		@return the collision data
	*/
	CollisionData *isColliding(VolumeRect *rect) override {
		return Volume::isColliding(this, rect);
	}

	/** Checks if two circles are colliding.
		@param circle
		@return the collision data
	*/
	CollisionData *isColliding(VolumeCircle *circle) override {
		float distance = sqrt(GameMath::distanceSquared(x(), y(), circle->x(), circle->y()));
		float combinedRadius = r + circle->r;

		if (distance < combinedRadius) {
			float penetration = combinedRadius - distance;
			float angle = atan2(y() - circle->y(), x() - circle->x());
			return new CollisionData(penetration * cos(angle), penetration * sin(angle));
		}

		return NULL;
	}

	/** @return a rectangle that can be used to render to display the position of the volume
	*/
	SDL_Rect getRenderRect() override {
		SDL_Rect renderRect = { static_cast<int>(x() - r), static_cast<int>(y() - r), static_cast<int>(r + r), static_cast<int>(r + r) };
		return renderRect;
	}

	float getWidth() override {
		return r * 2;
	}

	float getHeight() override {
		return r * 2;
	}

	float r;
};

#endif /* _VOLUMECIRCLE_H */