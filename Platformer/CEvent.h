#ifndef _CEVENT_H
#define _CEVENT_H

#include "Volume.h"

class Actor;

class CEvent {
public:
	virtual void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) = 0;
	virtual void triggerEventExit(Actor &actor, Actor &object) = 0;
};

#endif /* _CEVENT_H */