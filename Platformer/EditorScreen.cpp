#include "EditorScreen.h"
#include "EToolCreate.h"
#include "EToolSelect.h"
#include "TitleScreen.h"
#include "GameScreen.h"
#include "App.h"
#include "ActorFactory.h"
#include "EScreenActorPicker.h"
#include "EScreenActorProperties.h"
#include "EScreenHelp.h"

EditorScreen::EditorScreen(const char *levelFile, int cameraX, int cameraY) : Screen() {
	if (!std::string(levelFile).empty()) {
		mInitialLoad = new InitialLoadInfo(levelFile, cameraX, cameraY);
	}
}

/** Activates on entry into the screen, initializes values and loads media used in the screen.
	@param app The main application
*/
bool EditorScreen::onEntry(App &app) {
	if (!loadMedia(app)) {
		return false;
	}

	mToolCreate = new EToolCreate();
	mToolSelect = new EToolSelect(mMaxSelectedItems);

	mMouseX = 0;
	mMouseY = 0;

	mPlayer = NULL;

	mKeyStates = SDL_GetKeyboardState(NULL);

	mSelectedItems.reserve(mMaxSelectedItems);

	reset(app);

	if (mInitialLoad) {
		loadLevelFromFile(app, mInitialLoad->fileName.c_str());
		mCamera.x = mInitialLoad->cameraX;
		mCamera.y = mInitialLoad->cameraY;
	}

	return true;
}

/** Activates upon exiting the screen, freeing up memory used in it.
*/
void EditorScreen::onExit() {
	delete mToolCreate;
	delete mToolSelect;

	mActors.clear();
	ActorFactory::free();

	TTF_CloseFont(mFont);
	mFont = NULL;
}

/** Update the window title with the currently open file. 
*/
void EditorScreen::updateWindowTitle(App &app) {
	std::string windowFullTitle = "";
	windowFullTitle.append(mWindowTitle).append(" - ");
	if (mCurrentFile.empty()) {
		windowFullTitle.append("New level");
	} else {
		windowFullTitle.append(mCurrentFile);
	}
	if (mHasEdit) {
		windowFullTitle.append("*");
	}
	SDL_SetWindowTitle(app.mWindow, windowFullTitle.c_str());
}

/** Changes the current object based on the object's ID.
*/
void EditorScreen::changeObject(int actorID, Direction direction) {
	mCreateActorID = actorID;
	mCreateActor = ActorFactory::createActor(mCreateActorID, 0, 0, direction);

	switch (mCreateActorID) {
		case 0:
			mCreateActorName = "Small Wall";
			break;
		case 1:
			mCreateActorName = "Horizontal Wall";
			break;
		case 2:
			mCreateActorName = "Vertical Wall";
			break;
		case 3:
			mCreateActorName = "Platform Box";
			break;
		case 4:
			mCreateActorName = "Medium Sling";
			break;
		case 5:
			mCreateActorName = "Large Sling";
			break;
		case 6:
			mCreateActorName = "Death Block";
			break;
		case 7:
			mCreateActorName = "Death Floor";
			break;
		case 8:
			mCreateActorName = "Exit";
			break;
		case 9:
			mCreateActorName = "Crossbow";
			break;
		case 10:
			mCreateActorName = "Thin Platform";
			break;
	}
}

/** Loads all textures.
	@return true if all media successfully loaded
*/
bool EditorScreen::loadMedia(App &app) {
	if (!ActorFactory::loadMedia(app.mRenderer)) {
		return false;
	}

	if (!mTempBackground.loadFromFile(app.mRenderer, "testbackground2")) {
		return false;
	}

	mFont = TTF_OpenFont("Fonts/OpenSans-Regular.ttf", 14);
	if (!mFont) {
		return false;
	}

	return true;
}

/** Updates the screen.
*/
void EditorScreen::update(App &app) {
	int x, y;
	SDL_GetMouseState(&x, &y);
	mMouseX = static_cast<float>(x);
	mMouseY = static_cast<float>(y);
	
	mMouseText.loadText(app.mRenderer, mFont, ("Mouse Position: (" + std::to_string(static_cast<int>(mMouseX) + mCamera.x) + ", " + std::to_string(static_cast<int>(mMouseY) + mCamera.y) + ")"));
	mCameraText.loadText(app.mRenderer, mFont, ("Camera Position: (" + std::to_string(mCamera.x) + ", " + std::to_string(mCamera.y) + ")"));

	mCurrentTool->update(*this, app.mRenderer);
}

/** Renders actors and textures onto the screen.
*/
void EditorScreen::render(App &app, int currentTicks) {
	SDL_RenderClear(app.mRenderer);

	int linesPerMarker = 10;
	int gridBlockDim = 25;
	int gridLineX = (mCamera.x / gridBlockDim) * gridBlockDim;

	for (int i = 0; i < (app.mScreenWidth / gridBlockDim) + 2; ++i) {
		if (gridLineX == 0) {
			SDL_SetRenderDrawColor(app.mRenderer, 0xFF, 0x00, 0x00, 0xFF);
			SDL_RenderDrawLine(app.mRenderer, gridLineX - mCamera.x - 1, 0, gridLineX - mCamera.x - 1, app.mScreenHeight);
			SDL_RenderDrawLine(app.mRenderer, gridLineX - mCamera.x + 1, 0, gridLineX - mCamera.x + 1, app.mScreenHeight);
		} else if (gridLineX % (gridBlockDim * linesPerMarker) == 0) {
			SDL_SetRenderDrawColor(app.mRenderer, 0xFF, 0x00, 0x00, 0xFF);
		} else {
			SDL_SetRenderDrawColor(app.mRenderer, 0x00, 0x00, 0x00, 0xFF);
		}
		SDL_RenderDrawLine(app.mRenderer, gridLineX - mCamera.x, 0, gridLineX - mCamera.x, app.mScreenHeight);
		gridLineX += gridBlockDim;
	}

	int gridLineY = (mCamera.y / gridBlockDim) * gridBlockDim;

	for (int i = 0; i < (app.mScreenHeight / gridBlockDim) + 2; ++i) {
		if (gridLineY == 0) {
			SDL_SetRenderDrawColor(app.mRenderer, 0xFF, 0x00, 0x00, 0xFF);
			SDL_RenderDrawLine(app.mRenderer, 0, gridLineY - mCamera.y - 1, app.mScreenWidth, gridLineY - mCamera.y - 1);
			SDL_RenderDrawLine(app.mRenderer, 0, gridLineY - mCamera.y + 1, app.mScreenWidth, gridLineY - mCamera.y + 1);
		} else if (gridLineY % (gridBlockDim * linesPerMarker) == 0) {
			SDL_SetRenderDrawColor(app.mRenderer, 0xFF, 0x00, 0x00, 0xFF);
		} else {
			SDL_SetRenderDrawColor(app.mRenderer, 0x00, 0x00, 0x00, 0xFF);
		}
		SDL_RenderDrawLine(app.mRenderer, 0, gridLineY - mCamera.y, app.mScreenWidth, gridLineY - mCamera.y);
		gridLineY += gridBlockDim;
	}

	std::list<Actor>::iterator it;
	for (it = mActors.begin(); it != mActors.end(); ++it) {
		it->render(app.mRenderer, &mCamera, currentTicks);
		
		if (it->gun()) {
			SDL_SetRenderDrawBlendMode(app.mRenderer, SDL_BLENDMODE_BLEND);
			SDL_SetRenderDrawColor(app.mRenderer, 0x00, 0xFF, 0x00, 122);
			switch (it->gun()->mProjectileType) {
				case 0:
					drawProjectileIndicator(app, *it);
					break;
				case 1:
					drawShockwaveIndicator(app, *it);
					break;
			}
		}
	}

	if (mPlayer) {
		mPlayer->render(app.mRenderer, &mCamera, currentTicks);
	}

	// Draws a box around the text
	SDL_Rect positionBox = { app.mScreenWidth - 220, app.mScreenHeight - 50, 220, 50 };
	SDL_SetRenderDrawColor(app.mRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderFillRect(app.mRenderer, &positionBox);
	SDL_SetRenderDrawColor(app.mRenderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderDrawRect(app.mRenderer, &positionBox);

	// Renders text
	mMouseText.render(app.mRenderer, positionBox.x + 3, positionBox.y);
	mCameraText.render(app.mRenderer, positionBox.x + 3, positionBox.y + mCameraText.getHeight());

	mCurrentTool->render(*this, app.mRenderer, app.mScreenWidth, app.mScreenHeight, currentTicks);
}

void EditorScreen::drawProjectileIndicator(App &app, Actor &it) {
	int distance = it.gun()->mProjectileLifespan * 10;
	if (it.mDirection == LEFT || it.mDirection == UP) {
		distance = -distance;
	}

	int projectileOffsetX = it.gun()->mProjectileSpawnX;
	int projectileOffsetY = it.gun()->mProjectileSpawnY;
	if (it.mDirection == LEFT) {
		projectileOffsetX = -projectileOffsetX;
	}
	int projectilePosX = static_cast<int>(it.mPosX) + projectileOffsetX;
	int projectilePosY = static_cast<int>(it.mPosY) + projectileOffsetY;
	if (it.mDirection == LEFT || it.mDirection == RIGHT) {
		SDL_RenderDrawLine(app.mRenderer, projectilePosX - mCamera.x, projectilePosY - mCamera.y - 1, projectilePosX + distance - mCamera.x, projectilePosY - mCamera.y - 1);
		SDL_RenderDrawLine(app.mRenderer, projectilePosX - mCamera.x, projectilePosY - mCamera.y, projectilePosX + distance - mCamera.x, projectilePosY - mCamera.y);
		SDL_RenderDrawLine(app.mRenderer, projectilePosX - mCamera.x, projectilePosY - mCamera.y + 1, projectilePosX + distance - mCamera.x, projectilePosY - mCamera.y + 1);
	} else if (it.mDirection == UP || it.mDirection == DOWN) {
		SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) - mCamera.x, static_cast<int>(it.mPosY) - mCamera.y - 1, static_cast<int>(it.mPosX) - mCamera.x, static_cast<int>(it.mPosY) + distance - mCamera.y - 1);
		SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) - mCamera.x, static_cast<int>(it.mPosY) - mCamera.y, static_cast<int>(it.mPosX) - mCamera.x, static_cast<int>(it.mPosY) + distance - mCamera.y);
		SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) - mCamera.x, static_cast<int>(it.mPosY) - mCamera.y + 1, static_cast<int>(it.mPosX) - mCamera.x, static_cast<int>(it.mPosY) + distance - mCamera.y + 1);
	}
}

void EditorScreen::drawShockwaveIndicator(App &app, Actor &it) {
	SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) - 125 - mCamera.x, static_cast<int>(it.mPosY) - 1 - mCamera.y, static_cast<int>(it.mPosX) + 125 - mCamera.x, static_cast<int>(it.mPosY) - 1 - mCamera.y);
	SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) - 125 - mCamera.x, static_cast<int>(it.mPosY) - mCamera.y, static_cast<int>(it.mPosX) + 125 - mCamera.x, static_cast<int>(it.mPosY) - mCamera.y);
	SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) - 125 - mCamera.x, static_cast<int>(it.mPosY) + 1 - mCamera.y, static_cast<int>(it.mPosX) + 125 - mCamera.x, static_cast<int>(it.mPosY) + 1 - mCamera.y);

	SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) - 1 - mCamera.x, static_cast<int>(it.mPosY) - 125 - mCamera.y, static_cast<int>(it.mPosX) - 1 - mCamera.x, static_cast<int>(it.mPosY) + 125 - mCamera.y);
	SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) - mCamera.x, static_cast<int>(it.mPosY) - 125 - mCamera.y, static_cast<int>(it.mPosX) - mCamera.x, static_cast<int>(it.mPosY) + 125 - mCamera.y);
	SDL_RenderDrawLine(app.mRenderer, static_cast<int>(it.mPosX) + 1 - mCamera.x, static_cast<int>(it.mPosY) - 125 - mCamera.y, static_cast<int>(it.mPosX) + 1 - mCamera.x, static_cast<int>(it.mPosY) + 125 - mCamera.y);
}

void EditorScreen::saveLevelAs(App &app) {
	TCHAR startingPath[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, startingPath);

	SaveFileDialog sfd;

	sfd.FilterIndex = 1;
	sfd.Flags |= OFN_SHOWHELP;
	sfd.InitialDir = _T("C:\\Windows\\");
	sfd.Title = _T("Save Text File");

sfd.saveFile();

if (writeLevelDataToFile(sfd.FileName)) {
	MessageBox(0, sfd.FileName, _T("Level Saved"), MB_OK | MB_ICONINFORMATION);
	mCurrentFile = sfd.FileName;
}

SetCurrentDirectory(startingPath);
}

void EditorScreen::saveLevel(App &app) {
	if (!mCurrentFile.empty()) {
		writeLevelDataToFile(mCurrentFile.c_str());
	} else {
		saveLevelAs(app);
	}
}

bool EditorScreen::writeLevelDataToFile(const char *filePath) {
	SDL_RWops *file = SDL_RWFromFile(filePath, "w+b");
	if (file != NULL) {
		SDL_RWwrite(file, &mBackgroundID, sizeof(int), 1);
		SDL_RWwrite(file, &mMusicID, sizeof(int), 1);
		SDL_RWwrite(file, &mLevelWidth, sizeof(int), 1);
		SDL_RWwrite(file, &mLevelHeight, sizeof(int), 1);
		SDL_RWwrite(file, mLevelName.c_str(), sizeof(char) * 50, 1);
		SDL_RWwrite(file, mLevelName.c_str(), sizeof(char) * 150, 1);

		ActorData playerData;
		if (mPlayer) {
			playerData.ID = mPlayer->actorID;
			playerData.x = mPlayer->mPosX;
			playerData.y = mPlayer->mPosY;
			playerData.direction = mPlayer->mDirection;
		} else {
			playerData.ID = -2;
			playerData.x = 0;
			playerData.y = 0;
			playerData.direction = RIGHT;
		}
		SDL_RWwrite(file, &playerData, sizeof(ActorData), 1);

		std::list<Actor>::iterator it;
		for (it = mActors.begin(); it != mActors.end(); ++it) {
			ActorData data = { it->actorID, it->mPosX, it->mPosY, it->mDirection };
			if (it->gun()) {
				data.projectileCooldown = it->gun()->mCooldown;
				data.projectileLifespan = it->gun()->mProjectileLifespan;
				data.delay = it->gun()->mDelay;
			}
			SDL_RWwrite(file, &data, sizeof(ActorData), 1);
		}

		SDL_Log("%s saved", filePath);
		mHasEdit = false;
		SDL_RWclose(file);
		return true;
	}

	SDL_Log("Failed to save level");
	MessageBox(0, _T("Failed to save level!"), _T("Error"), MB_OK | MB_ICONERROR);
	return false;
}

void EditorScreen::openLevel(App &app) {
	TCHAR startingPath[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, startingPath);

	OpenFileDialog ofd;

	ofd.FilterIndex = 1;
	ofd.Flags |= OFN_SHOWHELP;
	ofd.InitialDir = _T("C:\\Windows\\");
	ofd.Title = _T("Open Text File");

	ofd.ShowDialog();

	loadLevelFromFile(app, ofd.FileName);

	SetCurrentDirectory(startingPath);
}

void EditorScreen::loadLevelFromFile(App &app, const char *filePath) {
	SDL_RWops *file = SDL_RWFromFile(filePath, "r+b");
	if (file != NULL) {
		saveBeforeClosePrompt(app);

		cleanUp();
		SDL_RWread(file, &mBackgroundID, sizeof(int), 1);
		SDL_RWread(file, &mMusicID, sizeof(int), 1);
		SDL_RWread(file, &mLevelWidth, sizeof(int), 1);
		SDL_RWread(file, &mLevelHeight, sizeof(int), 1);
		char levelName[50];
		SDL_RWread(file, &levelName, sizeof(char) * 50, 1);
		mLevelName = levelName;

		char temp[150];
		SDL_RWread(file, &temp, sizeof(char) * 150, 1);

		ActorData data;
		while (SDL_RWread(file, &data, sizeof(ActorData), 1)) {
			if (data.ID == -2) {
				mPlayer = ActorFactory::createActor(data.ID, data.x, data.y, data.direction);
			} else {
				Actor *actor = ActorFactory::createActor(data.ID, data.x, data.y, data.direction);
				if (data.projectileCooldown != -1 || data.projectileLifespan != -1 || data.delay != -1) {

					if (actor->gun()) {
						actor->gun()->setCooldown(data.projectileCooldown);
						actor->gun()->setProjectileLifespan(data.projectileLifespan);
						actor->gun()->setDelay(data.delay);
					}
				}
				mActors.push_back(*actor);
			}
		}

		SDL_RWclose(file);

		SDL_Log("Level Opened: %s", filePath);

		mCurrentFile = filePath;
		mHasEdit = false;
		updateWindowTitle(app);
	} else {
		SDL_Log("Failed to open level");
		MessageBox(0, "Failed to open level", _T("Load Failed"), MB_OK | MB_ICONERROR);
	}
}

void EditorScreen::saveBeforeClosePrompt(App &app) {
	if (mHasEdit) {
		int promptResponse = MessageBox(0, _T("You have unsaved changes. Save before closing?"), _T("Unsaved Changes"), MB_YESNO | MB_ICONQUESTION);
		if (promptResponse == IDYES) {
			saveLevel(app);
		}
	}
}

/** @param index index to check
@returns true if the list of selected items contains the specified index.
*/
bool EditorScreen::selectedItemsContains(int index) {
	for (std::vector<SelectedItem>::iterator it = mSelectedItems.begin(); it != mSelectedItems.end(); ++it) {
		if (it->index == index) {
			return true;
		}
	}
	return false;
}

/** @param index index to remove
    @returns true if the removal was successful
*/
bool EditorScreen::eraseSelectedItem(int index) {
	for (std::vector<SelectedItem>::iterator it = mSelectedItems.begin(); it != mSelectedItems.end(); ++it) {
		if (it->index == index) {
			mSelectedItems.erase(it);
			return true;
		}
	}
	return false;
}

/** Remove all game objects from memory.
*/
void EditorScreen::cleanUp() {
	mActors.clear();
	if (mPlayer) {
		delete mPlayer;
		mPlayer = NULL;
	}
}

/** Reset the editor and start a new file 
*/
void EditorScreen::reset(App &app) {
	mCurrentFile = "";
	mHasEdit = false;

	mCurrentTool = mToolCreate;

	mCamera.x = 0;
	mCamera.y = 0;
	mCamera.w = app.mScreenWidth;
	mCamera.h = app.mScreenHeight;

	updateWindowTitle(app);

	mCreateActor = NULL;
	mCreateActorID = 0;
	mCreateActorDirection = RIGHT;
	mCreateActorName = "";
	changeObject(mCreateActorID, mCreateActorDirection);

	mSelectedItems.clear();

	mIsPanActive = false;

	// Not currently used
	mBackgroundID = -1;
	mMusicID = -1;
	mLevelWidth = -1;
	mLevelHeight = -1;
	mLevelName = "";

	cleanUp();
}

void EditorScreen::placePlayer() {
	if (mPlayer) {
		mPlayer->setInitialPos(mMouseX + mCamera.x, mMouseY + mCamera.y);
	} else {
		mPlayer = ActorFactory::createActor(-2, mMouseX, mMouseY);
	}
}

void EditorScreen::moveCameraToPlayer(App &app) {
	if (mPlayer) {
		mCamera.x = static_cast<int>(mPlayer->mPosX) - app.mScreenWidth / 2;
		mCamera.y = static_cast<int>(mPlayer->mPosY) - app.mScreenHeight / 2;
	}
}

/** Handles non-event input.
*/
InputResult EditorScreen::handleInput() {
	InputResult res;
	res.inputConsumed = mCurrentTool->handleInput(*this);
	if (!res.inputConsumed) {
		mIsPanActive = mKeyStates[SDL_SCANCODE_SPACE];
	}
	return res;
}

/** Handles event input.
	@param e The event being handled.
*/
InputResult EditorScreen::handleInput(App &app, SDL_Event &e) {
	InputResult res;
	if (mIsPanActive) {
		if (e.type == SDL_MOUSEMOTION && (e.motion.state & SDL_BUTTON_LMASK)) {
			res.inputConsumed = true;
			mCamera.x -= e.motion.xrel;
			mCamera.y -= e.motion.yrel;
		}
	} else if (res.inputConsumed = mCurrentTool->handleInput(app, *this, e)) {
		mHasEdit = true;
		updateWindowTitle(app);
	} else {
		if (e.type == SDL_KEYDOWN) {
			if (e.key.keysym.mod & KMOD_LCTRL) {
				switch (e.key.keysym.sym) {
					case SDLK_s:
						res.inputConsumed = true;
						saveLevel(app);
						break;
					case SDLK_z:
						res.inputConsumed = true;
						if (mActors.size() > 0) {
							SDL_Log("undo");
							eraseSelectedItem(mActors.size() - 1);
							mActors.pop_back();
							mHasEdit = true;
						}
						break;
					case SDLK_o:
						res.inputConsumed = true;
						openLevel(app);
						break;
					case SDLK_n:
						res.inputConsumed = true;
						saveBeforeClosePrompt(app);
						reset(app);
						break;
				}
			} else if (e.key.keysym.mod & KMOD_LCTRL & KMOD_LALT) {
				switch (e.key.keysym.sym) {
					case SDLK_s:
						res.inputConsumed = true;
						saveLevelAs(app);
						break;
				}
			} else if (e.key.keysym.mod & KMOD_LSHIFT) {
				switch (e.key.keysym.sym) {
					case SDLK_p:
						res.inputConsumed = true;
						placePlayer();
						mHasEdit = true;
						break;
				}
			} else {
				switch (e.key.keysym.sym) {
					case SDLK_a:
						res.inputConsumed = true;
						mCamera.x -= 20;
						break;
					case SDLK_d:
						res.inputConsumed = true;
						mCamera.x += 20;
						break;
					case SDLK_w:
						res.inputConsumed = true;
						mCamera.y -= 20;
						break;
					case SDLK_s:
						res.inputConsumed = true;
						mCamera.y += 20;
						break;
					case SDLK_p:
						res.inputConsumed = true;
						moveCameraToPlayer(app);
						mHasEdit = true;
						break;
					case SDLK_b:
						res.inputConsumed = true;
						mCurrentTool = mToolCreate;
						break;
					case SDLK_v:
						res.inputConsumed = true;
						mCurrentTool = mToolSelect;
						break;
					case SDLK_BACKSPACE:
						res.inputConsumed = true;
						saveBeforeClosePrompt(app);
						res.newScreen = new GameScreen(mCurrentFile.c_str());
						break;
					case SDLK_TAB:
						res.inputConsumed = true;
						app.transitionScreen(new EScreenActorPicker(this));
						break;
					case SDLK_F1:
						res.inputConsumed = true;
						app.transitionScreen(new EScreenHelp());
						break;
				}
			}
		} else if (e.type == SDL_KEYUP) {
			//switch (e.key.keysym.sym) {
			//	case SDLK_SPACE:
			//		res.inputConsumed = true;
			//		mIsPanActive = false;
			//		break;
			//}
		} else if (e.type == SDL_QUIT) {
			res.inputConsumed = true;
			saveBeforeClosePrompt(app);
			app.mQuit = true;
		}

		updateWindowTitle(app);
	}
	return res;
}

bool EditorScreen::isStackedScreen() {
	return false;
}
