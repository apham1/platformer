#ifndef _CEVENTWALLMIDDLE_H
#define _CEVENTWALLMIDDLE_H

#include "CEvent.h"
#include "Actor.h"

class CEventWallMiddle : public CEvent {
public:
	void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override {
		actor.physics()->getVolume()->rollbackPosition();

		actor.setPosX(actor.mPosX - cData->xPenetration);

		if (cData->xPenetration != 0.0f) {
			actor.mVelX = 0.0f;
			actor.mSling.xSpeed = 0.0f;
		}

		actor.mSling.isSlinging = false;
		actor.mSling.hasSlung = false;
		actor.mIsKnockedBack = false;
	}
	void triggerEventExit(Actor &actor, Actor &object) override {};
};

#endif /* _CEVENTWALLMIDDLE_H */