#ifndef _CEVENTFIELD_H
#define _CEVENTFIELD_H

#include "CEvent.h"
#include "Actor.h"

class CEventField : public CEvent {
public:
	/** Triggers event upon entering field.
		@param actor The main actor
		@param object The field it's colliding with
		@param cData Collision data
	*/
	void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override {
		actor.mLastTouchedSling = &object;
	}

	/** Triggers upon exiting the field.
		@param @param actor The main actor
		@param object The field it's colliding with
	*/
	void triggerEventExit(Actor &actor, Actor &object) override {
		if (actor.mLastTouchedSling == &object) {
			actor.mLastTouchedSling = NULL;
			actor.mSling.hasSlung = false;
		}
	}
};

#endif /* _CEVENTFIELD_H */