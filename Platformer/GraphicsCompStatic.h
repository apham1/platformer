#ifndef _GRAPHICSCOMPSTATIC_H
#define _GRAPHICSCOMPSTATIC_H

#include "GraphicsComp.h"

class GraphicsCompStatic : public GraphicsComp {
public:
	GraphicsCompStatic(Texture* texture);
	void render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) override;
	int getWidth() override;
	int getHeight() override;
};

#endif /* _GRAPHICSCOMPSTATIC_H */
