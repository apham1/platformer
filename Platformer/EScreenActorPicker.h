#ifndef _ESCREENACTORPICKER_H
#define _ESCREENACTORPICKER_H

#include "EditorScreen.h"
#include "UIButtonActor.h"
#include "UIScrollBar.h"

class EScreenActorPicker : public Screen {
public:
	EScreenActorPicker(EditorScreen *parent);

	bool onEntry(App &app) override;
	void onExit() override;

	InputResult handleInput() override;
	InputResult handleInput(App &app, SDL_Event &e) override;

	void update(App &app) override;
	void render(App &app, int currentTicks) override;

	bool isStackedScreen() override;

private:
	EditorScreen *mParent;
	std::vector<UIButtonActor> mButtons;
	UIScrollBar mScrollBar;

	Texture mOverlayTexture;
};

#endif // _ESCREENACTORPICKER_H
