#ifndef _CEVENTWALLTOP_H
#define _CEVENTWALLTOP_H

#include "CEvent.h"
#include "Actor.h"

class CEventWallTop : public CEvent {
public:
	void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override {
		actor.physics()->getVolume()->rollbackPosition();

		actor.setPos(actor.mPosX - cData->xPenetration, actor.mPosY - cData->yPenetration);

		// Top side collision
		if (cData->yPenetration > 0.0f) {
		  	actor.mAirborne = false;
			actor.mVelY = 0.0f;

			actor.mSling.xSpeed = 0.0f;
			actor.mSling.ySpeed = 0.0f;
		}

		if (cData->xPenetration != 0.0f) {
			actor.mVelX = 0.0f;
			actor.mSling.xSpeed = 0.0f;
		}

		actor.mSling.isSlinging = false;
		actor.mSling.hasSlung = false;
		actor.mIsKnockedBack = false;
	}
	void triggerEventExit(Actor &actor, Actor &object) override {};
};

#endif /* _CEVENTWALLTOP_H */