#ifndef _ETOOLSELECT_H
#define _ETOOLSELECT_H

#include "EditorTool.h"

class EToolSelect : public EditorTool {
public:
	EToolSelect(int maxSelectedItems);
	~EToolSelect();

	bool handleInput(EditorScreen &editor) override;
	bool handleInput(App &app, EditorScreen &editor, SDL_Event &e) override;

	void update(EditorScreen &editor, SDL_Renderer *renderer) override;
	void render(EditorScreen &editor, SDL_Renderer *renderer, int screenWidth, int screenHeight, int currentTicks) override;

private:
	bool updateSelectedItems(EditorScreen &editor, VolumeRect *selectBox, bool isMultiSelect = false);
	void moveSelectedItems(EditorScreen &editor, int xDist, int yDist);
	void eraseSelectedItems(EditorScreen &editor);
	void reorderSelectedItem(EditorScreen &editor, int index);
	void copySelectedItems(EditorScreen &editor);
	void flipSelectedItems(EditorScreen &editor);
	void pasteFromClipboard(EditorScreen &editor);
	void alignTopSelectedItems(EditorScreen &editor);
	void alignVerticalSelectedItems(EditorScreen &editor);
	void drawBoldBox(EditorScreen &editor, SDL_Renderer *renderer, SDL_Rect *box, int thickness, SDL_Color *color);
	void freeMultiSelectBox();

	VolumeRect *mMultiSelectBox;
	std::vector<ActorData> mClipboard;
};

#endif // _ETOOLSELECT_H

