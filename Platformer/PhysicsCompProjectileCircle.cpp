#include "PhysicsCompProjectileCircle.h"
#include "Actor.h"

/** Updates the actor physics state and handles collision.
	@param actor The actor to update
	@param colliders The list of colliders
*/
void PhysicsCompProjectileCircle::update(Actor &actor) {
	if (actor.mDirection == LEFT) {
		setPos(actor, actor.mPosX - mSpeed,
					  actor.mPosY);
	} else if (actor.mDirection == RIGHT) {
		setPos(actor, actor.mPosX + mSpeed,
					  actor.mPosY);
	} else if (actor.mDirection == UP) {
		setPos(actor, actor.mPosX,
					  actor.mPosY - mSpeed);
	} else if (actor.mDirection == DOWN) {
		setPos(actor, actor.mPosX,
					  actor.mPosY + mSpeed);
	}

	if (actor.mLifeSpan == 0) {
		actor.mIsAlive = false;
	} else {
		--actor.mLifeSpan;
	}
}

/** Sets the position of the actor.
	@param actor The actor being changed
	@param x The x position
	@param y The y position
*/
void PhysicsCompProjectileCircle::setPos(Actor &actor, float x, float y) {
	actor.mPosX = x;
	actor.mPosY = y;

	VolumeCircle *circlePt = static_cast<VolumeCircle*>(mVolume);

	mVolume->setPosition(actor.mPosX, actor.mPosY);
}

/** Sets the initial position of the actor.
	@param actor The actor being changed
	@param x The x position
	@param y The y position
*/
void PhysicsCompProjectileCircle::setInitialPos(Actor &actor, float x, float y) {
	actor.mPosX = x;
	actor.mPosY = y;

	VolumeCircle *circlePt = static_cast<VolumeCircle*>(mVolume);

	mVolume->setInitialPosition(actor.mPosX, actor.mPosY);
}
