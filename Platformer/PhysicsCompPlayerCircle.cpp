#include "PhysicsCompPlayerCircle.h"
#include "Actor.h"

/** Updates the actor physics state and handles collision.
	@param actor The actor to update
	@param colliders The list of colliders
*/
void PhysicsCompPlayerCircle::update(Actor &actor, std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) {
	if (!actor.mSling.hasSlung) {
		actor.mVelY += mGravity;
	}
	setPos(actor, actor.mPosX + actor.mVelX + actor.mSling.xSpeed,
		          actor.mPosY + actor.mVelY + actor.mSling.ySpeed);

	// Set airborne to true here, collision resolution will determine if the actor is not airborne
	actor.mAirborne = true;
	std::vector<Actor>::iterator it;
	for (it = objects.begin(); it != objects.end(); ++it) {
		CollisionData *penetration = (*it).physics()->getVolume()->isColliding(static_cast<VolumeCircle*>(mVolume));
		if (penetration) {
			(*it).physics()->onEnterCollision(actor, (*it), penetration);
		} else {
			(*it).physics()->onExitCollision(actor, (*it));
		}
	}
}

/** Sets the position of the actor.
	@param actor The actor being changed
	@param x The x position
	@param y The y position
*/
void PhysicsCompPlayerCircle::setPos(Actor &actor, float x, float y) {
	actor.mPosX = x;
	actor.mPosY = y;

	VolumeCircle *circlePt = static_cast<VolumeCircle*>(mVolume);

	mVolume->setPosition(actor.mPosX, actor.mPosY);
}

/** Sets the initial position of the actor.
	@param actor The actor being changed
	@param x The x position
	@param y The y position
*/
void PhysicsCompPlayerCircle::setInitialPos(Actor &actor, float x, float y) {
	actor.mPosX = x;
	actor.mPosY = y;

	VolumeCircle *circlePt = static_cast<VolumeCircle*>(mVolume);

	mVolume->setInitialPosition(actor.mPosX, actor.mPosY);
}
