#ifndef _PHYSICSCOMPSTATICRECT_H
#define _PHYSICSCOMPSTATICRECT_H

#include "PhysicsComp.h"
#include "Actor.h"

class PhysicsCompStaticRect : public PhysicsComp {
public:
	PhysicsCompStaticRect(float width, float height, CEvent *e = NULL) : PhysicsComp(e) { 
		mVolume = new VolumeRect(width, height);
	}

	void update(Actor &actor) override {}
	void update(Actor &actor, std::vector<Actor> &objects) override {}
	void update(Actor &actor, std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) override {}

	/** Sets the actor's initial position and shifts colliders.
	@param actor The actor being changed
	@param x the x position
	@param y the y position
	*/
	void setPos(Actor &actor, float x, float y) override {
		actor.mPosX = x;
		actor.mPosY = y;

		VolumeRect *rectPt = static_cast<VolumeRect*>(mVolume);

		mVolume->setPosition(actor.mPosX - rectPt->w / 2, actor.mPosY - rectPt->h / 2);
	}

	/** Sets the actor's initial position and shifts colliders.
	@param actor The actor being changed
	@param x the x position
	@param y the y position
	*/
	void setInitialPos(Actor &actor, float x, float y) override {
		actor.mPosX = x;
		actor.mPosY = y;

		VolumeRect *rectPt = static_cast<VolumeRect*>(mVolume);

		mVolume->setInitialPosition(actor.mPosX - rectPt->w / 2, actor.mPosY - rectPt->h / 2);
	}
};
#endif /* _PHYSICSCOMPSTATICRECT_H */
