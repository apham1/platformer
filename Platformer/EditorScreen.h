#ifndef _EDITORSCREEN_H
#define _EDITORSCREEN_H

#include "Screen.h"
#include "Actor.h"
#include "ActorFactory.h"
#include "OpenFileDialog.h"
#include "SaveFileDialog.h"
#include <list>
#include <SDL_ttf.h>

struct SelectedItem {
	Actor *actor;
	int index;

	SelectedItem() {
		clear();
	}
	SelectedItem(Actor *actor, int index) {
		this->actor = actor;
		this->index = index;
	}


	void clear() {
		actor = NULL;
		index = -1;
	}
};

class EditorTool;
class EToolCreate;
class EToolSelect;
class EScreenActorPicker;
class EScreenActorProperties;

class EditorScreen : public Screen {
	friend class EToolCreate;
	friend class EToolSelect;

	friend class EScreenActorPicker;
	friend class EScreenActorProperties;

public:
	EditorScreen(const char *levelFile = "", int cameraX = 0, int cameraY = 0);

	bool onEntry(App &app) override;
	void onExit() override;

	InputResult handleInput() override;
	InputResult handleInput(App &app, SDL_Event &e) override;

	void update(App &app) override;
	void render(App &app, int currentTicks) override;

	bool isStackedScreen() override;

private:
	struct InitialLoadInfo {
		std::string fileName;
		int cameraX;
		int cameraY;
		
		InitialLoadInfo(const char *fileName, int cameraX, int cameraY) {
			this->fileName = fileName;
			this->cameraX = cameraX;
			this->cameraY = cameraY;
		}
	};
	
	void updateWindowTitle(App &app);
	bool loadMedia(App &app);
	void placePlayer();
	void moveCameraToPlayer(App &app);
	void saveLevelAs(App &app);
	void saveLevel(App &app);
	bool writeLevelDataToFile(const char *filePath);
	void openLevel(App &app);
	void loadLevelFromFile(App &app, const char *filePath);
	void saveBeforeClosePrompt(App &app);
	bool selectedItemsContains(int index);
	bool eraseSelectedItem(int index);
	void cleanUp();
	void reset(App &app);
	void drawProjectileIndicator(App &app, Actor &actor);
	void drawShockwaveIndicator(App &app, Actor &actor);

	InitialLoadInfo *mInitialLoad;

	const char *mWindowTitle = "Platformer Editor";
	std::string mCurrentFile;
	bool mHasEdit;

	const int mTotalActorTypes = 29;

	EditorTool *mCurrentTool;
	EToolCreate *mToolCreate;
	EToolSelect *mToolSelect;
	bool mIsPanActive;

	void changeObject(int actorID, Direction direction);

	Actor *mCreateActor;
	int mCreateActorID;
	Direction mCreateActorDirection;
	std::string mCreateActorName;
	Texture mCreateActorText;

	std::vector<SelectedItem> mSelectedItems;
	const unsigned int mMaxSelectedItems = 100;

	SDL_Rect mCamera;
	std::list<Actor> mActors;
	Actor *mPlayer;
	float mMouseX, mMouseY;
	int mBackgroundID, mMusicID;
	int mLevelWidth, mLevelHeight;
	std::string mLevelName;

	const Uint8 *mKeyStates;
	Texture mMouseText, mCameraText;
	Texture *mBackgroundTexture;

	TTF_Font *mFont;

	Texture mTempBackground;
};

#endif /* _EDITORSCREEN_H */