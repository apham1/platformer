#include "App.h"
#include "GameScreen.h"
#include "TitleScreen.h"
#include "EditorScreen.h"
#include "ActorFactory.h"
#include "Locator.h"
#include <string>

GameScreen::GameScreen(const char *levelFile) : Screen() {
	mLevelFileToLoad = levelFile;
}

/** Activates on entry into the screen, initializes values and loads media used in the screen.
	@param app The main application
*/
bool GameScreen::onEntry(App &app) {
	if (!loadMedia(app)) {
		return false;
	}

	mCamera.x = 0;
	mCamera.y = 0;
	mCamera.w = app.mScreenWidth;
	mCamera.h = app.mScreenHeight;

	resetGame();

	return true;
}

/** Activates upon exiting the screen, freeing up memory used in it.
*/
void GameScreen::onExit() {
	cleanUp();
	mBackgroundTexture.free();
	ActorFactory::free();
}

/** Loads all textures.
	@return true if all media successfully loaded
*/
bool GameScreen::loadMedia(App &app) {
	if (!ActorFactory::loadMedia(app.mRenderer)) {
		return false;
	}

	if (!mBackgroundTexture.loadFromFile(app.mRenderer, "testbackground2")) {
		return false;
	}

	return true;
}

/** Resets up the game.
*/
void GameScreen::resetGame() {
	cleanUp();
	mActors.reserve(1000);
	mProjectiles.reserve(200);
	mShockwaves.reserve(50);

	for (int i = 0; i < 200; ++i) {
		Actor *projectile = ActorFactory::createLightningBall(0, 0);
		mProjectiles.push_back(*projectile);
	}

	for (int i = 0; i < 50; ++i) {
		Actor *shockwave = ActorFactory::createShockwave(0, 0);
		mShockwaves.push_back(*shockwave);
	}

	mCombinedProjectiles.reserve(2);
	mCombinedProjectiles.push_back(&mProjectiles);
	mCombinedProjectiles.push_back(&mShockwaves);

	SDL_RWops *file = NULL;
	if (mLevelFileToLoad.empty()) {
		switch (mCurrentLevel) {
			case 1:
				file = SDL_RWFromFile("Levels/level_1.aweg", "rb");
				break;
			case 2:
				file = SDL_RWFromFile("Levels/level_2.aweg", "rb");
				break;
			case 3:
				file = SDL_RWFromFile("Levels/level_3.aweg", "rb");
				break;
			case 4:
				file = SDL_RWFromFile("Levels/level_4.aweg", "rb");
				break;
			case 5:
				file = SDL_RWFromFile("Levels/level_5.aweg", "rb");
				break;
			case 6:
				file = SDL_RWFromFile("Levels/biglevel.aweg", "rb");
				break;
			case 7:
				file = SDL_RWFromFile("Levels/bigslinglevel.aweg", "rb");
				break;
			case 8:
				file = SDL_RWFromFile("Levels/biglevel2.aweg", "rb");
				break;
		}
	} else {
		file = SDL_RWFromFile(mLevelFileToLoad.c_str(), "rb");
	}

	if (file != NULL) {
		int mBackgroundID, mMusicID;
		int mLevelWidth, mLevelHeight;
		char mLevelName[50];

		SDL_RWread(file, &mBackgroundID, sizeof(int), 1);
		SDL_RWread(file, &mMusicID, sizeof(int), 1);
		SDL_RWread(file, &mLevelWidth, sizeof(int), 1);
		SDL_RWread(file, &mLevelHeight, sizeof(int), 1);
		SDL_RWread(file, &mLevelName, sizeof(char) * 50, 1);

		char temp[150];
		SDL_RWread(file, &temp, sizeof(char) * 150, 1);

		ActorData data;
		while (SDL_RWread(file, &data, sizeof(ActorData), 1)) {
			if (data.ID == -2) {
				mPlayer = ActorFactory::createActor(data.ID, data.x, data.y, data.direction);
			} else {
				Actor *newActor = ActorFactory::createActor(data.ID, data.x, data.y, data.direction);
				if (data.projectileCooldown != -1 || data.projectileLifespan != -1 || data.delay != -1) {
					if (newActor->gun()) {
						newActor->gun()->setCooldown(data.projectileCooldown);
						newActor->gun()->setProjectileLifespan(data.projectileLifespan);
						newActor->gun()->setDelay(data.delay);
					}
				}
				mActors.push_back(*newActor);
			}
		}

		SDL_RWclose(file);
	}
}

/** Remove all game objects from memory.
*/
void GameScreen::cleanUp() {
	mActors.clear();
	mProjectiles.clear();
	mShockwaves.clear();
	mCombinedProjectiles.clear();
	if (mPlayer) {
		delete mPlayer;
	}
}

/** Continues to the next level.
*/
void GameScreen::levelUp() {
	mPlayer->mCanWin = false;
	mPlayer->mLevelUp = false;

	if (mCurrentLevel < mLastLevel) {
		mCurrentLevel++;
	}

	resetGame();
}

/** Updates all actors.
*/
void GameScreen::update(App &app) {
	if (mPlayer->mIsAlive) {
		mPlayer->update(mActors, mCombinedProjectiles);
		updateCamera(app);

		std::vector<Actor>::iterator it;
		for (it = mActors.begin(); it != mActors.end(); ++it) {
			if ((*it).mIsAlive) {
				(*it).update();
				
				if ((*it).mIsShooting) {
					switch ((*it).gun()->mProjectileType) {
						case 0:
							spawnProjectile(&(*it), &mProjectiles);
							break;
						case 1:
							spawnProjectile(&(*it), &mShockwaves);
							break;
					}
				}
			}
		}

		std::vector<std::vector<Actor>*>::iterator vectorIt;
		for (vectorIt = mCombinedProjectiles.begin(); vectorIt != mCombinedProjectiles.end(); ++vectorIt) {
			for (it = (*vectorIt)->begin(); it != (*vectorIt)->end(); ++it) {
				if ((*it).mIsAlive) {
					(*it).update();
				}
			}
		}
		
		if (mPlayer->mLevelUp) {
			levelUp();
		}
	} else {
		resetGame();
	}
}

/** Updates the camera
*/
void GameScreen::updateCamera(App &app) {
	mCamera.x = static_cast<int>(mPlayer->mPosX) - app.mScreenWidth / 2;
	mCamera.y = static_cast<int>(mPlayer->mPosY) - app.mScreenHeight / 2;
}

void GameScreen::spawnProjectile(Actor *shootingActor, std::vector<Actor> *entities) {
	std::vector<Actor>::iterator it;
	for (it = entities->begin(); it != entities->end(); ++it) {
		if (it->mIsAlive == false) {
			it->mIsAlive = true;
			it->mLifeSpan = shootingActor->gun()->mProjectileLifespan;
			int projectileOffsetX = shootingActor->gun()->mProjectileSpawnX;
			int projectileOffsetY = shootingActor->gun()->mProjectileSpawnY;
			if (shootingActor->mDirection == LEFT) {
				projectileOffsetX = -projectileOffsetX;
			}
			it->mPosX = shootingActor->mPosX + projectileOffsetX;
			it->mPosY = shootingActor->mPosY + projectileOffsetY;
			it->mDirection = shootingActor->mDirection;

			float distance = abs(GameMath::distanceSquared(mPlayer->mPosX, mPlayer->mPosY, it->mPosX, it->mPosY));
			float volume = (mMaxSoundDistanceSquared - distance) / mMaxSoundDistanceSquared * 128.0f;
			if (volume < 0.0f) {
				volume = 0.0f;
			} else if (volume > 128.0f) {
				volume = 128.0f;
			}
 			Locator::getAudio()->addSoundToQueue(SHOOT, static_cast<int>(volume));
			SDL_Log("%f", volume);

			shootingActor->mIsShooting = false;
			break;
		}
	}
}

/** Renders actors and textures onto the screen.
*/
void GameScreen::render(App &app, int currentTicks) {
	/*mBackgroundTexture.render(app.mRenderer, app.mScreenWidth / 2, app.mScreenHeight / 2, &mCamera);*/

	std::vector<Actor>::iterator it;
	for (it = mActors.begin(); it != mActors.end(); ++it) {
		if ((*it).mIsAlive) {
			(*it).render(app.mRenderer, &mCamera, currentTicks);
		}
	}

	std::vector<std::vector<Actor>*>::iterator vectorIt;
	for (vectorIt = mCombinedProjectiles.begin(); vectorIt != mCombinedProjectiles.end(); ++vectorIt) {
		for (it = (*vectorIt)->begin(); it != (*vectorIt)->end(); ++it) {
			if ((*it).mIsAlive) {
				(*it).render(app.mRenderer, &mCamera, currentTicks);
			}
		}
	}

	mPlayer->render(app.mRenderer, &mCamera, currentTicks);

	if (mShowHitboxes) {
		renderHitboxesRect(app);
	}
}

/** Render SDL primitives to show rectangular hitboxes
*/
void GameScreen::renderHitboxesRect(App &app) {
	SDL_SetRenderDrawColor(app.mRenderer, 0xFF, 0x00, 0x00, 0xFF);
	
	Volume* hitbox;
	SDL_Rect hitboxQuad;
	std::vector<Actor>::iterator it;
	for (it = mActors.begin(); it != mActors.end(); ++it) {
		hitbox = (*it).physics()->getVolume();
		if (hitbox) {
			hitboxQuad = hitbox->getRenderRect();
			hitboxQuad.x -= mCamera.x;
			hitboxQuad.y -= mCamera.y;
			SDL_RenderDrawRect(app.mRenderer, &hitboxQuad);
		}
	}

	for (it = mProjectiles.begin(); it != mProjectiles.end(); ++it) {
		if (it->mIsAlive) {
			hitbox = (*it).physics()->getVolume();
			if (hitbox) {
				hitboxQuad = hitbox->getRenderRect();
				hitboxQuad.x -= mCamera.x;
				hitboxQuad.y -= mCamera.y;
				SDL_RenderDrawRect(app.mRenderer, &hitboxQuad);
			}
		}
	}

	hitbox = mPlayer->physics()->getVolume();
	if (hitbox) {
		hitboxQuad = hitbox->getRenderRect();
		hitboxQuad.x -= mCamera.x;
		hitboxQuad.y -= mCamera.y;
		SDL_RenderDrawRect(app.mRenderer, &hitboxQuad);
	}
}

/** Handles non-event input.
*/
InputResult GameScreen::handleInput() {
	InputResult res;
	mPlayer->handleInput();
	return res;
}

/** Handles event input.
	@param e The event being handled.
*/
InputResult GameScreen::handleInput(App &app, SDL_Event &e) {
	InputResult res;
	if (e.type == SDL_KEYDOWN) {
		switch (e.key.keysym.sym) {
			case SDLK_ESCAPE:
				res.inputConsumed = true;
				res.newScreen = new TitleScreen();
				break;
			case SDLK_0:
				res.inputConsumed = true;
				resetGame();
				break;
			case SDLK_BACKSPACE:
				res.inputConsumed = true;
				res.newScreen = new EditorScreen(mLevelFileToLoad.c_str(), mCamera.x, mCamera.y);
				break;
			case SDLK_h:
				res.inputConsumed = true;
				mShowHitboxes = !mShowHitboxes;
				break;
			default:
				res.inputConsumed = mPlayer->handleInput(e);
				break;
		}
	} else if (e.type == SDL_MOUSEBUTTONDOWN) {
		res.inputConsumed = true;
		int x, y;
		SDL_GetMouseState(&x, &y);
		mPlayer->setPos(static_cast<float>(mCamera.x + x), static_cast<float>(mCamera.y + y));
		mPlayer->mVelX = 0;
		mPlayer->mVelY = 0;
	} else {
		res.inputConsumed = mPlayer->handleInput(e);
	}

	return res;
}

bool GameScreen::isStackedScreen() {
	return false;
}