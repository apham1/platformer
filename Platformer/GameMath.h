#ifndef _GAMEMATH_H
#define _GAMEMATH_H

class GameMath {
public:
	static float distanceSquared(float x1, float y1, float x2, float y2) {
		float deltaX = x2 - x1;
		float deltaY = y2 - y1;
		return deltaX*deltaX + deltaY*deltaY;
	}
};

#endif /*_GAMEMATH_H */