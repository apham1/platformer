#ifndef _TITLESCREEN_H
#define _TITLESCREEN_H

#include "Screen.h"
#include "Texture.h"

class TitleScreen : public Screen {
public:
	TitleScreen() : Screen() {}

	bool onEntry(App &app) override;
	void onExit() override;

	InputResult handleInput() override;
	InputResult handleInput(App &app, SDL_Event &e) override;

	void update(App &app) override;
	void render(App &app, int currentTicks) override;
	
	bool isStackedScreen() override;
	
private:
	bool loadMedia(App &app);

	Texture mTitleTexture, mPlayTexture;
	int mMouseX, mMouseY;
	int mPlayButtonX, mPlayButtonY;
};

#endif /* _TITLESCREEN_H */