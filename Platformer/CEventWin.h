#ifndef _CEVENTWIN_H
#define _CEVENTWIN_H

#include "CEvent.h"
#include "Actor.h"

class CEventWin : public CEvent {
public:
	/** Triggers upon colliding with the "win" object and allows the player to continue to the next level.
		@param actor The main actor
		@param object The "win" object it's colliding with
		@param cData Collision data
	*/
	void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override {
		actor.mCanWin = true;
	}

	/** Triggers upon exiting collision with the "win" object.
		@param actor The main actor
		@param object The object it's colliding with
	*/
	void triggerEventExit(Actor &actor, Actor &object) override {
		actor.mCanWin = false;
	}
};

#endif /* _CEVENTWIN_H */