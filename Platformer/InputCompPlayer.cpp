#include "InputCompPlayer.h"
#include "Actor.h"
#include "Locator.h"

InputCompPlayer::InputCompPlayer() {
	mKeyStates = SDL_GetKeyboardState(NULL);
}

/** Updates the actor's velocity based on input.
	@param actor The actor to update
*/
void InputCompPlayer::update(Actor &actor) {
	actor.mAcceleration = 0.0f;

	//Move left
	if (!actor.mIsDucking) {
		if (!actor.mSling.isSlinging) {
			if (mKeyStates[SDL_SCANCODE_A]) {
				actor.mAcceleration = -actor.mSpeed;
			}

			//Move right
			if (mKeyStates[SDL_SCANCODE_D]) {
				actor.mAcceleration = actor.mSpeed;
			}
		} else {
			if (mKeyStates[SDL_SCANCODE_A]) {
				actor.mAcceleration = -actor.mSpeed / 2.0f;
			}

			//Move right
			if (mKeyStates[SDL_SCANCODE_D]) {
				actor.mAcceleration = actor.mSpeed / 2.0f;
			}
		}
	}

	if (!actor.mAirborne) {
		if (mKeyStates[SDL_SCANCODE_S]) {
			duck(actor);
		} else {
			stand(actor);
		}
	}
}

/** Updates the actor using event input.
*/
bool InputCompPlayer::update(Actor &actor, SDL_Event &e) {
	if (e.type == SDL_MOUSEBUTTONDOWN) {
		
	} else if (e.type == SDL_KEYDOWN) {
		switch (e.key.keysym.sym) {
			case SDLK_SPACE:
				if (!e.key.repeat) {
					//Jump
					if (!actor.mAirborne && !actor.mIsDucking) {
						actor.mVelY = -10;
						Locator::getAudio()->addSoundToQueue(JUMP);
						return true;
					} else {
						//Sling
						if (!actor.mSling.hasSlung && actor.mLastTouchedSling) {
							actor.mSling.hasSlung = true;
							actor.mSling.isSlinging = true;

							float angle = atan2(actor.mPosY - actor.mLastTouchedSling->mPosY, actor.mPosX - actor.mLastTouchedSling->mPosX);

							actor.mSling.xSpeed = -(actor.mLastTouchedSling->mSling.slingSpeed * cos(angle));
							actor.mSling.ySpeed = -(actor.mLastTouchedSling->mSling.slingSpeed * sin(angle));

							actor.mVelX = actor.mSling.xSpeed;
							actor.mVelY = actor.mSling.ySpeed;
							return true;
						}
					}
				}
				break;
			case SDLK_w:
				if (actor.mCanWin) {
					actor.mLevelUp = true;
					return true;
				}
				break;
		}
	}
	
	return false;
}

void InputCompPlayer::stand(Actor &actor) {
	actor.physics()->stand(actor);
	actor.graphics()->stand();
	actor.mIsDucking = false;
}

void InputCompPlayer::duck(Actor &actor) {
	actor.physics()->duck(actor);
	actor.graphics()->duck();
	actor.mIsDucking = true;
}