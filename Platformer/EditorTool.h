#ifndef _EDITORTOOL_H
#define _EDITORTOOL_H

#include "App.h"
#include "EditorScreen.h"

class EditorTool {
public:
	virtual bool handleInput(EditorScreen &editorScreen) = 0;
	virtual bool handleInput(App &app, EditorScreen &editorScreen, SDL_Event &e) = 0;

	virtual void update(EditorScreen &editorScreen, SDL_Renderer *renderer) = 0;
	virtual void render(EditorScreen &editor, SDL_Renderer *renderer, int screenWidth, int screenHeight, int currentTicks) = 0;
};

#endif // _EDITORTOOL_H
