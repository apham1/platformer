#include "GraphicsCompStatic.h"
#include "Actor.h"

GraphicsCompStatic::GraphicsCompStatic(Texture* texture) : GraphicsComp() {
	mTexture = texture;
}

/** Delegates the render call to the texture class
    @param actor The actor used to render
	@param renderer The rendering state
	@param camera The game camera
	@param tick The current game tick
*/
void GraphicsCompStatic::render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) {
	mTexture->setAlpha(mAlpha);
	SDL_RendererFlip flip = SDL_FLIP_NONE;
	double rotate = 0.0;
	if (actor.mDirection == LEFT) {
		flip = SDL_FLIP_HORIZONTAL;
	} else if (actor.mDirection == DOWN) {
		rotate = 90.0;
	} else if (actor.mDirection == UP) {
		rotate = -90.0;
	}
	mTexture->render(renderer, static_cast<int>(actor.mPosX) - camera->x, static_cast<int>(actor.mPosY) - camera->y, actor.mScale, flip, NULL, rotate);
}

int GraphicsCompStatic::getWidth() {
	return static_cast<int>(mTexture->getWidth() * mTexture->getScaleX());
}

int GraphicsCompStatic::getHeight() {
	return static_cast<int>(mTexture->getHeight() * mTexture->getScaleY());
}