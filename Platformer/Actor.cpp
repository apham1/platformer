#include "Actor.h"

Actor::Actor() {
	mPosX = 0.0f;
	mPosY = 0.0f;
	mVelX = 0.0f;
	mVelY = 0.0f;
	mSpeed = 0.0f;
	mAcceleration = 0.0f;
	mScale = 1.0f;
	mInputComp = NULL;
	mGraphicsComp = NULL;
	mPhysicsComp = NULL;
	mGunComp = NULL;
	mAirborne = false;
	mIsAlive = true;
	mCanWin = false;
	mLevelUp = false;
	mIsInvincible = false;
	mIsKnockedBack = false;
	mIsShooting = false;
	mIsDucking = false;
	mInvincibleCount = 0;
	mLifeSpan = -1;
	actorID = -1;
	mDirection = RIGHT;

	//Sling initialization
	mSling.xPos = 0.0f;
	mSling.yPos = 0.0f;
	mSling.xSpeed = 0.0f;
	mSling.ySpeed = 0.0f;
	mSling.slingSpeed = 0.0f;
	mSling.hasSlung = false;
	mSling.isSlinging = false;
	mLastTouchedSling = NULL;
}

/** Constructor for the Actor.
	@param speed The movement speed of the Actor
	@param graphicsComp The component used to handle graphics rendering
	@param physicsComp The component used to handle physics
	@param inputComp The component used to handle input
*/
Actor::Actor(float speed, GraphicsComp *graphicsComp, PhysicsComp *physicsComp, InputComp *inputComp) : Actor() {
	mSpeed = speed;
	mGraphicsComp = graphicsComp;
	mPhysicsComp = physicsComp;
	mInputComp = inputComp;
}

/** Deconstructor for the actor, deletes all existing components.
*/
Actor::~Actor() {
	if (mInputComp) {
		delete mInputComp;
		mInputComp = NULL;
	}

	if (mGraphicsComp) {
		delete mGraphicsComp;
		mGraphicsComp = NULL;
	}

	if (mPhysicsComp) {
		delete mPhysicsComp;
		mPhysicsComp = NULL;
	}

	if (mGunComp) {
		delete mGunComp;
		mGunComp = NULL;
	}
}

void Actor::setGunComponent(GunComp *gunComponent) {
	mGunComp = gunComponent;
}

/** Handles input using the input component.
*/
void Actor::handleInput() {
	if (mInputComp) {
		mInputComp->update(*this);
	}
}

/** Handles input with events.
	@param e Event
*/
bool Actor::handleInput(SDL_Event &e) {
	if (mInputComp) {
		return mInputComp->update(*this, e);
	}
	return false;
}

/** Sets the alpha of the actor.
	@param alpha The alpha
*/
void Actor::setAlpha(int alpha) {
	if (mGraphicsComp) {
		mGraphicsComp->setAlpha(alpha);
	}
}

/** Updates the Actor based on input and actor's position.
*/
void Actor::update() {
	if (mPhysicsComp) {
		mPhysicsComp->update(*this);
	}

	if (mGunComp) {
		mGunComp->update(*this);
	}
}

void Actor::update(std::vector<Actor> &objects) {
	if (mPhysicsComp) {
		mPhysicsComp->update(*this, objects);
	}
}

/** Updates the Actor based on input and actor's position.
	@param objects Vector of objects to check physics with actor
*/
void Actor::update(std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) {
	if (mPhysicsComp) {
		mPhysicsComp->update(*this, objects, projectiles);
	}
}


/** Delegates the render call to the graphics component.
	@param renderer The rendering state
*/
void Actor::render(SDL_Renderer *renderer, SDL_Rect *camera, int tick) {
	if (mGraphicsComp) {
		mGraphicsComp->render(*this, renderer, camera, tick);
	}
}

/** Sets the position of the actor if there is a physics component.
	@param x The x position
	@param y The y position
*/
void Actor::setPos(float x, float y) {
	if (mPhysicsComp) {
		mPhysicsComp->setPos(*this, x, y);
	}
}

/** Sets the initial position of the actor if there is a physics component.
	@param x The x position
	@param y The y position
*/
void Actor::setInitialPos(float x, float y) {
	if (mPhysicsComp) {
		mPhysicsComp->setInitialPos(*this, x, y);
	}
}

/** Sets the x position of the actor.
	@param x The x position
*/
void Actor::setPosX(float x) {
	setPos(x, mPosY);
}

/** Sets the y position of the actor
	@param y The y position
*/
void Actor::setPosY(float y) {
	setPos(mPosX, y);
}

/** Sets the scale of the actor. This only affects graphics. 
    @param scale The scale of the actor
*/
void Actor::setScale(float scale) {
	mScale = scale;
	if (mPhysicsComp) {
		// currently does nothing
		mPhysicsComp->setScale(scale);
	}
}

/** Sets the actor's sling speed.
	@param slingSpeed The sling speed of the actor
*/
void Actor::setSlingSpeed(float slingSpeed) {
	mSling.slingSpeed = slingSpeed;
}

bool Actor::operator==(const Actor& other) {
	return (mPosX == other.mPosX) &&
		(mPosY == other.mPosY) &&
		(mVelX == other.mVelX) &&
		(mVelY == other.mVelY) &&
		(mSpeed == other.mSpeed) &&
		(mAcceleration == other.mAcceleration) &&
		(mScale == other.mScale) &&
		(mAirborne == other.mAirborne) &&
		(mIsAlive == other.mIsAlive) &&
		(mCanWin == other.mCanWin) &&
		(mLevelUp == other.mLevelUp) &&
		(mIsInvincible == other.mIsInvincible) &&
		(mIsKnockedBack == other.mIsKnockedBack) &&
		(actorID == other.actorID) &&
		(mInvincibleCount == other.mInvincibleCount) &&
		(mLifeSpan == other.mLifeSpan) &&
		(mIsShooting == other.mIsShooting) &&
		(mIsDucking == other.mIsDucking) &&
		(mDirection == other.mDirection) &&
		(mSling == other.mSling) &&
		(mLastTouchedSling == other.mLastTouchedSling) &&
		(mInputComp == other.mInputComp) &&
		(mGraphicsComp == other.mGraphicsComp) &&
		(mPhysicsComp == other.mPhysicsComp) &&
		(mGunComp == other.mGunComp);
}