#include "Texture.h"
#include <fstream>
#include <sstream>
#include <rapidjson/document.h>

Texture::Texture() {
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
	mCenterX = 0;
	mCenterY = 0;
	mScaleX = 1.0f;
	mScaleY = 1.0f;

	mFrameWidth = 0;
	mFrameHeight = 0;
	mInterval = 0;
	mAnimations.clear();
}

Texture::~Texture() {
	free();
}

/** Creates a texture from an image file.
	@param renderer The rendering state
	@param fileName file name of image
	@param scaleX Value between 0 and 1 to scale the width by
	@param scaleY Value between 0 and 1 to scale the height by
	@param centerX The center X of the texture
	@param centerY the center Y of the texture
	@return true if successful
*/
bool Texture::loadFromFile(SDL_Renderer *renderer, const char *fileName, float scaleX, float scaleY, int centerX, int centerY) {
	free();

	std::string textureFile = std::string("Textures/").append(fileName).append(".png");

	SDL_Surface *image = IMG_Load(textureFile.c_str());

	if (image == NULL) {
		SDL_Log("Failed to load from file %s: %s", textureFile.c_str(), IMG_GetError());
		return false;
	}

	mWidth = image->w;
	mHeight = image->h;
	mTexture = SDL_CreateTextureFromSurface(renderer, image);
	if (mTexture == NULL) {
		SDL_Log("Failed to load texture from surface %s: %s", textureFile.c_str(), IMG_GetError());
		return false;
	}

	SDL_FreeSurface(image);

	mScaleX = scaleX;
	mScaleY = scaleY;

	if (loadAnimationData(fileName)) {
		if (centerX == DEFAULT_CENTER_X) {
			centerX = mFrameWidth / 2;
		}
		if (centerY == DEFAULT_CENTER_Y) {
			centerY = mFrameHeight / 2;
		}
	} else {
		if (centerX == DEFAULT_CENTER_X) {
			centerX = mWidth / 2;
		}
		if (centerY == DEFAULT_CENTER_Y) {
			centerY = mHeight / 2;
		}
	}
	mCenterX = static_cast<int>(centerX * scaleX);
	mCenterY = static_cast<int>(centerY * scaleY);

	SDL_SetTextureBlendMode(mTexture, SDL_BLENDMODE_BLEND);

	return true;
}

bool Texture::loadAnimationData(const char *fileName) {
	std::string animationFile = std::string("Animations/").append(fileName).append(".json");

	// read entire file into fileContents
	std::ifstream file;
	file.open(animationFile, std::ios::in);
	if (file.fail()) {
		file.close();
		return false;
	}

	std::stringstream sstr;
	sstr << file.rdbuf();
	std::string fileContents = sstr.str();

	// load animation data
	rapidjson::Document d;
	d.Parse(fileContents.c_str());

	mFrameWidth = d["width"].GetInt();
	mFrameHeight = d["height"].GetInt();
	mInterval = d["interval"].GetInt();
	rapidjson::Value const& animations = d["animations"];
	for (rapidjson::SizeType i = 0; i < animations.Size(); ++i) {
		std::string name = animations[i]["name"].GetString();
		AnimationSeq anim;
		anim.startFrame = animations[i]["start"].GetInt();
		anim.totalFrames = animations[i]["total"].GetInt();
		mAnimations.insert(std::pair<std::string, AnimationSeq>(name, anim));
	}

	file.close();
	return true;
}

/** Creates a texture using text.
	@param renderer The rendering state
	@param font The font for the text
	@param text The text being converted to a texture
	@param color The color of the text
	@return true if successful
*/
bool Texture::loadText(SDL_Renderer* renderer, TTF_Font* font, std::string text, SDL_Color color, Uint32 wrapLength) {
	free();

	if (text.empty()) {
		text = " ";
	}

	SDL_Surface* textSurface = TTF_RenderText_Blended_Wrapped(font, text.c_str(), color, wrapLength);
	if (!textSurface) {
		return false;
	}

	mWidth = textSurface->w;
	mHeight = textSurface->h;

	mTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
	SDL_FreeSurface(textSurface);

	return true;
}

/** Renders texture to screen.
	@param renderer The rendering state
	@param x The x-position
	@param y The y-position
	@param angle The rotation angle
	@param rotateCenter The center point of rotation
	@param flip Enum specifying how texture should be flipped
*/ 
void Texture::render(SDL_Renderer *renderer, int x, int y, float scale, SDL_RendererFlip flip, SDL_Rect *clip, double angle, SDL_Point *rotateCenter) {
	render(renderer, x, y, mWidth, mHeight, scale, flip, clip, angle, rotateCenter);
}

/** Renders texture to screen.
    @param renderer The rendering state
	@param x The x-position
	@param y The y-position
	@param width The width of the render quad
	@param height The height of the render quad
	@param clip The clip quad
    @param angle The rotation angle
    @param rotateCenter The center point of rotation
    @param flip Enum specifying how texture should be flipped
*/
void Texture::render(SDL_Renderer *renderer, int x, int y, int width, int height, float scale, SDL_RendererFlip flip, SDL_Rect *clip, double angle, SDL_Point *rotateCenter) {
	SDL_Rect renderQuad = { x - static_cast<int>(mCenterX * scale), y - static_cast<int>(mCenterY * scale), static_cast<int>(width * mScaleX * scale), static_cast<int>(height * mScaleY * scale) };
	SDL_RenderCopyEx(renderer, mTexture, clip, &renderQuad, angle, rotateCenter, flip);
}

/** Sets the alpha of the texture.
	@param alpha The alpha
*/
void Texture::setAlpha(int alpha) {
	SDL_SetTextureAlphaMod(mTexture, alpha);
}

/** Gets the width of the texture.
	@return width of texture
*/
int Texture::getWidth() {
	return mWidth;
}

/** Gets the height of the texture.
	@return height of texture
*/
int Texture::getHeight() {
	return mHeight;
}

int Texture::getFrameWidth() {
	return mFrameWidth;
}

int Texture::getFrameHeight() {
	return mFrameHeight;
}

float Texture::getScaleX() {
	return mScaleX;
}

float Texture::getScaleY() {
	return mScaleY;
}

/**	Destroys the SDL_Texture if one exists.
*/
void Texture::free() {
	if (mTexture != NULL) {
		SDL_DestroyTexture(mTexture);
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}

	mFrameWidth = 0;
	mFrameHeight = 0;
	mInterval = 0;
	mAnimations.clear();
}