#include "GunComp.h"
#include "Actor.h"

void GunComp::update(Actor &actor) {
	if (!actor.mIsShooting) {
		++mCount;
		if (mCount >= mCooldown) {
			mCount = 0;
			actor.mIsShooting = true;
		}
	}
}

void GunComp::setCooldown(int cooldown) {
	mCooldown = cooldown;
}

void GunComp::setProjectileLifespan(int projectileLifespan) {
	mProjectileLifespan = projectileLifespan;
}

void GunComp::setDelay(int delay) {
	mDelay = delay;
	mCount = -mDelay;
}

void GunComp::setProjectileSpawnOffset(int x, int y) {
	mProjectileSpawnX = x;
	mProjectileSpawnY = y;
}

void GunComp::setProjectileType(int type) {
	mProjectileType = type;
}