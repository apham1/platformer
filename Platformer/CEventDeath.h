#ifndef _CEVENTDEATH_H
#define _CEVENTDEATH_H

#include "CEvent.h"
#include "Actor.h"

class CEventDeath : public CEvent {
public:
	/** Triggers upon colliding with the block and kills the actor that touches it.
		@param actor The main actor
		@param object The death block it's colliding with
		@param cData Collision data
	*/
	void CEventDeath::triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override {
		actor.mIsAlive = false;
	}

	void triggerEventExit(Actor &actor, Actor &object) override {};
};

#endif /* _CEVENTDEATH_H */