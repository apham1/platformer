#ifndef _GRAPHICSCOMPANIMATED_H
#define _GRAPHICSCOMPANIMATED_H

#include "GraphicsComp.h"
#include "Animation.h"

class GraphicsCompAnimated : public GraphicsComp {
public:
	GraphicsCompAnimated(Texture *texture, const char *animationSeq);
	void render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) override;
	int getWidth() override;
	int getHeight() override;

protected:
	Animation *mAnimation;
};

#endif /* _GRAPHICSCOMPANIMATED_H */
