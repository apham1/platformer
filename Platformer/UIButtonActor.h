#ifndef _UIBUTTONACTOR_H
#define _UIBUTTONACTOR_H

#include "UIButton.h"
#include "Actor.h"

class UIButtonActor : public UIButton {
public:
	UIButtonActor(int x, int y, int width, int height, Actor *actor, bool isCurrentActor = false) : UIButton(x, y, width, height) {
		mActor = actor;
		mIsCurrentActor = isCurrentActor;
		init();
	}

	~UIButtonActor() {
		if (mActor) {
			delete mActor;
			mActor = NULL;
		}
	}

	void render(SDL_Renderer *renderer, int scroll, int ticks) override {
		int yWithScroll = y - scroll;

		SDL_Rect buttonBackground = { x, yWithScroll, width, height };
		if (mIsClicked) {
			SDL_SetRenderDrawColor(renderer, 0x00, 0xFF, 0x30, 0xFF);
		} else {
			SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
		}
		SDL_RenderFillRect(renderer, &buttonBackground);
		int border = 8;
		buttonBackground.x += border;
		buttonBackground.y += border;
		buttonBackground.w -= border * 2;
		buttonBackground.h -= border * 2;
		if (mIsHovered) {
			SDL_SetRenderDrawColor(renderer, 0xFC, 0xFF, 0x6D, 0xFF);
		} else if (mIsCurrentActor) {
			SDL_SetRenderDrawColor(renderer, 0x95, 0xF4, 0x42, 0xFF);
		} else {
			SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		}
		SDL_RenderFillRect(renderer, &buttonBackground);

		if (mActor) {
			SDL_Rect camera = { -x, -yWithScroll, 1, 1 };
			mActor->render(renderer, &camera, ticks);
		}
	}

	Actor *getActor() {
		return mActor;
	}

private:
	void init() {
		if (mActor) {
			mActor->setPos(static_cast<float>(width / 2), static_cast<float>(height / 2));
			int textureWidth = mActor->graphics()->getWidth();
			int textureHeight = mActor->graphics()->getHeight();
			if (textureWidth > textureHeight) {
				mActor->setScale(static_cast<float>(width) / static_cast<float>(textureWidth) * 0.85f);
			} else {
				mActor->setScale(static_cast<float>(height) / static_cast<float>(textureHeight) * 0.85f);
			}
		}
	}

	Actor *mActor;
	bool mIsCurrentActor;
};

#endif /* _UIBUTTONACTOR_H */