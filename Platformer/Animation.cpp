#include "Animation.h"

Animation::Animation(Texture *texture, const char *defaultAnimSequence) {
	mTexture = texture;
	setDefaultSequence(defaultAnimSequence);
	mStartTick = -1;
}

/** Set the texture used by the animation set
    @param texture pointer to texture to use
*/
void Animation::setTexture(Texture *texture) {
	mTexture = texture;
}

/** Renders texture to screen.
    @param renderer The rendering state
    @param x The x-position
    @param y The y-position
    @param angle The rotation angle
    @param rotateCenter The center point of rotation
    @param flip Enum specifying how texture should be flipped
*/
void Animation::render(SDL_Renderer *renderer, int x, int y, int tick, float scale, SDL_RendererFlip flip, double angle, SDL_Point* rotateCenter) {
	if (!mCurSequence) {
		mCurSequence = mDefaultSequence;
	}

	AnimationSeq anim = mTexture->mAnimations[mCurSequence->sequenceName];
	
	if (mStartTick == -1) {
		mStartTick = tick;
	}

	tick -= mStartTick;

	int frameNum = 0;
	if (mCurSequence != mDefaultSequence && !mCurSequence->isLooping && (tick > mTexture->mInterval * anim.totalFrames)) {
		setSequence(mDefaultSequence);
	} else {
		frameNum = (tick / mTexture->mInterval) % anim.totalFrames + anim.startFrame;
	}

	int columns = mTexture->getWidth() / mTexture->mFrameWidth;
	int rows = mTexture->getHeight() / mTexture->mFrameHeight;
	int clipX = (frameNum * mTexture->mFrameWidth) % mTexture->getWidth();
	int clipY = (frameNum / columns) * mTexture->mFrameHeight;
	SDL_Rect clipRect = { clipX, clipY, mTexture->mFrameWidth, mTexture->mFrameHeight };
	mTexture->render(renderer, x, y, mTexture->mFrameWidth, mTexture->mFrameHeight, scale, flip, &clipRect, angle, rotateCenter);
}

/** Sets the alpha of the texture.
    @param alpha The alpha
*/
void Animation::setAlpha(int alpha) {
	mTexture->setAlpha(alpha);
}

/** Set the currently playing animation
	@param animationSeq name of the animation to play
	@param isLooping true if the animation is a looping animation
*/
void Animation::setSequence(const char *animationSeq, bool isLooping) {
	setSequence(new AnimSequence(animationSeq, isLooping));
}

/** Set the currently playing animation
	@param animSeq AnimSequence to play
*/
void Animation::setSequence(AnimSequence *animSeq) {
	// Do not free the default sequence here
	if (mCurSequence != mDefaultSequence) {
		freeAnimSequence(mCurSequence);
	}
	mCurSequence = animSeq;
	mStartTick = -1;
}

/** Set the default animation sequence. This animation will play when no other animations are being played. 
	@param animationSeq name of the default animation
	@param isLooping true if the animation is a looping animation
*/
void Animation::setDefaultSequence(const char *animationSeq, bool isLooping) {
	freeAnimSequence(mDefaultSequence);
	mDefaultSequence = new AnimSequence(animationSeq, isLooping);
}

void Animation::freeAnimSequence(AnimSequence *&animSeq) {
	if (animSeq) {
		delete animSeq;
		animSeq = NULL;
	}
}
