#include "App.h"
#include "GameAudio.h"
#include "Locator.h"
#include "GameScreen.h"
#include "TitleScreen.h"

int main(int argc, char *argv[]) {
	App app;

	if (app.init() == 0) {
		app.run();
	}

	return 0;
}

App::App() {
	mWindow = NULL;
	mRenderer = NULL;
	mQuit = false;
}

App::~App() {
	SDL_DestroyWindow(mWindow);
	mWindow = NULL;

	SDL_DestroyRenderer(mRenderer);
	mRenderer = NULL;

	Locator::getAudio()->close();
	SDL_Quit();
	TTF_Quit();
}

/** Initialize SDL systems, creates window, and creates renderer.
	@return -1 if the SDL systems failed to initialize
		    -2 if the window failed to create
			-3 if the renderer failed to create
*/
int App::init() {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return -1;
	}

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");

	mWindow = SDL_CreateWindow("Platformer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, mScreenWidth, mScreenHeight, SDL_WINDOW_SHOWN);
	if (mWindow == NULL) {
		SDL_Log("Unable to create window: %s", SDL_GetError());
		return -2;
	}

	mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED);
	if (mRenderer == NULL) {
		SDL_Log("Unable to create renderer: %s", SDL_GetError());
		return -3;
	}

	GameAudio *gameAudio = new GameAudio();
	if (!gameAudio->init()) {
		SDL_Log("Unable to initialize audio");
		return -4;
	}
	Locator::provide(gameAudio);

	if (!transitionScreen(new TitleScreen())) {
		SDL_Log("Unable to transition screen");
		return -5;
	}

	if (TTF_Init() == -1) {
		SDL_Log("Unable to initialize TTF: %s", TTF_GetError());
		return -6;
	}

	return 0;
}

/** Pushes a new screen and pops all existing screens from the stack.
	@param screen The screen to be pushed onto the stack
	@return true if the screen initialized successfully
*/
bool App::transitionScreen(Screen *screen) {
	if (!screen->isStackedScreen()) {
		while (!mScreen.empty()) {
			popScreen();
		}
	}

	mScreen.push(screen);
	return mScreen.top()->onEntry(*this);
}

/** Runs exit code for the current screen and pops it from the screen stack. 
*/
void App::popScreen() {
	mScreen.top()->onExit();
	mScreen.pop();
}

/** Sets up the game and launches the game loop.
*/
void App::run() {
	Uint32 previousTicks = SDL_GetTicks();
	Uint32 currentTicks = 0;
	Uint32 elapsedTicks = 0;
	double lag = 0.0;

	while (!mQuit) {
		currentTicks = SDL_GetTicks();
		elapsedTicks = currentTicks - previousTicks;
		previousTicks = currentTicks;
		lag += static_cast<double>(elapsedTicks);

		handleInput();

		while (lag >= MS_PER_UPDATE) {
			lag -= MS_PER_UPDATE;
			update();
		}

		render(currentTicks);
		Locator::getAudio()->playQueuedSounds();
	}
}

/** Handles keyboard and mouse input.
*/
void App::handleInput() {
	Screen *screen = mScreen.top()->handleInput().newScreen;

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		InputResult res = mScreen.top()->handleInput(*this, e);
		if (res.inputConsumed) {
			screen = res.newScreen;
		} else {
			if (e.type == SDL_QUIT) {
				mQuit = true;
			}
		}
	}

	if (screen != NULL) {
		transitionScreen(screen);
	}
}

/** Updates the current screen.
*/
void App::update() {
	mScreen.top()->update(*this);
}

/** Clears and re-renders the current screen.
*/
void App::render(int currentTicks) {
	SDL_SetRenderDrawColor(mRenderer, 255, 255, 255, 255);
	SDL_RenderClear(mRenderer);

	mScreen.top()->render(*this, currentTicks);

	SDL_RenderPresent(mRenderer);
}