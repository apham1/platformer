#include "GraphicsCompPlayer.h"
#include "Actor.h"

GraphicsCompPlayer::GraphicsCompPlayer(Texture* texture, Texture *duckingTexture) : GraphicsComp() {
	mStandingTexture = texture;
	mDuckingTexture = duckingTexture;
	mTexture = mStandingTexture;
}

/** Delegates the render call to the texture class
	@param actor The actor used to render
	@param renderer The rendering state
	@param camera The game camera
	@param tick The current game tick
*/
void GraphicsCompPlayer::render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) {
	mTexture->setAlpha(mAlpha);
	if (actor.mDirection == RIGHT) {
		mTexture->render(renderer, static_cast<int>(actor.mPosX) - camera->x, static_cast<int>(actor.mPosY) - camera->y, actor.mScale, SDL_FLIP_NONE);
	} else if (actor.mDirection == LEFT) {
		mTexture->render(renderer, static_cast<int>(actor.mPosX) - camera->x, static_cast<int>(actor.mPosY) - camera->y, actor.mScale, SDL_FLIP_HORIZONTAL);
	} else {
		mTexture->render(renderer, static_cast<int>(actor.mPosX) - camera->x, static_cast<int>(actor.mPosY) - camera->y, actor.mScale);
	}
}

int GraphicsCompPlayer::getWidth() {
	return static_cast<int>(mTexture->getWidth() * mTexture->getScaleX());
}

int GraphicsCompPlayer::getHeight() {
	return static_cast<int>(mTexture->getHeight() * mTexture->getScaleY());
}

void GraphicsCompPlayer::stand() {
	changeTexture(mStandingTexture);
}

void GraphicsCompPlayer::duck() {
	changeTexture(mDuckingTexture);
}