#ifndef _CEVENTPLATFORM_H
#define _CEVENTPLATFORM_H

#include "CEvent.h"
#include "Actor.h"

class CEventPlatform : public CEvent {
public:
	/** Triggers event upon entering field.
		@param actor The main actor
		@param object The field it's colliding with
		@param cData Collision data
	*/
	void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override {
		// Top side collision
		if (cData->xPenetration == 0.0f && cData->yPenetration > 0.0f) {
			actor.physics()->getVolume()->rollbackPosition();

			actor.mAirborne = false;
			actor.mVelY = 0.0f;

			actor.mSling.xSpeed = 0.0f;
			actor.mSling.ySpeed = 0.0f;

			actor.setPos(actor.mPosX, actor.mPosY - cData->yPenetration);
			actor.mSling.isSlinging = false;
			actor.mIsKnockedBack = false;
		}
	}

	void triggerEventExit(Actor &actor, Actor &object) override {}
};

#endif /* _CEVENTPLATFORM_H */