#include "GraphicsCompAnimated.h"
#include "Actor.h"

GraphicsCompAnimated::GraphicsCompAnimated(Texture *texture, const char *animationSeq) : GraphicsComp() {
	mTexture = texture;
	mAnimation = new Animation(mTexture, animationSeq);
}

/** Delegates the render call to the texture class
    @param actor The actor used to render
    @param renderer The rendering state
    @param camera The game camera
    @param tick The current game tick
*/
void GraphicsCompAnimated::render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) {
	mTexture->setAlpha(mAlpha);
	SDL_RendererFlip flip = SDL_FLIP_NONE;
	double rotate = 0.0;
	if (actor.mDirection == LEFT) {
		flip = SDL_FLIP_HORIZONTAL;
	} else if (actor.mDirection == DOWN) {
		rotate = 90.0;
	} else if (actor.mDirection == UP) {
		rotate = -90.0;
	}
	mAnimation->render(renderer, static_cast<int>(actor.mPosX) - camera->x, static_cast<int>(actor.mPosY) - camera->y, tick, actor.mScale, flip, rotate);
}

int GraphicsCompAnimated::getWidth() {
	return static_cast<int>(mTexture->getFrameWidth() * mTexture->getScaleX());
}

int GraphicsCompAnimated::getHeight() {
	return static_cast<int>(mTexture->getFrameHeight() * mTexture->getScaleY());
}