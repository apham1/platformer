#include "EScreenActorPicker.h"
#include "App.h"
#include "EToolCreate.h"

EScreenActorPicker::EScreenActorPicker(EditorScreen *parent) {
	mParent = parent;
}

bool EScreenActorPicker::onEntry(App &app) {
	mOverlayTexture.loadFromFile(app.mRenderer, "overlay");
	mOverlayTexture.setAlpha(100);

	mButtons.reserve(mParent->mTotalActorTypes);

	int buttonWidth = 200;
	int buttonHeight = 200;
	int buttonSpacing = 10;
	int leftMargin = 50;
	int topMargin = 50;
	int buttonX = leftMargin;
	int buttonY = topMargin;
	int selectedActorPosY = 0;
	for (int i = 0; i < mParent->mTotalActorTypes; ++i) {
		UIButtonActor *button = new UIButtonActor(buttonX, buttonY, buttonWidth, buttonHeight, ActorFactory::createActor(i), mParent->mCreateActorID == i);
		mButtons.push_back(*button);
		buttonX += buttonWidth + buttonSpacing;
		if (buttonX + buttonWidth > app.mScreenWidth) {
			buttonX = leftMargin;
			buttonY += buttonHeight + buttonSpacing;
		}

		// set scroll position to the currently selected actor
		if (mParent->mCreateActorID == i) {
			selectedActorPosY = buttonY;
		}
	}

	mScrollBar.init(app.mScreenWidth, app.mScreenHeight, buttonY + buttonHeight * 2, app.mScreenHeight);
	mScrollBar.setScrollAmount(selectedActorPosY);

	return true;
}

void EScreenActorPicker::onExit() {
	mOverlayTexture.free();

	mButtons.clear();
}

InputResult EScreenActorPicker::handleInput() {
	InputResult res;
	return res;
}

InputResult EScreenActorPicker::handleInput(App &app, SDL_Event &e) {
	InputResult res;
	if (mScrollBar.handleInput(e)) {
		res.inputConsumed = true;
	} else if (e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP) {
		res.inputConsumed = true;
		// buttons
		for (std::vector<UIButtonActor>::iterator it = mButtons.begin(); it != mButtons.end(); ++it) {
			if (it->isClicked()) {
				if (e.type == SDL_MOUSEBUTTONUP) {
					if (it->getActor()) {
						mParent->changeObject(it->getActor()->actorID, it->getActor()->mDirection);
						mParent->mCurrentTool = mParent->mToolCreate;
						app.popScreen();
					}
					break;
				}
			}
			it->updateButton(e.button.x, e.button.y + mScrollBar.getScrollAmount(), e.button.button == SDL_BUTTON_LEFT && e.type == SDL_MOUSEBUTTONDOWN);
		}
	} else if (e.type == SDL_MOUSEMOTION) {
		res.inputConsumed = true;
		for (std::vector<UIButtonActor>::iterator it = mButtons.begin(); it != mButtons.end(); ++it) {
			it->updateButton(e.motion.x, e.motion.y + mScrollBar.getScrollAmount(), e.motion.state & SDL_BUTTON_LMASK);
		}
	} else if (e.type == SDL_KEYDOWN) {
		switch (e.key.keysym.sym) {
			case SDLK_TAB:
			case SDLK_ESCAPE:
				res.inputConsumed = true;
				app.popScreen();
				break;
		}
	}
	return res;
}

void EScreenActorPicker::update(App &app) {

}

void EScreenActorPicker::render(App &app, int currentTicks) {
	mParent->render(app, currentTicks);

	mOverlayTexture.render(app.mRenderer, 0, 0, app.mScreenWidth, app.mScreenHeight);

	for (std::vector<UIButtonActor>::iterator it = mButtons.begin(); it != mButtons.end(); ++it) {
		it->render(app.mRenderer, mScrollBar.getScrollAmount(), currentTicks);
	}
	
	mScrollBar.render(app.mRenderer, currentTicks);
}

bool EScreenActorPicker::isStackedScreen() {
	return true;
}