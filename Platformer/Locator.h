#ifndef _LOCATOR_H
#define _LOCATOR_H

#include "GameAudio.h"

class Locator {
public:
	static Audio *getAudio();
	static void provide(Audio *service);

private:
	static Audio *mService;
};

#endif /* _LOCATOR_H */