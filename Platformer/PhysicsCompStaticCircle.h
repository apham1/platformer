#ifndef _PHYSICSCOMPSTATICCIRCLE_H
#define _PHYSICSCOMPSTATICCIRCLE_H

#include "PhysicsComp.h"
#include "VolumeCircle.h"

class PhysicsCompStaticCircle : public PhysicsComp {
public:
	PhysicsCompStaticCircle(float radius, CEvent *e = NULL) : PhysicsComp(e) {
		mVolume = new VolumeCircle(radius);
	}

	void update(Actor &actor) override {}
	void update(Actor &actor, std::vector<Actor> &objects) override {}
	void update(Actor &actor, std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) override {}

	/** Sets the actor's initial position and shifts colliders.
	@param actor The actor being changed
	@param x the x position
	@param y the y position
	*/
	void setPos(Actor &actor, float x, float y) override {
		actor.mPosX = x;
		actor.mPosY = y;

		mVolume->setPosition(actor.mPosX, actor.mPosY);
	}

	/** Sets the actor's initial position and shifts colliders.
	@param actor The actor being changed
	@param x the x position
	@param y the y position
	*/
	void setInitialPos(Actor &actor, float x, float y) override {
		actor.mPosX = x;
		actor.mPosY = y;

		mVolume->setInitialPosition(actor.mPosX, actor.mPosY);
	}
};
#endif /* _PHYSICSCOMPSTATICCIRCLE_H */
