#ifndef _PHYSICSCOMP_H
#define _PHYSICSCOMP_H

#include <vector>
#include "CEvent.h"

class Actor;

class PhysicsComp {
public:
	PhysicsComp(CEvent *e = NULL) {
		mVolume = NULL;
		mEvent = e;
		mGravity = 0.0f;
	}

	virtual ~PhysicsComp() {}
	virtual void update(Actor &actor) = 0;
	virtual void update(Actor &actor, std::vector<Actor> &objects) = 0;
	virtual void update(Actor &actor, std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) = 0;
	Volume *getVolume() { return mVolume; }
	virtual void setPos(Actor &actor, float x, float y) = 0;
	virtual void setInitialPos(Actor &actor, float x, float y) = 0;
	virtual void stand(Actor &actor) {};
	virtual void duck(Actor &actor) {};

	// currently does nothing
	virtual void setScale(float scale) {
		mVolume->setScale(scale);
	}

	virtual void changeHitbox(Volume *volume) {
		if (mVolume != volume) {
			mVolume = volume;
		}
	}

	void onEnterCollision(Actor &actor, Actor &object, CollisionData *cData) {
		if (mEvent) {
			mEvent->triggerEventEnter(actor, object, cData);
		}
	}

	void onExitCollision(Actor &actor, Actor &object) {
		if (mEvent) {
			mEvent->triggerEventExit(actor, object);
		}
	}

	float mGravity;

protected:
	Volume *mVolume;
	CEvent *mEvent;
};

#endif /* _PHYSICSCOMP_H */
