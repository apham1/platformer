#include <assert.h>
#include "PhysicsCompPlayer.h"
#include "Actor.h"

/** Updates the actor physics state and handles collision.
	@param actor The actor to update
	@param colliders The list of colliders
*/
void PhysicsCompPlayer::update(Actor &actor, std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) {
	if (!actor.mSling.hasSlung) {
		actor.mVelY += mGravity;
	}
	actor.mVelX += actor.mAcceleration;

	if (actor.mIsKnockedBack) {
		actor.mVelX -= actor.mAcceleration;
		applyFriction(actor, 0.5f);

		if (actor.mVelX < -mMaxPlayerSpeed * 3.0f) {
			actor.mVelX = -mMaxPlayerSpeed * 3.0f;
		} else if (actor.mVelX > mMaxPlayerSpeed * 3.0f) {
			actor.mVelX = mMaxPlayerSpeed * 3.0f;
		}
	} else if (actor.mSling.isSlinging) {
		applyFriction(actor, 0.25f);

		if (actor.mSling.xSpeed > mMaxPlayerSpeed) {
			if (actor.mVelX < actor.mSling.xSpeed) {
				actor.mSling.xSpeed = actor.mVelX;
			}

			if (actor.mVelX < -mMaxPlayerSpeed) {
				actor.mVelX = -mMaxPlayerSpeed;
			} else if (actor.mVelX > actor.mSling.xSpeed) {
				actor.mVelX = actor.mSling.xSpeed;
			}
		} else if (actor.mSling.xSpeed < -mMaxPlayerSpeed) {
			if (actor.mVelX > actor.mSling.xSpeed) {
				actor.mSling.xSpeed = actor.mVelX;
			}

			if (actor.mVelX > mMaxPlayerSpeed) {
				actor.mVelX = mMaxPlayerSpeed;
			} else if (actor.mVelX < actor.mSling.xSpeed) {
				actor.mVelX = actor.mSling.xSpeed;
			}
		} else if (actor.mSling.xSpeed >= -mMaxPlayerSpeed && actor.mSling.xSpeed <= mMaxPlayerSpeed) {
			if (actor.mVelX < -mMaxPlayerSpeed) {
				actor.mVelX = -mMaxPlayerSpeed;
			} else if (actor.mVelX > mMaxPlayerSpeed) {
				actor.mVelX = mMaxPlayerSpeed;
			}
		}
	} else {
		applyFriction(actor);

		if (actor.mVelX < -mMaxPlayerSpeed) {
			actor.mVelX = -mMaxPlayerSpeed;
		} else if (actor.mVelX > mMaxPlayerSpeed) {
			actor.mVelX = mMaxPlayerSpeed;
		}
	}

	if (actor.mIsInvincible) {
		++actor.mInvincibleCount;
		if (actor.mInvincibleCount >= 75) {
			actor.mIsInvincible = false;
			actor.mInvincibleCount = 0;
		}
	}

	setPos(actor, actor.mPosX + actor.mVelX,
				  actor.mPosY + actor.mVelY);

	// Set airborne to true here, collision resolution will determine if the actor is not airborne
	actor.mAirborne = true;
	handleCollision(actor, objects);

	std::vector<std::vector<Actor>*>::iterator it;
	for (it = projectiles.begin(); it != projectiles.end(); ++it) {
		handleCollision(actor, **it);
	}
}

void PhysicsCompPlayer::applyFriction(Actor &actor, float multiplier) {
	if (actor.mVelX < 0.0f) {
		if (actor.mVelX + mFriction * multiplier > 0.0f) {
			actor.mVelX = 0.0f;
		} else {
			actor.mVelX += mFriction * multiplier;
		}
	} else if (actor.mVelX > 0.0f) {
		if (actor.mVelX - mFriction * multiplier < 0.0f) {
			actor.mVelX = 0.0f;
		} else {
			actor.mVelX -= mFriction * multiplier;
		}
	}
}

void PhysicsCompPlayer::handleCollision(Actor &actor, std::vector<Actor> &objects) {
	std::vector<Actor>::iterator it;
	for (it = objects.begin(); it != objects.end(); ++it) {
		if (it->mIsAlive) {
			assert((*it).physics() != NULL);
			CollisionData *penetration = (*it).physics()->getVolume()->isColliding(static_cast<VolumeRect*>(mVolume));
			if (penetration) {
				(*it).physics()->onEnterCollision(actor, (*it), penetration);
			} else {
				(*it).physics()->onExitCollision(actor, (*it));
			}
		}
	}
}

/** Sets the position of the actor.
	@param actor The actor being changed
	@param x The x position
	@param y The y position
*/
void PhysicsCompPlayer::setPos(Actor &actor, float x, float y) {
	actor.mPosX = x;
	actor.mPosY = y;

	VolumeRect *rectPt = static_cast<VolumeRect*>(mVolume);

	mVolume->setPosition(actor.mPosX - rectPt->w / 2, actor.mPosY - rectPt->h / 2);
}

/** Sets the initial position of the actor.
	@param actor The actor being changed
	@param x The x position
	@param y The y position
*/
void PhysicsCompPlayer::setInitialPos(Actor &actor, float x, float y) {
	actor.mPosX = x;
	actor.mPosY = y;

	VolumeRect *rectPt = static_cast<VolumeRect*>(mVolume);

	mVolume->setInitialPosition(actor.mPosX - rectPt->w / 2, actor.mPosY - rectPt->h / 2);
}

void PhysicsCompPlayer::stand(Actor &actor) {
	changeHitbox(mStandingVolume);
}

void PhysicsCompPlayer::duck(Actor &actor) {
	if (mVolume != mDuckingVolume) {
		changeHitbox(mDuckingVolume);
		actor.mPosY += (mStandingVolume->getHeight() - mDuckingVolume->getHeight()) / 2 - 1; // Moves player position down so the player doesn't fall from their original height after ducking	
	}
}