#ifndef _VOLUMERECT_H
#define _VOLUMERECT_H

#include <algorithm>
#include "Volume.h"

class VolumeRect : public Volume {
public:
	VolumeRect(float width, float height) : Volume() {
		w = width;
		h = height;
	}

	~VolumeRect() {}

	/** Checks if the objects are colliding.
		@param rect
	    @return the collision data
	*/
	CollisionData *isColliding(VolumeRect *rect) override {
		VolumeRect convertedRect = convertFromNegativeDimensions();
		VolumeRect convertedOther = rect->convertFromNegativeDimensions();

		float left = convertedRect.x();
		float right = left + convertedRect.w;
		float top = convertedRect.y();
		float bottom = top + convertedRect.h;

		float leftOther = convertedOther.x();
		float rightOther = leftOther + convertedOther.w;
		float topOther = convertedOther.y();
		float bottomOther = topOther + convertedOther.h;

		bool horizontalCollision = (right > leftOther) && (left < rightOther);
		bool verticalCollision = (bottom > topOther) && (top < bottomOther);

		CollisionData *cData = NULL;
		if (horizontalCollision && verticalCollision) {
			float oldLeft = convertedRect.xOld();
			float oldRight = oldLeft + convertedRect.w;
			float oldTop = convertedRect.yOld();
			float oldBottom = oldTop + convertedRect.h;

			float oldLeftOther = convertedOther.xOld();
			float oldRightOther = oldLeftOther + convertedOther.w;
			float oldTopOther = convertedOther.yOld();
			float oldBottomOther = oldTopOther + convertedOther.h;

			float xPen = std::min(std::abs(rightOther - left), std::abs(right - leftOther));
			float yPen = std::min(std::abs(bottomOther - top), std::abs(bottom - topOther));

			// Top side collision
			if (oldBottomOther <= oldTop) {
				xPen = 0.0f;
			// Bottom side collision
			} else if (oldTopOther >= oldBottom) {
				xPen = 0.0f;
				yPen = -yPen;
			// Left side collision
			} else if (oldRightOther <= oldLeft) {
				yPen = 0.0f;
			// Right side collision
			} else if (oldLeftOther >= oldRight) {
				yPen = 0.0f;
				xPen = -xPen;
			}

			cData = new CollisionData(xPen, yPen);
		}

		return cData;
	}

	/** Checks if this rectangle is colliding with a circle.
		@param circle A circular object.
		@return Collision data delegated from parent class.
	*/
	CollisionData *isColliding(VolumeCircle *circle) override {
		return Volume::isColliding(this, circle);
	}

	/** @return a rectangle that can be used to render to display the position of the volume
	*/
	SDL_Rect getRenderRect() override {
		SDL_Rect renderRect = { static_cast<int>(x()), static_cast<int>(y()), static_cast<int>(w), static_cast<int>(h) };
		return renderRect;
	}

	/** @return an equivalent VolumeRect with no negative dimensions
	*/
	VolumeRect convertFromNegativeDimensions() {
		if (w > 0.0f && h > 0.0f) {
			return *this;
		} else {
			VolumeRect converted = copy();
			if (w < 0.0f) {
				converted.w = -converted.w;
				converted.xPos[0] -= converted.w;
				converted.xPos[1] -= converted.w;
			}
			if (h < 0.0f) {
				converted.h = -converted.h;
				converted.yPos[0] -= converted.h;
				converted.yPos[1] -= converted.h;
			}
			return converted;
		}
	}

	/** @return a copy of the VolumeRect
	*/
	VolumeRect copy() {
		VolumeRect copy(w, h);
		copy.xPos[0] = xPos[0];
		copy.xPos[1] = xPos[1];
		copy.yPos[0] = yPos[0];
		copy.yPos[1] = yPos[1];
		copy.current = current;
		copy.mScale = mScale;
		return copy;
	}

	float getWidth() override {
		return w;
	}

	float getHeight() override {
		return h;
	}

	float w;
	float h;
};

#endif /* _VOLUMERECT_H */