#ifndef _PHYSICSCOMPPLAYER_H
#define _PHYSICSCOMPPLAYER_H

#include "PhysicsComp.h"
#include "VolumeRect.h"

class PhysicsCompPlayer : public PhysicsComp {
public:
	PhysicsCompPlayer(CEvent *e = NULL) : PhysicsComp(e) {
		mStandingVolume = new VolumeRect(50.0f, 80.0f);
		mDuckingVolume = new VolumeRect(50.0f, 50.0f);
		mVolume = mStandingVolume;
		mGravity = 0.5f;
		mFriction = 0.4f;
		mMaxPlayerSpeed = 5.0f;
	}
	void update(Actor &actor) override {}
	void update(Actor &actor, std::vector<Actor> &objects) override {}
	void update(Actor &actor, std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles) override;
	void setPos(Actor &actor, float x, float y) override;
	void setInitialPos(Actor &actor, float x, float y) override;
	void stand(Actor &actor) override;
	void duck(Actor &actor) override;

	void applyFriction(Actor &actor, float multiplier = 1.0f);
	void handleCollision(Actor &actor, std::vector<Actor> &objects);

	float mFriction, mMaxPlayerSpeed;

private:
	Volume *mStandingVolume, *mDuckingVolume;
};
#endif /* _PHYSICSCOMPPLAYER_H */