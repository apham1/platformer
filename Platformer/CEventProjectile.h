#ifndef _CEVENTPROJECTILE_H
#define _CEVENTPROJECTILE_H

#include "CEventHit.h"

class CEventProjectile : public CEventHit {
public:
	void triggerEventEnter(Actor &actor, Actor &object, CollisionData *cData) override {
		CEventHit::triggerEventEnter(actor, object, cData);
		object.mIsAlive = false;
	}
};

#endif /* _CEVENTPROJECTILE_H */