#ifndef _GRAPHICSCOMP_H
#define _GRAPHICSCOMP_H

#include "Texture.h"

class Actor;

class GraphicsComp {
public:
	GraphicsComp() {
		mAlpha = 255;
	}
	virtual ~GraphicsComp() {}
	virtual void render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) = 0;
	virtual int getWidth() = 0;
	virtual int getHeight() = 0;

	virtual void changeTexture(Texture *texture) {
		if (mTexture != texture) {
			mTexture = texture;
		}
	}

	virtual void setAlpha(int alpha) {
		mAlpha = alpha;
	}

	virtual void stand() {};
	virtual void duck() {};

protected:
	int mAlpha;
	Texture *mTexture;
};

#endif /* _GRAPHICSCOMP_H */
