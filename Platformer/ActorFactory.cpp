#include "ActorFactory.h"
#include "GunComp.h"
#include "GraphicsCompCannon.h"

Texture ActorFactory::mPlayerTexture;
Texture ActorFactory::mPlayerDuckingTexture;
Texture ActorFactory::mWallTexture;
Texture ActorFactory::mWallHorizontalTexture;
Texture ActorFactory::mWallVerticalTexture;
Texture ActorFactory::mSlingMediumTexture;
Texture ActorFactory::mSlingLargeTexture;
Texture ActorFactory::mExitTexture;
Texture ActorFactory::mDeathFloorTexture;
Texture ActorFactory::mDeathBlockTexture;
Texture ActorFactory::mBoxTexture;
Texture ActorFactory::mPlatformTexture;
Texture ActorFactory::mPlatformSmallTexture;
Texture ActorFactory::mPlatformBigTexture;
Texture ActorFactory::mPlatformThinTexture;
Texture ActorFactory::mPillarTexture;
Texture ActorFactory::mNinjaStarTexture;
Texture ActorFactory::mCrossbowTexture;
Texture ActorFactory::mCannonTexture;
Texture ActorFactory::mLightningBallTexture;
Texture ActorFactory::mFloorTexture;
Texture ActorFactory::mFloorConnTexture;
Texture ActorFactory::mFloorRightEndTexture;
Texture ActorFactory::mFloorLeftEndTexture;
Texture ActorFactory::mWallPieceTexture;
Texture ActorFactory::mWallConnTexture;
Texture ActorFactory::mWallTopTexture;
Texture ActorFactory::mWallBottomTexture;
Texture ActorFactory::mShockwaveTexture;
Texture ActorFactory::mGeneratorTexture;
Texture ActorFactory::mWidePlatformTexture;
Texture ActorFactory::mFloorSmallTexture;
Texture ActorFactory::mCrateTexture;

Actor *ActorFactory::createPlayer(float x, float y) {
	Actor *player = new Actor(1.0f, new GraphicsCompPlayer(&mPlayerTexture, &mPlayerDuckingTexture),
									new PhysicsCompPlayer(),
									new InputCompPlayer());
	player->setInitialPos(x, y);

	return player;
}

Actor *ActorFactory::createSmallWall(float x, float y) {
	Actor *wall = new Actor(0.0f, new GraphicsCompStatic(&mWallTexture),
								  new PhysicsCompStaticRect(64.0f, 64.0f, new CEventWall()));
	wall->setInitialPos(x, y);

	return wall;
}

Actor *ActorFactory::createHorizontalWall(float x, float y) {
	Actor *wall = new Actor(0.0f, new GraphicsCompStatic(&mWallHorizontalTexture),
								  new PhysicsCompStaticRect(600.0f, 64.0f, new CEventWall()));
	wall->setInitialPos(x, y);

	return wall;
}

Actor *ActorFactory::createVerticalWall(float x, float y) {
	Actor *wall = new Actor(0.0f, new GraphicsCompAnimated(&mWallVerticalTexture, "glowing"),
								  new PhysicsCompStaticRect(100.0f, 775.0f, new CEventWall()));
	wall->setInitialPos(x, y);

	return wall;
}

Actor *ActorFactory::createMediumSling(float x, float y) {
	Actor *sling = new Actor(0.0f, new GraphicsCompStatic(&mSlingMediumTexture),
								   new PhysicsCompStaticCircle(125.0f, new CEventField()));
	sling->setSlingSpeed(20.0f);
	sling->setInitialPos(x, y);

	return sling;
}

Actor *ActorFactory::createLargeSling(float x, float y) {
	Actor *sling = new Actor(0.0f, new GraphicsCompStatic(&mSlingLargeTexture),
								   new PhysicsCompStaticCircle(150.0f, new CEventField()));
	sling->setSlingSpeed(20.0f);
	sling->setInitialPos(x, y);

	return sling;
}

Actor *ActorFactory::createExit(float x, float y) {
	Actor *exit = new Actor(0.0f, new GraphicsCompStatic(&mExitTexture),
								  new PhysicsCompStaticRect(50.0f, 100.0f, new CEventWin()));
	exit->setInitialPos(x, y);

	return exit;
}

Actor *ActorFactory::createDeathFloor(float x, float y) {
	Actor *deathFloor = new Actor(0.0f, new GraphicsCompStatic(&mDeathFloorTexture),
										new PhysicsCompStaticRect(8000.0f, 64.0f, new CEventDeath()));
	deathFloor->setInitialPos(x, y);

	return deathFloor;
}

Actor *ActorFactory::createDeathBlock(float x, float y) {
	Actor *deathBlock = new Actor(0.0f, new GraphicsCompStatic(&mDeathBlockTexture),
										new PhysicsCompStaticRect(64.0f, 64.0f, new CEventDeath()));
	deathBlock->setInitialPos(x, y);

	return deathBlock;
}

Actor *ActorFactory::createPlatformBox(float x, float y) {
	Actor *platform = new Actor(0.0f, new GraphicsCompAnimated(&mPlatformTexture, "hovering"),
									  new PhysicsCompStaticRect(135.0f, 104.0f, new CEventPlatform()));
	platform->setInitialPos(x, y);

	return platform;
}

Actor *ActorFactory::createNinjaStar(float x, float y) {
	Actor *ninjaStar = new Actor(0.0f, new GraphicsCompStatic(&mNinjaStarTexture),
									   new PhysicsCompProjectileCircle(10.5f, 10.0f, new CEventHit()));
	ninjaStar->setInitialPos(x, y);
	ninjaStar->mIsAlive = false;

	return ninjaStar;
}

Actor *ActorFactory::createGunHorizontal(float x, float y) {
	Actor *crossbow = new Actor(0.0f, new GraphicsCompStatic(&mCrossbowTexture),
									  new PhysicsCompStaticRect(50.0f, 50.0f));
	crossbow->setInitialPos(x, y);
	crossbow->setGunComponent(new GunComp(100, 50, 0));

	return crossbow;
}

Actor *ActorFactory::createGunUp(float x, float y) {
	Actor *crossbow = new Actor(0.0f, new GraphicsCompStatic(&mCrossbowTexture),
									  new PhysicsCompStaticRect(50.0f, 50.0f));
	crossbow->setInitialPos(x, y);
	crossbow->setGunComponent(new GunComp(100, 50, 0));

	return crossbow;
}

Actor *ActorFactory::createGunDown(float x, float y) {
	Actor *crossbow = new Actor(0.0f, new GraphicsCompStatic(&mCrossbowTexture),
									  new PhysicsCompStaticRect(50.0f, 50.0f));
	crossbow->setInitialPos(x, y);
	crossbow->setGunComponent(new GunComp(100, 50, 0));

	return crossbow;
}

Actor *ActorFactory::createPlatformSmall(float x, float y) {
	Actor *platform = new Actor(0.0f, new GraphicsCompAnimated(&mPlatformSmallTexture, "hovering"),
									  new PhysicsCompStaticRect(40.0f, 43.0f, new CEventPlatform()));
	platform->setInitialPos(x, y);

	return platform;
}

Actor *ActorFactory::createPlatformBig(float x, float y) {
	Actor *platform = new Actor(0.0f, new GraphicsCompStatic(&mPlatformBigTexture),
									  new PhysicsCompStaticRect(695.0f, 190.0f, new CEventPlatform()));
	platform->setInitialPos(x, y);

	return platform;
}

Actor *ActorFactory::createPlatformThin(float x, float y) {
	Actor *platform = new Actor(0.0f, new GraphicsCompStatic(&mPlatformThinTexture),
		new PhysicsCompStaticRect(85.0f, 10.0f, new CEventPlatform()));
	platform->setInitialPos(x, y);

	return platform;
}

Actor *ActorFactory::createPillar(float x, float y) {
	Actor *pillar = new Actor(0.0f, new GraphicsCompStatic(&mPillarTexture),
									new PhysicsCompStaticRect(135.0f, 450.0f));
	pillar->setInitialPos(x, y);

	return pillar;
}

Actor *ActorFactory::createCannon(float x, float y) {
	Actor *cannon = new Actor(0.0f, new GraphicsCompCannon(&mCannonTexture, "idle"),
									new PhysicsCompStaticRect(50.0f, 50.0f));
	cannon->setInitialPos(x, y);
	cannon->setGunComponent(new GunComp(100, 50, 0, 0, 60, -5));
	return cannon;
}

Actor *ActorFactory::createLightningBall(float x, float y) {
	Actor *lightningBall = new Actor(0.0f, new GraphicsCompAnimated(&mLightningBallTexture, "flying"),
									   new PhysicsCompProjectileCircle(10.5f, 10.0f, new CEventHit()));
	lightningBall->setInitialPos(x, y);
	lightningBall->mIsAlive = false;

	return lightningBall;
}

Actor *ActorFactory::createFloor(float x, float y) {
	Actor *floor = new Actor(0.0f, new GraphicsCompStatic(&mFloorTexture),
		new PhysicsCompStaticRect(570.0f, 50.0f, new CEventWall()));
	floor->setInitialPos(x, y);

	return floor;
}

Actor *ActorFactory::createFloorConnector(float x, float y) {
	Actor *floorConnector = new Actor(0.0f, new GraphicsCompStatic(&mFloorConnTexture),
		new PhysicsCompStaticRect(60.0f, 50.0f, new CEventWall()));
	floorConnector->setInitialPos(x, y);

	return floorConnector;
}

Actor *ActorFactory::createFloorLeftEnd(float x, float y) {
	Actor *floorLeftEnd = new Actor(0.0f, new GraphicsCompStatic(&mFloorLeftEndTexture),
		new PhysicsCompStaticRect(32.0f, 50.0f, new CEventWall()));
	floorLeftEnd->setInitialPos(x, y);

	return floorLeftEnd;
}

Actor *ActorFactory::createFloorRightEnd(float x, float y) {
	Actor *floorRightEnd = new Actor(0.0f, new GraphicsCompStatic(&mFloorRightEndTexture),
		new PhysicsCompStaticRect(32.0f, 50.0f, new CEventWall()));
	floorRightEnd->setInitialPos(x, y);

	return floorRightEnd;
}

Actor *ActorFactory::createWallPiece(float x, float y) {
	Actor *wallPiece = new Actor(0.0f, new GraphicsCompAnimated(&mWallPieceTexture, "glowing"),
		new PhysicsCompStaticRect(100.0f, 320.0f, new CEventWallMiddle()));
	wallPiece->setInitialPos(x, y);

	return wallPiece;
}

Actor *ActorFactory::createWallConnector(float x, float y) {
	Actor *wallConnector = new Actor(0.0f, new GraphicsCompAnimated(&mWallConnTexture, "glowing"),
		new PhysicsCompStaticRect(100.0f, 200.0f, new CEventWallMiddle()));
	wallConnector->setInitialPos(x, y);

	return wallConnector;
}

Actor *ActorFactory::createWallTop(float x, float y) {
	Actor *wallTop = new Actor(0.0f, new GraphicsCompAnimated(&mWallTopTexture, "glowing"),
		new PhysicsCompStaticRect(100.0f, 195.0f, new CEventWallTop()));
	wallTop->setInitialPos(x, y);

	return wallTop;
}

Actor *ActorFactory::createWallBottom(float x, float y) {
	Actor *wallBottom = new Actor(0.0f, new GraphicsCompAnimated(&mWallBottomTexture, "glowing"),
		new PhysicsCompStaticRect(100.0f, 195.0f, new CEventWallBottom()));
	wallBottom->setInitialPos(x, y);

	return wallBottom;
}

Actor *ActorFactory::createShockwave(float x, float y) {
	Actor *shockwave = new Actor(0.0f, new GraphicsCompStatic(&mShockwaveTexture),
									   new PhysicsCompProjectileCircle(125.0f, 0.0f, new CEventHit()));
	shockwave->setInitialPos(x, y);
	shockwave->mIsAlive = false;

	return shockwave;
}

Actor *ActorFactory::createShockwaveGenerator(float x, float y) {
	Actor *generator = new Actor(0.0f, new GraphicsCompStatic(&mGeneratorTexture),
									   new PhysicsCompStaticRect(25.0f, 25.0f));
	generator->setInitialPos(x, y);
	generator->setGunComponent(new GunComp(100, 15, 0, 1));

	return generator;
}

Actor *ActorFactory::createWidePlatform(float x, float y) {
	Actor *widePlatform = new Actor(0.0f, new GraphicsCompStatic(&mWidePlatformTexture),
										  new PhysicsCompStaticRect(250.0f, 64.0f, new CEventPlatform()));
	widePlatform->setInitialPos(x, y);

	return widePlatform;
}

Actor *ActorFactory::createFloorSmall(float x, float y) {
	Actor *floorSmall = new Actor(0.0f, new GraphicsCompStatic(&mFloorSmallTexture),
		new PhysicsCompStaticRect(215.0f, 50.0f, new CEventWall()));
	floorSmall->setInitialPos(x, y);

	return floorSmall;
}

Actor *ActorFactory::createCrate(float x, float y) {
	Actor *crate = new Actor(0.0f, new GraphicsCompStatic(&mCrateTexture),
		new PhysicsCompStaticRect(75.0f, 67.0f, new CEventWall()));
	crate->setInitialPos(x, y);

	return crate;
}

void ActorFactory::free() {
	mPlayerTexture.free();
	mPlayerDuckingTexture.free();
	mWallTexture.free();
	mBoxTexture.free();
	mWallHorizontalTexture.free();
	mWallVerticalTexture.free();
	mDeathBlockTexture.free();
	mExitTexture.free();
	mDeathFloorTexture.free();
	mPlatformTexture.free();
	mPlatformSmallTexture.free();
	mPlatformBigTexture.free();
	mPlatformThinTexture.free();
	mPillarTexture.free();
	mSlingMediumTexture.free();
	mSlingLargeTexture.free();
	mNinjaStarTexture.free();
	mCrossbowTexture.free();
	mCannonTexture.free();
	mLightningBallTexture.free();
	mFloorTexture.free();
	mFloorConnTexture.free();
	mFloorLeftEndTexture.free();
	mFloorRightEndTexture.free();
	mWallPieceTexture.free();
	mWallConnTexture.free();
	mWallTopTexture.free();
	mWallBottomTexture.free();
	mShockwaveTexture.free();
	mGeneratorTexture.free();
	mWidePlatformTexture.free();
	mFloorSmallTexture.free();
}

bool ActorFactory::loadMedia(SDL_Renderer *renderer) {
	if (!mPlayerTexture.loadFromFile(renderer, "player")) {
		return false;
	}

	if (!mPlayerDuckingTexture.loadFromFile(renderer, "playerducking")) {
		return false;
	}

	if (!mWallTexture.loadFromFile(renderer, "wall")) {
		return false;
	}

	if (!mWallHorizontalTexture.loadFromFile(renderer, "wallhorizontal")) {
		return false;
	}

	if (!mWallVerticalTexture.loadFromFile(renderer, "wallvert", 0.5f, 0.5f)) {
		return false;
	}

	if (!mSlingMediumTexture.loadFromFile(renderer, "batteryfield2")) {
		return false;
	}

	if (!mSlingLargeTexture.loadFromFile(renderer, "batteryfield3")) {
		return false;
	}

	if (!mExitTexture.loadFromFile(renderer, "exit")) {
		return false;
	}

	if (!mDeathFloorTexture.loadFromFile(renderer, "deathplatform")) {
		return false;
	}

	if (!mDeathBlockTexture.loadFromFile(renderer, "deathblock")) {
		return false;
	}

	if (!mBoxTexture.loadFromFile(renderer, "box")) {
		return false;
	}

	if (!mPlatformTexture.loadFromFile(renderer, "platform", 0.3f, 0.3f)) {
		return false;
	}

	if (!mPlatformSmallTexture.loadFromFile(renderer, "platformsmall", 0.3f, 0.3f)) {
		return false;
	}

	if (!mPlatformBigTexture.loadFromFile(renderer, "platformbig", 0.6f, 0.6f)) {
		return false;
	}

	if (!mPillarTexture.loadFromFile(renderer, "pillar", 0.6f, 0.6f)) {
		return false;
	}

	if (!mNinjaStarTexture.loadFromFile(renderer, "ninjastar")) {
		return false;
	}

	if (!mCrossbowTexture.loadFromFile(renderer, "crossbow")) {
		return false;
	}

	if (!mPlatformThinTexture.loadFromFile(renderer, "platformthin", 0.45f, 0.45f)) {
		return false;
	}

	if (!mCannonTexture.loadFromFile(renderer, "cannon", 0.5f, 0.5f)) {
		return false;
	}

	if (!mLightningBallTexture.loadFromFile(renderer, "lightningball", 0.75f, 0.75f)) {
		return false;
	}

	if (!mFloorTexture.loadFromFile(renderer, "floor", 0.5f, 0.5f)) {
		return false;
	}

	if (!mFloorConnTexture.loadFromFile(renderer, "floorconnector", 0.5f, 0.5f)) {
		return false;
	}

	if (!mFloorLeftEndTexture.loadFromFile(renderer, "floorleftend", 0.5f, 0.5f)) {
		return false;
	}

	if (!mFloorRightEndTexture.loadFromFile(renderer, "floorrightend", 0.5f, 0.5f)) {
		return false;
	}

	if (!mWallPieceTexture.loadFromFile(renderer, "wallpiece")) {
		return false;
	}

	if (!mWallConnTexture.loadFromFile(renderer, "wallconnector")) {
		return false;
	}

	if (!mWallTopTexture.loadFromFile(renderer, "walltop")) {
		return false;
	}

	if (!mWallBottomTexture.loadFromFile(renderer, "wallbottom")) {
		return false;
	}

	if (!mShockwaveTexture.loadFromFile(renderer, "shockwave")) {
		return false;
	}

	if (!mGeneratorTexture.loadFromFile(renderer, "shockwavegenerator")) {
		return false;
	}

	if (!mWidePlatformTexture.loadFromFile(renderer, "wideplatform")) {
		return false;
	}

	if (!mFloorSmallTexture.loadFromFile(renderer, "floorsmall", 0.5f, 0.5f)) {
		return false;
	}

	if (!mCrateTexture.loadFromFile(renderer, "crate", 0.35f, 0.35f)) {
		return false;
	}

	return true;
}

Actor *ActorFactory::createActor(int ID, float x, float y, Direction direction) {
	Actor *actor = NULL;

	switch (ID) {
		case -2:
			actor = ActorFactory::createPlayer(x, y);
			break;
		case 0:
			actor = ActorFactory::createSmallWall(x, y);
			break;
		case 1:
			actor = ActorFactory::createHorizontalWall(x, y);
			break;
		case 2:
			actor = ActorFactory::createVerticalWall(x, y);
			break;
		case 3:
			actor = ActorFactory::createPlatformBox(x, y);
			break;
		case 4:
			actor = ActorFactory::createMediumSling(x, y);
			break;
		case 5:
			actor = ActorFactory::createLargeSling(x, y);
			break;
		case 6:
			actor = ActorFactory::createDeathBlock(x, y);
			break;
		case 7:
			actor = ActorFactory::createDeathFloor(x, y);
			break;
		case 8:
			actor = ActorFactory::createExit(x, y);
			break;
		case 9:
			actor = ActorFactory::createGunHorizontal(x, y);
			break;
		case 10:
			actor = ActorFactory::createPlatformThin(x, y);
			break;
		case 11:
			actor = ActorFactory::createPlatformSmall(x, y);
			break;
		case 12:
			actor = ActorFactory::createPlatformBig(x, y);
			break;
		case 13:
			actor = ActorFactory::createPillar(x, y);
			break;
		case 14:
			actor = ActorFactory::createGunUp(x, y);
			ActorFactory::setData(actor, ID, UP);
			return actor;
		case 15:
			actor = ActorFactory::createGunDown(x, y);
			ActorFactory::setData(actor, ID, DOWN);
			return actor;
		case 16:
			actor = ActorFactory::createCannon(x, y);
			break;
		case 17:
			actor = ActorFactory::createFloor(x, y);
			break;
		case 18:
			actor = ActorFactory::createFloorConnector(x, y);
			break;
		case 19:
			actor = ActorFactory::createFloorLeftEnd(x, y);
			break;
		case 20:
			actor = ActorFactory::createFloorRightEnd(x, y);
			break;
		case 21:
			actor = ActorFactory::createWallPiece(x, y);
			break;
		case 22:
			actor = ActorFactory::createWallConnector(x, y);
			break;
		case 23:
			actor = ActorFactory::createWallTop(x, y);
			break;
		case 24:
			actor = ActorFactory::createWallBottom(x, y);
			break;
		case 25:
			actor = ActorFactory::createShockwaveGenerator(x, y);
			break;
		case 26:
			actor = ActorFactory::createWidePlatform(x, y);
			break;
		case 27:
			actor = ActorFactory::createFloorSmall(x, y);
			break;
		case 28:
			actor = ActorFactory::createCrate(x, y);
			break;
	}

	if (actor) {
		ActorFactory::setData(actor, ID, direction);
	}
	return actor;
}

void ActorFactory::setData(Actor *actor, int ID, Direction direction) {
	actor->actorID = ID;
	actor->mDirection = direction;
}