#ifndef _GAMEAUDIO_H
#define _GAMEAUDIO_H

#include <SDL.h>
#include <SDL_mixer.h>
#include <assert.h>
#include <vector>
#include "Audio.h"

class GameAudio : public Audio {
public:
	/** Plays all sounds currently queued and deletes them from the list.
	*/
	void playQueuedSounds() {
		std::vector<SoundData>::iterator it;
		for (it = mSounds.begin(); it != mSounds.end(); ++it) {
			playSound(*it);
		}
		mSounds.clear();
	}

	/** Adds a sound to the queue to be played.
		@param Sound the sound being added
	*/
	void addSoundToQueue(Sound sound, int volume) {
		std::vector<SoundData>::iterator it;
		bool isDuplicate = false;
		for (it = mSounds.begin(); it != mSounds.end(); ++it) {
			if (sound == it->sound) {
				isDuplicate = true;
				if (volume > it->volume) {
					it->volume = volume;
				}
			}
		}
		
		if (!isDuplicate) {
			SoundData soundData(sound, volume);
			mSounds.push_back(soundData);
		}
	}

	/** Initializes mixer audio, allocats channels, and loads sound files.
		@return true if successfully initialized
	*/
	bool init() override {
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
			SDL_Log("Unable to initialize audio: %s", Mix_GetError());
			return false;
		}

		Mix_AllocateChannels(mMaxChannels);
		loadMedia();
		mSounds.reserve(200);

		return true;
	}

	/** Closes mixer and frees all sound files.
	*/
	void close() override {
		for (int i = 0; i < TOTAL_SOUNDS; i++) {
			Mix_FreeChunk(mChunk[i]);
		}

		Mix_Quit();
	}

private:
	/** Plays a sound to the first open channel. If there are no open channels, no sound is played.
		@param sound Sound to be played
		@param volume Volume of the sound
	*/
	void playSound(SoundData soundData) override {
		int channel = getOpenChannel();
		if (channel != -1) {
			Mix_Volume(channel, soundData.volume);
			Mix_PlayChannel(channel, mChunk[soundData.sound], 0);
		}
	}

	/** Finds the first open channel.
		@return the first open channel
	*/
	int getOpenChannel() {
		for (int i = 0; i < mMaxChannels; i++) {
			if (!Mix_Playing(i)) {
				return i;
			}
		}

		return -1;
	}

	/** Loads sound files.
	*/
	void loadMedia() {
		loadChunk(JUMP, "Sounds/jump.wav");
		loadChunk(SHOOT, "Sounds/laser.wav");
	}

	/** Loads a chunk into an array of sounds.
		@param sound The sound being loaded
		@param path The directory of the file
	*/
	void loadChunk(Sound sound, const char *path) {
		mChunk[sound] = Mix_LoadWAV(path);
		assert(mChunk[sound]);
	}

	const int mMaxChannels = 16;
	Mix_Chunk *mChunk[TOTAL_SOUNDS];
	std::vector<SoundData> mSounds;
};

#endif /* _GAMEAUDIO_H */