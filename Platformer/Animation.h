#ifndef _ANIMATION_H
#define _ANIMATION_H

#include <map>
#include "Texture.h"

class Animation {
public:
	Animation(Texture *texture, const char *defaultAnimSequence);

	void setTexture(Texture *texture);
	void render(SDL_Renderer *renderer, int x, int y, int tick, float scale = 1.0f, SDL_RendererFlip flip = SDL_FLIP_NONE, double angle = 0.0, SDL_Point* rotateCenter = NULL);
	void setAlpha(int alpha);
	void setSequence(const char *animationSeq, bool isLooping = false);
	void setDefaultSequence(const char *animationSeq, bool isLooping = true);

private:
	struct AnimSequence {
		std::string sequenceName;
		bool isLooping;

		AnimSequence(const char *sequenceName, bool isLooping) {
			this->sequenceName = sequenceName;
			this->isLooping = isLooping;
		}
	};

	void setSequence(AnimSequence *animSeq);
	void freeAnimSequence(AnimSequence *&animSeq);

	Texture* mTexture;
	int mStartTick;
	AnimSequence *mCurSequence;
	AnimSequence *mDefaultSequence;
};

#endif /* _ANIMATION_H */