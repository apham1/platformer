#ifndef _UISCROLLBAR_H
#define _UISCROLLBAR_H

#include <SDL.h>

class UIScrollBar {
public:
	void init(int screenWidth, int screenHeight, int bottom, int height, int startPos = 0, SDL_Color barColor = { 0x53, 0x81, 0xCC, 0xFF }, SDL_Color backgroundColor = { 0xC6, 0xDC, 0xFF, 0xFF }) {
		mScreenWidth = screenWidth;
		mScreenHeight = screenHeight;
		mBottom = bottom;

		mBackground.w = 20;
		mBackground.x = mScreenWidth - mBackground.w;
		mBackground.y = startPos;
		mBackground.h = height;

		mBar.x = mBackground.x + mBorder;
		mBar.w = mBackground.w - mBorder * 2;
		mBar.h = static_cast<int>((static_cast<float>(mScreenHeight) / mBottom) * mBackground.h);
		if (mBar.h < 150) {
			mBar.h = 150;
		}

		mBarColor = barColor;
		mBackgroundColor = backgroundColor;

		mScrollAmount = 0;
		mIsClicked = false;
	}

	void render(SDL_Renderer *renderer, int ticks) {
		if (isScrollBarNeeded()) {
			SDL_SetRenderDrawColor(renderer, mBackgroundColor.r, mBackgroundColor.g, mBackgroundColor.b, mBackgroundColor.a);
			SDL_RenderFillRect(renderer, &mBackground);
			mBar.y = mBackground.y + mBorder + static_cast<int>(static_cast<float>(mScrollAmount) / (mBottom - mBackground.h) * (mBackground.h - mBar.h));
			if (mIsClicked) {
				SDL_SetRenderDrawColor(renderer, static_cast<Uint8>(mBarColor.r * 0.8f), static_cast<Uint8>(mBarColor.g * 0.8f), static_cast<Uint8>(mBarColor.b * 0.8f), mBarColor.a);
			} else {
				SDL_SetRenderDrawColor(renderer, mBarColor.r, mBarColor.g, mBarColor.b, mBarColor.a);
			}
			SDL_RenderFillRect(renderer, &mBar);
		}
	}

	bool handleInput(SDL_Event &e) {
		if (isScrollBarNeeded()) {
			if (e.type == SDL_MOUSEBUTTONDOWN) {
				if (e.button.x >= mBar.x && e.button.x <= mBar.x + mBar.w && e.button.y >= mBar.y && e.button.y <= mBar.y + mBar.h) {
					mIsClicked = true;
					return true;
				} else if (e.button.x >= mBackground.x && e.button.x <= mBackground.x + mBackground.w && e.button.y >= mBackground.y && e.button.y <= mBackground.y + mBackground.h) {
					if (e.button.y < mBar.y) {
						scroll(10);
					} else {
						scroll(-10);
					}
					return true;
				}
			} else if (e.type == SDL_MOUSEBUTTONUP) {
				if (mIsClicked) {
					mIsClicked = false;
					return true;
				}
			} else if (e.type == SDL_MOUSEMOTION) {
				if (mIsClicked) {
					scroll(barDistToScrollAmount(-e.motion.yrel), 1);
					return true;
				}
			} else if (e.type == SDL_MOUSEWHEEL) {
				scroll(e.wheel.y);
				return true;
			} else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
					case SDLK_UP:
						scroll(1);
						return true;
					case SDLK_DOWN:
						scroll(-1);
						return true;
					case SDLK_PAGEUP:
						scroll(5);
						return true;
					case SDLK_PAGEDOWN:
						scroll(-5);
						return true;
					case SDLK_HOME:
						toTop();
						return true;
					case SDLK_END:
						toBottom();
						return true;
				}
			}
		}
		return false;
	}

	void scroll(int amount, int scrollSpeed) {
		mScrollAmount -= amount * scrollSpeed;
		validateScrollAmount();
	}

	void scroll(int amount) {
		scroll(amount, mScrollSpeed);
	}


	void toTop() {
		mScrollAmount = 0;
	}

	void toBottom() {
		mScrollAmount = mBottom - mScreenHeight;
	}

	bool isClicked() {
		return mIsClicked;
	}

	int getScrollAmount() {
		return mScrollAmount;
	}

	void setScrollAmount(int scrollAmount) {
		mScrollAmount = scrollAmount;
		validateScrollAmount();
	}

private:
	bool isScrollBarNeeded() {
		return mScreenHeight < mBackground.y + mBottom;
	}

	int barDistToScrollAmount(int dist) {
		float percent = static_cast<float>(dist) / mBackground.h;
		return static_cast<int>(percent * mBottom);
	}

	void validateScrollAmount() {
		if (mScrollAmount < 0) {
			toTop();
		} else if (mScrollAmount + mScreenHeight > mBottom) {
			toBottom();
		}
	}

	const int mScrollSpeed = 30;
	const int mBorder = 2;

	int mScrollAmount;
	int mBottom;
	int mScreenWidth;
	int mScreenHeight;
	SDL_Rect mBar;
	SDL_Rect mBackground;
	SDL_Color mBarColor;
	SDL_Color mBackgroundColor;
	bool mIsClicked;
};

#endif // _UISCROLLBAR_H
