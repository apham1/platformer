#ifndef _GAMESCREEN_H
#define _GAMESCREEN_H

#include "Screen.h"
#include "Actor.h"

class GameScreen : public Screen {
public:
	GameScreen(const char *levelFile = "");

	bool onEntry(App &app) override;
	void onExit() override;

	InputResult handleInput() override;
	InputResult handleInput(App &app, SDL_Event &e) override;

	void update(App &app) override;
	void render(App &app, int currentTicks) override;

	bool isStackedScreen() override;

private:
	bool loadMedia(App &app);
	void cleanUp();
	void updateCamera(App &app);
	void resetGame();
	void levelUp();
	void spawnProjectile(Actor *shootingActor, std::vector<Actor> *entities);

	void renderHitboxesRect(App &app);
	bool mShowHitboxes;

	SDL_Rect mCamera;
	Texture mBackgroundTexture;
	Actor *mPlayer;
	std::vector<Actor> mActors;
	std::vector<Actor> mProjectiles;
	std::vector<Actor> mShockwaves;
	std::vector<std::vector<Actor>*> mCombinedProjectiles;
	int mCurrentLevel = 8;
	int mLastLevel = 8;

	const float mMaxSoundDistanceSquared = 1200.0f * 1200.0f;

	std::string mLevelFileToLoad;
};

#endif /* _GAMESCREEN_H */