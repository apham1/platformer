#ifndef _AUDIO_H
#define _AUDIO_H

enum Sound {
	JUMP,
	SHOOT,
	TOTAL_SOUNDS
};

struct SoundData {
	SoundData(Sound sound, int volume = 100) {
		this->sound = sound;
		this->volume = volume;
	}

	Sound sound;
	int volume;
};

class Audio {
public:
	virtual ~Audio() {}
	virtual void addSoundToQueue(Sound sound, int volume = 100) = 0;
	virtual void playQueuedSounds() = 0;
	virtual bool init() = 0;
	virtual void close() = 0;

private:
	virtual void playSound(SoundData soundData) = 0;
};

#endif /* _AUDIO_H */