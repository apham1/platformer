#ifndef _ESCREENHELP_H
#define _ESCREENHELP_H

#include "Screen.h"
#include "Texture.h"
#include <SDL_ttf.h>
#include <vector>
#include "UIScrollBar.h"

class EScreenHelp : public Screen {
public:
	EScreenHelp();

	bool onEntry(App &app) override;
	void onExit() override;

	InputResult handleInput() override;
	InputResult handleInput(App &app, SDL_Event &e) override;

	void update(App &app) override;
	void render(App &app, int currentTicks) override;

	bool isStackedScreen() override;

private:
	struct TextLine {
		std::vector<Texture> text;
	};

	enum TextType {
		DEFAULT,
		HEADER1,
		HEADER2,
		HEADER3,
		LINEBREAK = -1
	};

	std::vector<TextLine> mPage;

	const int mLeftMargin = 40;
	const int mTopMargin = 40;
	const int mColumnSpacer = 40;

	TTF_Font *mTitleFont;
	const SDL_Color mTitleColor = { 0xF2, 0xFF, 0x00, 0xFF };

	TTF_Font *mCategoryFont;
	const SDL_Color mCategoryColor = { 0xBF, 0xC9, 0x00, 0xFF };

	TTF_Font *mFunctionFont;
	const SDL_Color mFunctionColor = { 0xED, 0xA2, 0x00, 0xFF };

	TTF_Font *mKeyFont;
	const SDL_Color mKeyColor = { 0xFF, 0xFF, 0xFF, 0xFF };

	UIScrollBar mScrollBar;
};

#endif // _ESCREENHELP_H
