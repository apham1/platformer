#ifndef _UIBUTTONTEXTURE_H
#define _UIBUTTONTEXTURE_H

#include "UIButton.h"
#include "Texture.h"

class UIButtonTexture : public UIButton {
public:
	UIButtonTexture(int x, int y, int width, int height, Texture *texture) : UIButton(x, y, width, height) {
		mTexture = texture;
	}

	void render(SDL_Renderer *renderer, int scroll, int ticks) override {
		mTexture->render(renderer, x, y, width, height);
	}

private:
	Texture *mTexture;
};

#endif /* _UIBUTTONTEXTURE_H */