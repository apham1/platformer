#ifndef _GUNCOMP_H
#define _GUNCOMP_H

class Actor;

class GunComp {
public:
	GunComp(int cooldown, int projectileLifespan, int delay, int projectileType = 0, int projectileSpawnX = 0, int projectileSpawnY = 0) {
		mCooldown = cooldown;
		mProjectileLifespan = projectileLifespan;
		mDelay = delay;
		mCount = -mDelay;
		mProjectileSpawnX = projectileSpawnX;
		mProjectileSpawnY = projectileSpawnY;
		mProjectileType = projectileType;
	}

	virtual void update(Actor &actor);
	void setCooldown(int cooldown);
	void setProjectileLifespan(int projectileLifespan);
	void setDelay(int delay);
	void setProjectileSpawnOffset(int x, int y);
	void setProjectileType(int type);

	int mCooldown, mProjectileLifespan, mDelay, mCount, mProjectileType;
	int mProjectileSpawnX, mProjectileSpawnY;
};
#endif /* _GUNCOMP_H */