#ifndef _UIBUTTON_H
#define _UIBUTTON_H

#include <SDL.h>

class UIButton {
public:
	UIButton(int x, int y, int width, int height) {
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
		mIsHovered = false;
	}

	virtual void render(SDL_Renderer *renderer, int scroll, int ticks) = 0;

	virtual void updateButton(int mouseX, int mouseY, bool mousePressed) {
		mIsHovered = false;
		mIsClicked = false;
		if (mouseX >= x && mouseX <= x + width && mouseY >= y && mouseY <= y + height) {
			mIsHovered = true;
			if (mousePressed) {
				mIsClicked = true;
			}
		}
	}

	virtual bool isClicked() {
		return mIsClicked;
	}

	virtual bool isHovered() {
		return mIsHovered;
	}

protected:
	int x, y, width, height;
	bool mIsHovered, mIsClicked;
};

#endif // _UIBUTTON_H
