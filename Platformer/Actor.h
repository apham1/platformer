#ifndef _ACTOR_H
#define _ACTOR_H

#include "InputComp.h"
#include "GraphicsComp.h"
#include "PhysicsComp.h"
#include "GunComp.h"

enum Direction {
	LEFT,
	RIGHT,
	UP,
	DOWN
};

struct ActorData {
	int ID;
	float x, y;
	Direction direction;
	int projectileCooldown, projectileLifespan, delay;
	char buf[84];

	ActorData(int ID = -1, float x = 0.0f, float y = 0.0f, Direction direction = RIGHT, int cooldown = -1, int projectileLifeSpan = -1, int delay = -1) {
		this->ID = ID;
		this->x = x;
		this->y = y;
		this->direction = direction;
		this->projectileCooldown = cooldown;
		this->projectileLifespan = projectileLifeSpan;
		this->delay = delay;
	}
};

struct Sling {
	float xPos, yPos;
	float xSpeed, ySpeed, slingSpeed;
	bool hasSlung, isSlinging;

	bool operator==(const Sling& other) {
		return (xPos == other.xPos) &&
			   (yPos == other.yPos) &&
			   (xSpeed == other.xSpeed) &&
			   (ySpeed == other.ySpeed) &&
			   (slingSpeed == other.slingSpeed) &&
			   (hasSlung == other.hasSlung) &&
			   (isSlinging == other.isSlinging);
	}
};

class Actor {
public:
	float mPosX, mPosY;
	float mVelX, mVelY;
	float mSpeed, mAcceleration;
	float mScale;
	bool mAirborne, mIsAlive, mCanWin, mLevelUp, mIsInvincible, mIsKnockedBack, mIsShooting, mIsDucking;
	int actorID;
	int mInvincibleCount, mLifeSpan;
	Direction mDirection;

	Sling mSling;
	Actor *mLastTouchedSling;

	Actor();
	Actor(float speed, GraphicsComp *graphicsComp = NULL, PhysicsComp *physicsComp = NULL, InputComp *inputComp = NULL);
	~Actor();

	void handleInput();
	bool handleInput(SDL_Event &e);
	void setAlpha(int alpha);
	void update();
	void update(std::vector<Actor> &objects);
	void update(std::vector<Actor> &objects, std::vector<std::vector<Actor>*> &projectiles);
	void render(SDL_Renderer *renderer, SDL_Rect *camera, int tick);

	void setGunComponent(GunComp *gunComponent);

	GraphicsComp *graphics() { return mGraphicsComp; }
	PhysicsComp *physics() { return mPhysicsComp; }
	GunComp *gun() { return mGunComp; };
	void setPos(float x, float y);
	void setInitialPos(float x, float y);
	void setPosX(float x);
	void setPosY(float y);
	void setScale(float scale);
	void setSlingSpeed(float slingSpeed);

	bool operator==(const Actor& other);

private:
	InputComp *mInputComp;
	GraphicsComp *mGraphicsComp;
	PhysicsComp *mPhysicsComp;
	GunComp *mGunComp;
};

#endif /* _ACTOR_H */