#include "EToolCreate.h"

EToolCreate::EToolCreate() {
}

/** Handles non-event input.
    @param editor The editor screen. 
*/
bool EToolCreate::handleInput(EditorScreen &editor) {
	return false;
}

/** Handles event input.
    @param editor The editor screen. 
    @param e The event being handled.
*/
bool EToolCreate::handleInput(App &app, EditorScreen &editor, SDL_Event &e) {
	if (e.type == SDL_MOUSEBUTTONDOWN) {
		if (e.button.button == SDL_BUTTON_LEFT) {
			Actor *newActor = ActorFactory::createActor(editor.mCreateActor->actorID, editor.mMouseX + editor.mCamera.x, editor.mMouseY + editor.mCamera.y, editor.mCreateActor->mDirection);
			editor.mActors.push_back(*newActor);
			return true;
		}
	} else if (e.type == SDL_KEYDOWN) {
		switch (e.key.keysym.sym) {
			int nextActorID;
			case SDLK_q:
				if (editor.mCreateActorID <= 0) {
					nextActorID = editor.mTotalActorTypes - 1;
				} else {
					nextActorID = editor.mCreateActorID - 1;
				}
				editor.changeObject(nextActorID, editor.mCreateActorDirection);

				return true;
			case SDLK_e:
				if (editor.mCreateActorID >= editor.mTotalActorTypes - 1) {
					nextActorID = 0;
				} else {
					nextActorID = editor.mCreateActorID + 1;
				}
				editor.changeObject(nextActorID, editor.mCreateActorDirection);

				return true;
			case SDLK_f:
				if (editor.mCreateActorDirection == LEFT) {
					editor.mCreateActorDirection = RIGHT;
				} else if (editor.mCreateActorDirection == RIGHT) {
					editor.mCreateActorDirection = LEFT;
				}
				editor.changeObject(editor.mCreateActorID, editor.mCreateActorDirection);
				return true;
		}
	}

	return false;
}

/** Updates the screen.
    @param editor The editor screen. 
	@param renderer The SDL renderer used to update text. 
*/
void EToolCreate::update(EditorScreen &editor, SDL_Renderer *renderer) {
	editor.mCreateActor->setPos(editor.mMouseX + editor.mCamera.x, editor.mMouseY + editor.mCamera.y);
	editor.mCreateActorText.loadText(renderer, editor.mFont, "Current Actor: " + editor.mCreateActorName);
}

/** Renders actors and textures onto the screen.
    @param editor The editor screen. 
	@param renderer The SDL renderer used to render textures onto the screen.
	@param screenWidth The total width of the screen. 
	@param screenHeight The total height of the screen.
	@param currentTicks The current tick of the game. 
*/
void EToolCreate::render(EditorScreen &editor, SDL_Renderer *renderer, int screenWidth, int screenHeight, int currentTicks) {
	editor.mCreateActor->setAlpha(150);
	editor.mCreateActor->render(renderer, &editor.mCamera, currentTicks);
	editor.mCreateActor->setAlpha(255);

	SDL_Rect currentActorBox = { 0, screenHeight - 25, 250, 25 };
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderFillRect(renderer, &currentActorBox);
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderDrawRect(renderer, &currentActorBox);

	editor.mCreateActorText.render(renderer, currentActorBox.x + 3, currentActorBox.y + 3);
}
