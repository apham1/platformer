#ifndef _GRAPHICSCOMPPLAYER_H
#define _GRAPHICSCOMPPLAYER_H

#include "GraphicsComp.h"

class GraphicsCompPlayer : public GraphicsComp {
public:
	GraphicsCompPlayer(Texture* texture, Texture *duckingTexture);
	void render(Actor &actor, SDL_Renderer *renderer, SDL_Rect *camera, int tick) override;
	int getWidth() override;
	int getHeight() override;

	void stand() override;
	void duck() override;

private:
	Texture *mStandingTexture, *mDuckingTexture;
};

#endif /* _GRAPHICSCOMPPLAYER_H */
