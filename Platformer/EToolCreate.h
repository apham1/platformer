#ifndef _ETOOLCREATE_H
#define _ETOOLCREATE_H

#include "EditorTool.h"

class EToolCreate : public EditorTool {
public:
	EToolCreate();

	bool handleInput(EditorScreen &editor) override;
	bool handleInput(App &app, EditorScreen &editor, SDL_Event &e) override;

	void update(EditorScreen &editor, SDL_Renderer *renderer) override;
	void render(EditorScreen &editor, SDL_Renderer *renderer, int screenWidth, int screenHeight, int currentTicks) override;

};

#endif // _ETOOLCREATE_H

